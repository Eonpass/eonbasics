from flask import Blueprint, Flask
from flask_restplus import Api
from werkzeug.middleware.proxy_fix import ProxyFix

from app.main.config import config_by_name
from app.main.controller.auth_controller import api as auth_ns
from app.main.controller.user_controller import api as user_ns
from app.main.controller.txo_controller import api as txo_ns
from app.main.controller.document_controller import api as document_ns
from app.main.controller.dlcsession_controller import api as dlcsession_ns
from app.main.controller.dlcoutcome_controller import api as dlcoutcome_ns
from app.main.controller.notarizationsession_controller import api as notarizationsession_ns
from app.main.controller.notarizationitem_controller import api as notarizationitem_ns
from app.main.controller.asset_controller import api as asset_ns
from app.main.controller.rpc_controller import api as rpc_ns
from app.main.services import db, flask_bcrypt
from app.main.celery import init_celery
from app.main.service.user_service import create_admin_first_startup

def create_app(**kwargs):
    """ create the flask app, then initalise the services: db, bycrypt, celery; finally preapre the api blueprint"""
    config_name=kwargs.get("config_name")
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_port=1)

    db.init_app(app)
    flask_bcrypt.init_app(app)
    if(kwargs.get("celery")):
      init_celery(kwargs.get("celery"), app)

    blueprint = Blueprint('api', __name__)
    api = Api(blueprint,
              title='EONBASICS',
              version='0.1.0',
              description='Eonpass reference implementation with restplus web service and JWT')
    api.add_namespace(user_ns, path='/user')
    api.add_namespace(auth_ns)
    api.add_namespace(txo_ns, path='/txo')
    api.add_namespace(document_ns, path='/document')
    api.add_namespace(notarizationsession_ns, path='/notarizationsession')
    api.add_namespace(notarizationitem_ns, path='/notarizationitem')
    api.add_namespace(dlcsession_ns, path='/dlcsession')
    api.add_namespace(dlcoutcome_ns, path='/dlcoutcome')
    api.add_namespace(asset_ns, path='/asset')
    api.add_namespace(rpc_ns, path='/rpc')
    app.register_blueprint(blueprint)

    app.app_context().push()

    @app.before_first_request
    def before_first_request():
      create_admin_first_startup()

    return app
