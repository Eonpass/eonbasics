from flask import request
from flask_restplus import Resource

from app.main.service.notarizationsession_service import (save_new_notarization_session, get_a_notarization_session, get_all_notarization_sessions, lock_notarization_session, notarize_session, get_availabe_tips, update_session, delete_notarization_session)
from app.main.util.dto import NotarizationSessionDto
from app.main.util.eonerror import EonError
from app.main.util.decorator import admin_token_required, token_required

api = NotarizationSessionDto.api
_notarizationsession = NotarizationSessionDto.notarizationsession
_new_notarizationsession = NotarizationSessionDto.new_notarizationsession
_notarizationsession_page = NotarizationSessionDto.paged_notarizationsession_list

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class NotarizationSessionList(Resource):
    @api.doc('List of notarization sessions registere on this node')
    @api.doc(params={
        'page': {'description': 'desired page number, defaults to 1', 'in': 'query', 'type': 'int'},
        'page_size': {'description': 'desired number of records per page, defaults to 20', 'in': 'query', 'type': 'int'}
    })
    @api.marshal_with(_notarizationsession_page)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self):
        """Returns notarization sessions in a paged list"""
        try:
            return get_all_notarization_sessions(request.args)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('Register a new notarization session')
    @api.expect(parser, _new_notarizationsession, validate=True)
    @api.response(201, 'Notarization Session successfully created.')
    @api.response(400, 'Notarization Session input data is invalid.')
    @api.response(409, 'Notarization Session already exists.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        """Register a new Notarization Session"""
        data = request.json
        try:
            return save_new_notarization_session(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/availabletips')
class NotarizationSession(Resource):
    @api.doc('get a notarization session')
    @api.expect(parser)
    @api.marshal_with(_notarizationsession, as_list=True)
    @api.response(404, 'Notarization Session not found.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def get(self):
        """Get a list of sessions which can be used as parent_session""" 
        try:
            return get_availabe_tips()
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The Notarization Session identifier')
class NotarizationSession(Resource):
    @api.doc('get a notarization session')
    @api.marshal_with(_notarizationsession)
    @api.response(404, 'Notarization Session not found.')
    @api.response(500, 'Internal Server Error.')
    def get(self, public_id):
        """Get details for a single notarization session""" 
        try:
            return get_a_notarization_session(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('delete a session')
    @api.response(200, 'Success.')
    @api.response(404, 'Session not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def delete(self, public_id):
        """Delete a session""" 
        try:
            return delete_notarization_session(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('update a session - change previous session')
    @api.response(200, 'Success.')
    @api.response(404, 'Session not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _notarizationsession, validate=True)
    @api.marshal_with(_notarizationsession)
    @admin_token_required
    def put(self, public_id):
        """Update a session - change the previous session""" 
        data = request.json
        try:
            return update_session(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/rpc/lock')
@api.param('public_id', 'The Notarization Session identifier')
class NotarizationSessionLock(Resource):
    @api.doc('Lock a Notarization Session')
    @api.expect(parser)
    @api.marshal_with(_notarizationsession)
    @api.response(404, 'Notarization Session not found.')
    @api.response(500, 'Internal Server Error.')
    def post(self, public_id):
        """ Lock a session, i.e. create the merkle tree and create the document """
        try:
            data = request.json
            return lock_notarization_session(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/rpc/send')
@api.param('public_id', 'The Notarization Session identifier')
class NotarizationSessionSend(Resource):
    @api.doc('Send a Notarization Session to the blockchain')
    @api.expect(parser)
    @api.marshal_with(_notarizationsession)
    @api.response(404, 'Notarization Session not found.')
    @api.response(500, 'Internal Server Error.')
    def post(self, public_id):
        """ Spend the txo and put the notarisation data in the OP_RETURN """
        try:
            data = request.json
            return notarize_session(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)