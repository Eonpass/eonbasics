from flask import request
from flask_restplus import Resource

from app.main.service.notarizationitem_service import (save_new_notarization_item, get_a_notarization_item, get_notarization_item_proof)
from app.main.util.dto import NotarizationItemDto
from app.main.util.eonerror import EonError
from app.main.util.decorator import admin_token_required, token_required

api = NotarizationItemDto.api
_notarizationitem = NotarizationItemDto.notarizationitem
_new_notarizationitem = NotarizationItemDto.new_notarizationitem

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class NotarizationItemList(Resource):
    @api.doc('Register a new notarization item')
    @api.expect(parser, _new_notarizationitem, validate=True)
    @api.response(201, 'Notarization Item successfully created.')
    @api.response(400, 'Notarization Item input data is invalid.')
    @api.response(409, 'Notarization Item already exists.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        """Register a new Notarization Item"""
        data = request.json
        try:
            return save_new_notarization_item(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<public_id>')
@api.param('public_id', 'The Notarization Item identifier')
class NotarizationItem(Resource):
    @api.doc('get a notarization item')
    @api.marshal_with(_notarizationitem)
    @api.response(404, 'Notarization Item not found.')
    @api.response(500, 'Internal Server Error.')
    def get(self, public_id):
        """Get details for a single notarization item""" 
        try:
            return get_a_notarization_item(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/proof')
@api.param('public_id', 'The Notarization Item identifier')
class NotarizationItem(Resource):
    @api.doc('get a notarization item proof')
    @api.response(404, 'Notarization Item not found.')
    @api.response(500, 'Internal Server Error.')
    def get(self, public_id):
        """Get a complete proof of the notarisation""" 
        try:
            return get_notarization_item_proof(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)
