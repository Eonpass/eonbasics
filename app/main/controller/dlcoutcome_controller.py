from flask import request
from flask_restplus import Resource

from app.main.service.dlcoutcome_service import (get_a_dlcoutcome, get_all_dlcoutcomes, save_new_dlcoutcome, update_an_outcome, confirm_outcome, pay_outcome, default_outcome)
from app.main.util.dto import DlcOutcomeDto
from app.main.util.eonerror import EonError
from app.main.util.decorator import admin_token_required, token_required

api = DlcOutcomeDto.api
_dlcoutcome = DlcOutcomeDto.dlcoutcome
_new_dlcoutcome = DlcOutcomeDto.new_dlcoutcome
_dlcoutcome_page = DlcOutcomeDto.paged_dlcoutcome_list
_upd_dlcoutcome = DlcOutcomeDto.update_dlcoutcome
_confirm_dlcoutcome = DlcOutcomeDto.confirm_dlcoutcome
_pay_dlcoutcome = DlcOutcomeDto.pay_dlcoutcome

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class DlcOutcomeList(Resource):
    @api.doc('List of dlc outcomes registere on this node')
    @api.doc(params={
        'page': {'description': 'desired page number, defaults to 1', 'in': 'query', 'type': 'int'},
        'page_size': {'description': 'desired number of records per page, defaults to 20', 'in': 'query', 'type': 'int'}
    })
    @api.marshal_with(_dlcoutcome_page)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self):
        """Returns dlc sessions in a paged list"""
        try:
            return get_all_dlcoutcomes(request.args)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('Register a new dlc outcome')
    @api.expect(parser, _new_dlcoutcome, validate=True)
    @api.response(201, 'Dlc Outcome successfully created.')
    @api.response(400, 'Dlc Outcome input data is invalid.')
    @api.response(409, 'Dlc Outcome already exists.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        """Register a new Dlc Outcome"""
        data = request.json
        try:
            return save_new_dlcoutcome(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<public_id>')
@api.param('public_id', 'The Dlc Outcome identifier')
class DlcOutcome(Resource):
    @api.doc('get a dlc outcome')
    @api.marshal_with(_dlcoutcome)
    @api.response(404, 'Dlc Outcome not found.')
    @api.response(500, 'Internal Server Error.')
    def get(self, public_id):
        """Get details for a single dlc outcome""" 
        try:
            return get_a_dlcoutcome(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('Update winner address of a dlc outcome')
    @api.expect(parser, _upd_dlcoutcome, validate=True)
    @api.response(201, 'Dlc Outcome successfully updated.')
    @api.response(400, 'Dlc Outcome input data is invalid.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self, public_id):
        """ Update a pending outcome """
        try:
            data = request.json
            return update_an_outcome(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/rpc/confirm')
@api.param('public_id', 'The Dlc Outcome identifier')
class DlcOutcomeConfirm(Resource):
    @api.doc('confirm a dlc_outcome by uploading the signed message by the oracle')
    @api.expect(parser, _confirm_dlcoutcome, validate=True)
    @api.marshal_with(_dlcoutcome)
    @api.response(404, 'Dlc Outcome not found.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self, public_id):
        """ Confirm a pending outcome """
        try:
            data = request.json
            return confirm_outcome(public_id, data.get("signed_message"))
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/rpc/pay')
@api.param('public_id', 'The Dlc Outcome identifier')
class DlcOutcomePay(Resource):
    @api.doc('Pay a confirmed Dlc Outcome')
    @api.expect(parser, _pay_dlcoutcome, validate=True)
    @api.marshal_with(_dlcoutcome)
    @api.response(404, 'Dlc Outcome not found.')
    @api.response(409, 'Key errors.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self, public_id):
        """ Pay a confirmed outcome """
        try:
            data = request.json
            return pay_outcome(public_id, data.get("wif"))
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/rpc/default')
@api.param('public_id', 'The Dlc Outcome identifier')
class DlcOutcomePay(Resource):
    @api.doc('Pay the default option of a Dlc Outcome - after the timelock expires')
    @api.expect(parser, _pay_dlcoutcome, validate=True)
    @api.marshal_with(_dlcoutcome)
    @api.response(404, 'Dlc Outcome not found.')
    @api.response(409, 'Key errors or timelock not expired yet.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self, public_id):
        """ Pay the default option of an outcome after the timelock expires """
        try:
            data = request.json
            return default_outcome(public_id, data.get("wif"))
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)