from flask import request
from flask_restplus import Resource

from app.main.service.dlcsession_service import (get_a_dlcsession, get_all_dlcsessions, save_new_dlcsession, lock_session)
from app.main.util.dto import DlcSessionDto
from app.main.util.eonerror import EonError
from app.main.util.decorator import admin_token_required, token_required

api = DlcSessionDto.api
_dlcsession = DlcSessionDto.dlcsession
_new_dlcsession = DlcSessionDto.new_dlcsession
_dlcsessions_page = DlcSessionDto.paged_dlcsession_list

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class DlcSessionList(Resource):
    @api.doc('List of dlc sessions registere on this node')
    @api.doc(params={
        'page': {'description': 'desired page number, defaults to 1', 'in': 'query', 'type': 'int'},
        'page_size': {'description': 'desired number of records per page, defaults to 20', 'in': 'query', 'type': 'int'}
    })
    @api.marshal_with(_dlcsessions_page)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self):
        """Returns dlc sessions in a paged list"""
        try:
            return get_all_dlcsessions(request.args)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('Register a new dlc session')
    @api.expect(parser, _new_dlcsession, validate=True)
    @api.response(201, 'Dlc Session successfully created.')
    @api.response(400, 'Dlc Session input data is invalid.')
    @api.response(409, 'Dlc Session already exists.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        """Register a new Dlc Session"""
        data = request.json
        try:
            return save_new_dlcsession(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<public_id>')
@api.param('public_id', 'The Dlc Session identifier')
class DlcSession(Resource):
    @api.doc('get a dlc session')
    @api.marshal_with(_dlcsession)
    @api.response(404, 'Dlc Session not found.')
    @api.response(500, 'Internal Server Error.')
    def get(self, public_id):
        """Get details for a single dlc session""" 
        try:
            return get_a_dlcsession(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<public_id>/rpc/lock')
@api.param('public_id', 'The Dlc Session identifier')
class DlcSessionLock(Resource):
    @api.doc('Lock a Dlc Session')
    @api.expect(parser)
    @api.marshal_with(_dlcsession)
    @api.response(404, 'Dlc Session not found.')
    @api.response(500, 'Internal Server Error.')
    def post(self, public_id):
        """ Lock a session, i.e. fund the escrow account """
        try:
            data = request.json
            return lock_session(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)