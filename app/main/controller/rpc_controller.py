from flask import request
from flask_restplus import Resource


from app.main.service.rpc_service import check_signature, get_local_pub_key, get_new_address, schnorr_sign, verify_schnorr_sign, update_local_key

from app.main.util.dto import MethodResultDto
from app.main.util.eonerror import EonError
from app.main.util.decorator import admin_token_required, token_required

api = MethodResultDto.api
_method_result = MethodResultDto.method_result
_schnorr_sign_body = MethodResultDto.schnorr_sign_body
_schnorr_verify_body = MethodResultDto.schnorr_verify_body
_new_key = MethodResultDto.new_key

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")


@api.route('/check-signature')
class SignatureCalls(Resource):
    @api.doc('Method for checking signatures')
    @api.marshal_with(_method_result)
    @api.response(200, 'Method executed, check result.')
    @api.response(400, 'Bad request, invalid input data format.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        data = request.json
        try:
            return check_signature(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/schnorr-signature/sign')
class SchnorrSignatureCalls(Resource):
    @api.doc('Method for creating a schnorr signature, DEMO PURPOSES ONLY')
    @api.marshal_with(_method_result)
    @api.response(200, 'Method executed, check result.')
    @api.response(400, 'Bad request, invalid input data format.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _schnorr_sign_body, validate=True)
    #@admin_token_required
    def post(self):
        data = request.json
        try:
            return schnorr_sign(data["message"], data["k"], data["priv"])
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/schnorr-signature/verify')
class SchnorrSignatureVerify(Resource):
    @api.doc('Method for checking a schnorr signature')
    @api.marshal_with(_method_result)
    @api.response(200, 'Method executed, check result.')
    @api.response(400, 'Bad request, invalid input data format.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _schnorr_verify_body, validate=True)
    #@admin_token_required
    def post(self):
        data = request.json
        try:
            return verify_schnorr_sign(data["message"], data["k"], data["pub"], data["signed_message"])
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route("/public-key")
class Keys(Resource):
    @api.doc('Method for getting the public key')
    @api.marshal_with(_method_result)
    @api.response(200, 'Method executed, check result.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @token_required
    def get(self):
        try:
            return get_local_pub_key()
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)
        except Exception as general_e:
            api.abort(500)

@api.route('/new-address')
class AddressCalls(Resource):
    @api.doc('Method for getting a new address from the node')
    @api.marshal_with(_method_result)
    @api.response(200, 'Method executed, check result.')
    @api.response(400, 'Bad request, invalid input data format.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        data = request.json
        try:
            return get_new_address()
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/update-local-key')
class LocalKey(Resource):
    @api.doc('DEMO ONLY - update the local key use by the node, nodes in production are not using local keys anyway')
    @api.response(200, 'ACK')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _new_key)
    @admin_token_required
    def post(self):
        """Update the local key of the node - used only in dev and testing""" 
        data = request.json
        try:
            return update_local_key(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)



