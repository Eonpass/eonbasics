import datetime

from bitcoinrpc.authproxy import JSONRPCException
from sqlalchemy import Index

from app.main.services import db
from app.main.util.crypto import cu

"""
Dlc Outcome model

store the information about possible outcomes: 
-the message in clear to be signed by the oracle
-the address of the winner of that outcome
-the public key of the winner of that outcome


"""


class DlcOutcome(db.Model):
    """ Txo Model for storing txids and vouts related details """
    __tablename__ = "dlc_outcome"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(100), unique=True, index=True)
    dlc_session_id = db.Column(db.String(100), db.ForeignKey('dlc_session.public_id'))
    message = db.Column(db.String(100), nullable=False)
    winner_pubkey = db.Column(db.String(100), nullable=False)
    combined_pubkey = db.Column(db.String(100), nullable=False)
    default_pubkey = db.Column(db.String(100), nullable=False)
    escrow_address =  db.Column(db.String(100), nullable=False)
    locktime =  db.Column(db.BigInteger, nullable=False)
    winner_address = db.Column(db.String(100), nullable=False)
    signed_message = db.Column(db.String(100), nullable=True)
    sent_with_tx = db.Column(db.String(100), nullable=True)

    def __repr__(self):
        return "<DLC Outcome public_id:'{}', message:'{}', address:'{}'>".format(
            self.public_id, self.message, self.winner_address)

    @property
    def status(self):
        """
        Pending: new outcome
        Confirmed: outcome message has been signed by the oracle
        Paied: outcome value has been sent
        Expired: the outcome has not been signed and the value is going back to the sender
        """
        if(self.sent_with_tx):
            return "paied"
        elif(self.signed_message):
            return "confirmed"
        elif(self.sent_with_tx and not self.signed_message):
            return "default"
        else:
            return "pending"
