import datetime

from bitcoinrpc.authproxy import JSONRPCException
from sqlalchemy import Index
from sqlalchemy.orm  import relationship

from app.main.services import db
from app.main.util.crypto import cu

"""
Dlc Outcome model

store the information about possible outcomes: 
-the message in clear to be signed by the oracle
-the address of the winner of that outcome
-the public key of the winner of that outcome

"""

class NotarizationItem(db.Model):
    """ Notarization Item Model for storing the leafs of the merkle tree to be notarized """
    __tablename__ = "notarization_item"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(100), unique=True, index=True)
    notarization_session_id = db.Column(db.String(100), db.ForeignKey('notarization_session.public_id'), nullable=False)
    message_hash = db.Column(db.String(100), nullable=False)
    notes = db.Column(db.String(100), nullable=True)
    ebsi_notes = db.Column(db.String(1000), nullable=True)
    created_on = db.Column(db.DateTime, nullable=False)
    session = relationship("NotarizationSession", back_populates="items")

    def __repr__(self):
        return "<Notarization Item public_id:'{}', message_hash:'{}', session:'{}'>".format(
            self.public_id, self.message_hash, self.notarization_session_id)

