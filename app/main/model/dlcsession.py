import datetime
from sqlalchemy.orm  import relationship

from bitcoinrpc.authproxy import JSONRPCException
from sqlalchemy import Index

from app.main.services import db
from app.main.util.crypto import cu

"""
Oracle Session model

store the information about oracle sessions: 
-the fixed point R for the adaptor signatures
-the public key of the oracle 
-the possible list of outcomes

"""


class DlcSession(db.Model):
    """ DlcSession Model for storing DLC details, oracles, escrow account, etc.."""
    __tablename__ = "dlc_session"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(100), unique=True, index=True)
    k = db.Column(db.LargeBinary(32), nullable=False)
    oracle_pubkey = db.Column(db.String(66), unique=False, index=True)
    value = db.Column(db.Numeric)
    escrow_txid = db.Column(db.String(100), nullable=True)
    escrow_voutn = db.Column(db.Integer, nullable=True)
    outcomes = relationship("DlcOutcome", primaryjoin="DlcSession.public_id==DlcOutcome.dlc_session_id")

    def __repr__(self):
        return "<DLC Session public_id:'{}', k:'{}'>".format(
            self.public_id, self.k)

    @property
    def status(self):
        """
        Query the database to see if there is at least one outcome which has been signed
        """

        if not self.escrow_txid:
            return "pending"
        else:
            status="locked"
            for outcome in self.outcomes:
                if outcome.signed_message:
                    status="outcome confirmed"
                    if outcome.sent_with_tx:
                        status="paid"
                    break
            return status
