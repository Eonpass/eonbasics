import datetime

from bitcoinrpc.authproxy import JSONRPCException
from sqlalchemy import Index

from app.main.services import db
from app.main.util.crypto import cu

"""
TXO model

store the information about txos so that they can be used as seals. This table actas as 
a buffer between Eonbasics and the blockchain node.

the status (spent or unspent) is not stored in the db but it's handled as a property which,
every time it's checked queries the node if the status is not already spent.

TODO:
rationalise the way node information are bound to TXOs, for now we assume every isntance 
of Eonbasics will use Elements. But there may be more than one blockchain in the future.


"""


class Txo(db.Model):
    """ Txo Model for storing txids and vouts related details """
    __tablename__ = "txo"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(100), unique=True, index=True)
    txid = db.Column(db.String(100), nullable=False)
    voutn = db.Column(db.Integer, nullable=False)
    value = db.Column(db.Numeric)
    blockchain = db.Column(db.String(20))
    last_checked_time = db.Column(db.DateTime)
    last_checked_status = db.Column(db.String(10))
    last_checked_blockhash = db.Column(db.String(100))
    spent_with_tx = db.Column(db.String(100))
    __table_args__ = (Index('unique_txo_data', "txid", "voutn"), )
    # different blockchain may have the same values.. for now we ignore this collision
    # the current scope focuses only on 1 underlaying blockchain


    def __repr__(self):
        return "<TXO txid:'{}', voutn:'{}', value:'{}'>".format(
            self.txid, self.voutn, self.value)

    @property
    def status(self):
        """
        Query the blockchain to see the status of this txo, if it's alraedy
        spent, just retun "spent"
        """
        if(self.last_checked_status == 'spent'):
            return self.last_checked_status
        else:
            try:
                status = cu.check_txout(self.txid, self.voutn)
                return status
            except JSONRPCException as json_exception:
                print("A JSON RPC Exception occured: " + str(json_exception))
                return json_exception
            except Exception as general_exception:
                print("An Exception occured: " + str(general_exception))
                return general_exception

