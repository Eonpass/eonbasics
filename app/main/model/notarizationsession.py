import datetime
from sqlalchemy.orm  import relationship

from bitcoinrpc.authproxy import JSONRPCException
from sqlalchemy import Index

from app.main.services import db
from app.main.util.crypto import cu

from app.main.model.document import Document

"""
Notarisation Session model

store the information about notarisation sessions: 
- previous session (relevant to get the proper seals)
- merkle root of all items
- linked items to notarise

"""


class NotarizationSession(db.Model):
    """ Notarization Session Model for building merkle trees of items """
    __tablename__ = "notarization_session"

    #id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(100), unique=True, index=True, primary_key=True)
    previous_session_id = db.Column(db.String(100), db.ForeignKey('notarization_session.public_id'))
    notarization_txid = db.Column(db.String(100), nullable=True)
    merkle_root = db.Column(db.String(100), nullable=True)
    stamper_type = db.Column(db.String(100), nullable=True)
    document_id = db.Column(db.String(100), db.ForeignKey('document.public_id'), nullable=True, unique=True)
    items = relationship("NotarizationItem", primaryjoin="NotarizationSession.public_id==NotarizationItem.notarization_session_id", cascade="all, delete-orphan")
    parent_session = db.relationship('NotarizationSession', remote_side=[public_id], backref=db.backref('children', lazy='dynamic'))


    @property
    def status(self):
        """
        Query the database to see if there is at least one outcome which has been signed
        """

        if not self.merkle_root:
            return "new"
        else:
            status="locked and waiting notarization"
            if self.notarization_txid:
                status="closed"
            return status

    def __repr__(self):
        return "<Notarization Session public_id:'{}', status:'{}'>".format(
            self.public_id, self.status)