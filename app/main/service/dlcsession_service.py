import uuid
import datetime
import math

from app.main.services import db
from app.main.util.crypto import cu
import app.main.util.schnorr as schnorr
import app.main.util.address as address
from app.main.model.dlcsession import DlcSession
from app.main.util.eonerror import EonError
#from app.main.util.decorator import log_error
# TODO: set the logging preference in the config file

def save_new_dlcsession(data):
    """
    Save a new DLC session in the local db

    the same R point can be used for more than one session for now, although discouraged

    Parameters
    ----------
    data: dict
        an object with the following fields, as described in dto dlc_session
        'k', the random int that will be used as fixed point R, hex string, optional if none or empty a random one is generated

    Returns
    -------
        [dict, int]
            an array which contains:
            0: the response_object with the following fields: 'status' success/fail,
            'message' and in case of success the 'public_id' of the new txo
            1: the return code for the successfull operation, 201

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is something strange in the args (e.g. r out of bound)
    """
    try:
        k=b""
        if data.get('k', None) is not None:
            #test with given k
            k = bytearray.fromhex(data['k'])
            if(len(k) > 32):
                raise EonError('Wrong k, max 32 bytes length.', 404)
        else:
            random_k = schnorr.generate_random()
            k = random_k.to_bytes(32, 'big')

        #test if the pubkey actually works:
        prefix = data.get("oracle_pubkey")[0:2]

        if(prefix!="02" and prefix!="03"):
            raise EonError("Only compressed pubkey are acceptect", 404)
        O = address.deserialize_pubkey(data.get("oracle_pubkey"))

        new_dlc_session = DlcSession(
            public_id=str(uuid.uuid4()),
            k=k,
            oracle_pubkey=data.get("oracle_pubkey"),
            value = data.get("value")
        )
        _save_changes(new_dlc_session)
        response_object = {
            'status': 'success',
            'message': 'New Dlc Session registered.',
            'public_id': new_dlc_session.public_id
        }
        return response_object, 201
    except EonError as eerr:
        raise eerr
    except Exception as e:
        print(e)
        raise EonError('Something went wrong, check the format of k and pubkey.'+str(e), 404)


def get_all_dlcsessions(args):
    """ 
    Get all the DLC sessions
    
    Parameters
    ----------
    args is requet.args passed from the API controller

    args.get("page"): str
        page number, will be cast to int
    args.get("page_size"): str
        records per page, will be cast to int

    Returns:
    --------
    dict
        a dict with the pagianted resources:
        'total_record_count', the total number of resources on the lcoal db
        'page_size', how many records per page
        'page', pg number
        'records', [obj], array of ORM objects

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        400: when there is something strange in the args (e.g. cannot cast to int)
    """
    try:

        local_dlc_sessions = DlcSession.query.all()
        payload = {}
        payload['total_record_count']=len(local_dlc_sessions)
        payload['page_size']= 20 if not args.get("page_size") else int(args.get("page_size"))
        payload['page']= 1 if not args.get("page") else int(args.get("page"))
        payload['records']=[]
        payload['page_size'] = payload['page_size'] if not payload['page_size'] > 200 else 200
        if(payload['page']<=0):
            payload['page']=1
        if(payload['page_size']*payload['page'] > payload['total_record_count']):
            #if indexes are out of bound, just return the last page
            payload['page'] = math.ceil(payload['total_record_count']/payload['page_size'])
        start_index = payload['page_size']*(payload['page']-1)
        end_index = (payload['page_size']*payload['page'])-1
        for dlcsession in local_dlc_sessions[start_index:end_index]:
            #on servers with low memory, if I pass the object after modifying k, an UPDATE is triggered (session flush)
            #this will throw an error because k is no longer bytes but a str
            session_payload = {
                "public_id":dlcsession.public_id,
                "k":dlcsession.k.hex(),
                "oracle_pubkey":dlcsession.oracle_pubkey,
                "value":dlcsession.value,
                "status":dlcsession.status,
                "escrow_txid":dlcsession.escrow_txid,
                "escrow_voutn":dlcsession.escrow_voutn
            }
            payload['records'].append(session_payload)
        return payload
    except ValueError as verr:
        # casting args to int fails
        raise EonError(verr, 400)
    except Exception as e:
        # get_utxos throws Exception when the node has connection problems for instance
        print(e)
        raise EonError(e, 500)

def get_a_dlcsession(public_id):
    """
    Returns details of a dlc session

    Parameters
    ----------
    public_id: str
        the id on the db of the desired dlc session

    Returns
    -------
    object
        the dlc session object

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is no matching public_id in the local db
    """
    dlcsession = DlcSession.query.filter_by(public_id=public_id).first()
    if not dlcsession:
        raise EonError('Dlc Session not found.', 404)
    #sometimes, if I manipulate the dlcsession object, an UPDATE is triggered and then k is no longer bytes but a string...!!
    session_payload = {
        "public_id":dlcsession.public_id,
        "k":dlcsession.k.hex(),
        "oracle_pubkey":dlcsession.oracle_pubkey,
        "value":dlcsession.value,
        "status":dlcsession.status,
        "escrow_txid":dlcsession.escrow_txid,
        "escrow_voutn":dlcsession.escrow_voutn
    }
    return session_payload

def lock_session(public_id):
    """
    Freeze a session by sending funds to the contract address

    -get the contract address (combination of winner+oracle key and timelock for cashback)
    -send the value to the address
    -save the tx details (txid and vout possibly)

    Parameters
    ----------
    public_id: str
        the id on the db of the desired dlc session

    Returns
    -------
    object
        the dlc session object

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is no matching public_id in the local db
    """
    dlcsession = DlcSession.query.filter_by(public_id=public_id).first()
    if not dlcsession:
        raise EonError('Dlc Session not found.', 404)

    #we expect to have at least 1 outcome, for now, excalty one outcome
    if len(dlcsession.outcomes)!=1:
        raise EonError('Dlc Session must have only 1 outcome.', 400)

    escrow_address = dlcsession.outcomes[0].escrow_address
    if not(escrow_address):
        raise EonError('Dlc Outcome is missing the combined address.', 400)

    value = dlcsession.value 
    if not(value):
        raise EonError('Dlc Session is missing the value.', 400)

    utxos = cu.get_utxos()
    if(len(utxos)<1):
        raise EonError('There are no available outputs to fund this escrow, check the node.', 404)

    sent_tx = cu.create_simple_transaction(utxos[0]['txid'], utxos[0]['n'], '7e57', escrow_address, value)

    raw_tx = cu.get_raw_tx(sent_tx)
    sent_tx_id = raw_tx["txid"]
    formatted_value = value = '{:.8f}'.format(value)
    spendable_output_n = 0;
    for vout in raw_tx["vout"]:            
        if(vout.get("value")== formatted_value): #todo, check the script OP_HASH160 <hash> OP_EQUAL, not the value!
            break
        else:
            spendable_output_n+=1

    #TOD: there are no blinders here, try to toy around with raw_tx, etc..

    dlcsession.escrow_txid = sent_tx_id
    dlcsession.escrow_voutn = spendable_output_n
    _save_changes(dlcsession)
    return(dlcsession)

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def generate_address_from_outcomes(dlcsession):
    """
    Given a dlc session, generate the escrow address

    TODO: this part will be useful when multiple outcomes are possible and we build a merkle tree for them
    """

    #collect: oracle's key, winner's key and message
    outcomes = dlcsession.outcomes
    #TODO: for now we expect only 1 outcome
    winner_pubkey = outcomes[0].winner_pubkey
    oracle_pubkey = dlcsession.oracle_pubkey
    message = outcomes[0].message

    n = schnorr.getCurve()[1]






