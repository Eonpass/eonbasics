import uuid
import datetime
import math

from app.main.services import db
from app.main.util.crypto import cu
from app.main.util.keymanagementutils import kmc
from app.main.model.document import Document
from app.main.model.txo import Txo
from app.main.util.eonutils import EonUtils
from app.main.util.eonerror import EonError

from app.main.service.txo_service import refresh_txo_status, spend_a_txo, unlock_a_txo, lock_a_txo, get_a_txo


def save_new_document(data):
    """
    Inserts a new document

    check if there are already a document betwen the given seal, if not insert.
    This is a simple operation, all the logic for checking signatures and hashes is
    done in the update. Therefore the expected flow is to create the document with
    seals and basic hash, then update it with the other data.

    TODO:
    -use a composite index for uniqueness, try to insert and catch the uniqueness error

    Parameters:
    -----------
    data: dict
        the body of the POST request

    Returns:
    --------
    [dict, int]
        an array containing
        0: a dict with the result of the operation and the id of the newly created doc
        1: the success code for the operation, 201

    Raises:
    -------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is something strange in the args (e.g. txid doesn't exist)
        409: when there is already a document with the same seals

    """
    current_seal_id = None if not data.get('current_seal') else data.get('current_seal')
    next_seal_id = None if not data.get('next_seal') else data.get('next_seal')
    existing_doc = Document.query.filter_by(current_seal=current_seal_id, next_seal=next_seal_id).first()
    if not existing_doc:
        current_txo = Txo.query.filter_by(public_id=current_seal_id).first()
        next_txo = Txo.query.filter_by(public_id=next_seal_id).first()
        # check that document_with_seals_hash matches, if the field  was left
        # empty and there is the data, populate it:
        if(current_txo and next_txo):
            try:
                _check_hashed_message(data, current_txo, next_txo)
            except Exception as error:
                print(error)
                raise EonError(error.args[0], 409)


        message = EonUtils().concatenate_hash_and_txos(data.get('document_hash'), 
            current_txo.txid if current_txo else "", 
            current_txo.voutn if current_txo else "", 
            next_txo.txid if next_txo else "", 
            next_txo.voutn if next_txo else "")
        hashed_message = cu.hexdigest(message)
        document_with_seals_hash = hashed_message

        new_document = Document(
            public_id=str(uuid.uuid4()),
            current_seal=current_seal_id,
            next_seal=next_seal_id,
            document_hash=data.get('document_hash'),
            document_with_seals_hash = document_with_seals_hash
        )
        _save_changes(new_document)
        _lock_txos(current_seal_id, next_seal_id, current_seal_id, next_seal_id) #lock txos accepts: old_current_seal, old_next_seal, new_current_seal, new_next_seal; here old and new are the same
        return _confirm_creation(new_document)
    else:
        raise EonError('Another document already exists with the given seals.', 409)


def update_document(data):
    """
    Updates the document with the new field

    Check the payload for errors, see specifics for an accurate description, if all is coherent: update.
    Some fields shoudl be bytes while handled inside Eonpass but may arrive encoded from the requests.
    This method also tries to cast the bytes field to the correct type if needed (for public keys and signed messages)

    Parameters:
    -----------
    data: dict
        the body of the PUT request

    Returns:
    --------
    dict
        an object with: public_id, message and status confirming the operation success

    Raises:
    -------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is something strange in the args (e.g. txid doesn't exist)
        409: when there is already a document with the same seals
    """
    doc = Document.query.filter_by(public_id=data['public_id']).first()
    if not doc:
        raise EonError('Document does not exist.', 404)

    # check if there are the seals
    old_current_txo = Txo.query.filter_by(public_id=doc.current_seal).first()
    new_current_txo = Txo.query.filter_by(public_id=data.get('current_seal')).first()
    old_next_txo = Txo.query.filter_by(public_id=doc.next_seal).first()
    new_next_txo = Txo.query.filter_by(public_id=data.get('next_seal')).first()

    # allow for only one document between two seals, regardless of the stage
    overlapping_doc = Document.query.filter_by(current_seal=new_current_txo.public_id if new_current_txo else None, next_seal=new_next_txo.public_id if new_next_txo else None).first()
    if overlapping_doc and overlapping_doc.public_id != doc.public_id:
        raise EonError('Another document already exists with the given seals.', 409)

    # allow to change seals only if they are unspent
    if new_current_txo and old_current_txo and  old_current_txo is not new_current_txo:
        if old_current_txo.status == 'spent' or new_current_txo.status == 'spent':
            raise EonError('Current seal is already spent, cannot change documents with spent seals.', 409)

    if new_next_txo and old_next_txo and old_next_txo is not new_next_txo:
        if old_next_txo.status == 'spent' or new_next_txo.status == 'spent':
            raise EonError('Next seal is already spent, cannot change documents with spent seals.', 409)
    #checks on seals done, prepare the respective fields for update:
    doc.current_seal = doc.current_seal  if not data.get('current_seal') else data.get('current_seal')
    doc.next_seal = doc.next_seal if not data.get('next_seal') else data.get('next_seal') 

    # check the hased message
    # make sure you have the document_hash, if it's not in the payload, use the current one
    data['document_hash'] = doc.document_hash if not data.get('document_hash') else data.get('document_hash')
    try:
        current_txo = new_current_txo if new_current_txo else old_current_txo
        next_txo =  new_next_txo if new_next_txo else old_next_txo
        if(current_txo and next_txo):
            _check_hashed_message(data, current_txo, next_txo)
    except Exception as error:
        print(error)
        raise EonError(error.args[0], 409)
    #check done, prepare the field for update
    doc.document_hash = doc.document_hash if not data.get('document_hash') else data.get('document_hash')
    #document_with_seals_hash should be computed here:
    message = EonUtils().concatenate_hash_and_txos(doc.document_hash, doc.current_seal.txid, doc.current_seal.voutn, doc.next_seal.txid, doc.next_seal.voutn)
    hashed_message = cu.hexdigest(message)
    doc.document_with_seals_hash = hashed_message

    # check the messages and the signatures if present
    
    doc.sender_signed_hash = doc.sender_signed_hash if not data.get('sender_signed_hash') else data.get('sender_signed_hash')
    doc.sender_public_key = doc.sender_public_key if not data.get('sender_public_key') else data.get('sender_public_key')
    if(doc.sender_signed_hash and not doc.sender_public_key):
        raise EonError('Sender signature is missing the pub key.', 409)
    if doc.sender_signed_hash:
        is_correct = cu.check_message(bytes.fromhex(doc.sender_signed_hash),
                                      bytes.fromhex(doc.document_with_seals_hash), #this is a hexdigest by default when filled by _check_hashed_message
                                      doc.sender_public_key.encode('utf-8'))
        if not is_correct:
            raise EonError('Sender signature is corrupt.', 409)

    doc.sender_and_receiver_signed_hash = doc.sender_and_receiver_signed_hash if not data.get('sender_and_receiver_signed_hash') else data.get('sender_and_receiver_signed_hash')
    doc.receiver_public_key = doc.receiver_public_key if not data.get('receiver_public_key') else data.get('receiver_public_key')
    if(doc.sender_and_receiver_signed_hash and not doc.receiver_public_key):
        raise EonError('Receiver signature is missing the pub key.', 409)
    if doc.sender_and_receiver_signed_hash:
        is_correct = cu.check_message(bytes.fromhex(doc.sender_and_receiver_signed_hash),
                                      bytes.fromhex(doc.sender_signed_hash),
                                      doc.receiver_public_key.encode('utf-8'))
        if not is_correct:  
            raise EonError('Receiver signature is corrupt.', 409)

    #check that the history of this doc looks good (i.e. the previous documents check out, #TODO: evaluate whether to always check all document chain)
    previous_docs = get_related_documents(data['public_id'])
    for prev_doc in reversed(previous_docs):
        try:
            #get the seal, if not spent refresh it to see if it got spent in the meanwhile
            current_seal = get_a_txo(prev_doc.current_seal, "true")
            if(not(current_seal.spent_with_tx)):
                current_seal = get_a_txo(prev_doc.current_seal, "false")
            #if you have the spent_txid, check that the OP_RETURN is coherent
            if(current_seal.spent_with_tx):
                #TODO, add the OP_RETURN check
                check_spent_status = cu.was_seal_spent_in_tx(current_seal.txid, current_seal.voutn, current_seal.spent_with_tx)
                if(check_spent_status["confirmations"]<1):
                    raise EonError(400, "The history is broken at doc: %s" % prev_doc.public_id)
            
            #if you don't have the spent_txid, and the current doc is not the latest one, the history is broken!
            if(not(current_seal.spent_with_tx) and prev_doc.public_id!=data['public_id']):
                raise EonError(400, "The history is broken at doc: %s" % prev_doc.public_id)

            #finally, exit when inspecting the current doc, we care for the past history history
            if(prev_doc.public_id==data['public_id']): break
        except EonError as e:
            if(prev_doc.public_id==data['public_id']):
                #this could be partial data update in the current doc, the status will report what it is eventually:
                break;
            else:
                #Txo was not found, corrupt doc in history:
                raise EonError(400, "The history is broken at doc: %s" % prev_doc.public_id)
        except Exception as e:
            raise EonError(400, "The history is broken at doc: %s" % prev_doc.public_id)

    #all fields good to go, update
    _save_changes(doc)
    _lock_txos(old_current_txo.public_id if old_current_txo else None, #this is not there when inserting a document with text only and then trying to update with a proper id
        old_next_txo.public_id if old_next_txo else None, 
        new_current_txo.public_id if new_current_txo else (old_current_txo.public_id if old_current_txo else 'unknownid'), #meaning it's only updating other fields and the current_seal doesn't change
        new_next_txo.public_id if new_next_txo else None)
    return _confirm_update(doc)


def get_all_documents(args):
    """
    return the unfiltered list of all documents in the db

    Documents are a concept present only in the eonpass server, so cached refers to the state 
    of the seals (txos). Cached false will query the txos for their status from the node.
    The status of the document depends on the status of its seals.

    Parameters
    ----------
    args is requet.args passed from the API controller

    args.get("cached"): str
        true/false, default to true and returns the latest status from the db
    args.get("page"): str
        page number, will be cast to int
    args.get("page_size"): str
        records per page, will be cast to int

    Returns:
    --------
    dict
        a dict with the pagianted resources:
        'total_record_count', the total number of resources on the lcoal db
        'page_size', how many records per page
        'page', pg number
        'records', [obj], array of ORM objects

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        400: when there is something strange in the args (e.g. cannot cast to int)
    """
    local_docs = Document.query.all()
    try:        
        payload = {}
        payload['total_record_count']=len(local_docs)
        payload['page_size']= 20 if not args.get("page_size") else int(args.get("page_size"))
        payload['page']= 1 if not args.get("page") else int(args.get("page"))
        payload['records']=[]
        payload['page_size'] = payload['page_size'] if not payload['page_size'] > 200 else 200
        if(payload['page']<=0):
            payload['page']=1
        if(payload['page_size']*payload['page'] > payload['total_record_count']):
            #if indexes are out of bound, just return the last page
            payload['page'] = math.ceil(payload['total_record_count']/payload['page_size'])
        start_index = payload['page_size']*(payload['page']-1)
        end_index = (payload['page_size']*payload['page'])-1
        for doc in local_docs[start_index:end_index]:
            #if cached is false, use get_txo for the two seals
            #TODO
            if(args.get("cached") and args.get("cached")=="false"):
                st = doc.status

            payload['records'].append(doc)
        return payload
    except ValueError as verr:
        # casting args to int fails
        raise EonError(verr, 400)
    except Exception as e:
        # get_utxos throws Exception when the node has connection problems for instance
        print(e)
        raise EonError(e, 500)

def get_a_document(public_id, cached):
    """
    Returns details of a document, when cached is false, query the node and get fresh statuses for the seals

    Every time a non-cached request is done, its results are saved in the last_checked_ fields.

    Parameters
    ----------
    public_id: str
        the id on the db of the desired document

    cached: str (bool: "true"/"false", since it comes from query params)
        true returns data only form the db, false checks also on the node, defaults to true

    Returns
    -------
    obj
        the document object

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is no matching public_id in the local db
    """
    doc = Document.query.filter_by(public_id=public_id).first()
    if(cached and cached=="false"):
        if(doc.current_seal):
            current_seal = refresh_txo_status(doc.current_seal)
        if(doc.next_seal):
            next_seal = refresh_txo_status(doc.next_seal)
    else:
        if not doc:
            raise EonError('Document not found.', 404)
    return doc

def send_a_document(public_id):
    """
    Spend the current_seal on a document with the proper OP_RETURN

    Check if the document is ready to be sent, then compute
    the data payload and finally call the spend function of the Txo

    Parameters
    ----------
    public_id: str
        the public id of the document to send

    Returns
    -------
    #obj
    #    the document object with the new state and the "sent with tx" field
    str, txid
        the txid which was used to spend the seal, not clear why the doc does not have the sent_with_tx_field populated, TODO: debug


    Raises
    ------
    EonError
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        409: when the document is corrupt, not in the correct state or missing things
        404: when there is no matching public_id in the local db
    """

    doc = get_a_document(public_id, "true")
    if(not doc):
        raise EonError('Document not found', 404)

    #if public key and sender/sender+receiver signed messages are empty, sign with the key of the node and go on
    if(not doc.sender_public_key and not doc.sender_signed_hash and not doc.receiver_public_key and not doc.sender_and_receiver_signed_hash):
        local_pubkey = kmc.get_serialized_pub_key().decode('utf-8')
        doc.sender_public_key = local_pubkey
        doc.receiver_public_key = local_pubkey
        signed_message = cu.sign_message(bytes.fromhex(doc.document_with_seals_hash))
        doc.sender_signed_hash = signed_message['signed'].hex()
        receiver_signed_message = cu.sign_message(bytes.fromhex(doc.sender_signed_hash))
        doc.sender_and_receiver_signed_hash=receiver_signed_message['signed'].hex()
        _save_changes(doc)
    doc = get_a_document(public_id, "false")

    if(doc.status!='ready'):
        raise EonError('Document not ready for being sent, current status: '+doc.status, 409)

    #get the previous step in the document chain (if any) and check that it's sending seal
    #is confimrmed at least X times
    #prev_doc = Document.query(next_seal=doc.current_seal).first()
    #TODO

    #check the data before committing to the transaction: 
    #hash and signatures are checked when the document is updated through the eonpass node but
    #we may not know if the db was corrupt somehow in the meantime
    #therefore at least inspect that the payload is coherent:
    #check the hashed message:
    current_txo = Txo.query.filter_by(public_id=doc.current_seal).first()
    next_txo = Txo.query.filter_by(public_id=doc.next_seal).first()
    doc_data = {
        'document_hash':doc.document_hash
    }
    if not _check_hashed_message(doc_data, current_txo, next_txo):
        raise EonError('Hash of the document data and seals is corrupt', 409)
    #check signatures
    
    #sender side:
    if not cu.check_message(bytes.fromhex(doc.sender_signed_hash), bytes.fromhex(doc.document_with_seals_hash), doc.sender_public_key.encode("utf-8")):
        raise EonError('Sender signature is corrupt', 409)
    #receiver side:
    if not cu.check_message(bytes.fromhex(doc.sender_and_receiver_signed_hash), bytes.fromhex(doc.sender_signed_hash), doc.receiver_public_key.encode("utf-8")):
        raise EonError('Receiver signature is corrupt', 409)

    #spent the Txo
    unlock_a_txo(doc.current_seal)

    #TODO: double check the length of the hash according to max OP_RETURN size
    payload={
        'msg_hex': doc.sender_and_receiver_signed_hash
    }
    spent_txo = spend_a_txo(doc.current_seal, payload)

    #poll it to be sure it gets the sent_with_tx
    doc = get_a_document(public_id, "false")
    return spent_txo.spent_with_tx


def get_related_documents(public_id):
    """
    Get the aray of documents from start to the most recent which are concatenated through their seals

    Start from the document with the given public_id and explore the seals upstream and downstrewam.
    The origin document is the one which current_seal is no next_seal of any other document
    The tail documeent is the one which next_seal is empty of unknown. In case of unknown txo, the
    controller must warn the client.
    
    Parameters
    ----------
    public_id: str
        the public_id of the document for which the chain will be retrieved

    Returns
    -------
    [obj]
        the array of documents, starting from the origin to the latest.

    """
    doc = Document.query.filter_by(public_id=public_id).first()
    if not doc:
        raise EonError('Document not found.', 404)
    doc_chain = [doc]
    #explore upstream:
    upstream_doc = Document.query.filter_by(next_seal=doc.current_seal).first()
    while(upstream_doc):
        doc_chain.insert(0, upstream_doc)
        upstream_doc = Document.query.filter_by(next_seal=upstream_doc.current_seal).first()

    #explore downstream:
    downstream_doc = Document.query.filter_by(current_seal=doc.next_seal).first() if (doc.next_seal!=None) else None
    while(downstream_doc):
        doc_chain.append(downstream_doc)
        downstream_doc = Document.query.filter_by(current_seal=downstream_doc.next_seal).first() if (downstream_doc.next_seal!=None) else None

    return doc_chain

def delete_document(public_id):
    """
    destroy a document, this of course doesn't change the state of txos on the blockchain
    """
    doc = Document.query.filter_by(public_id=public_id).first()
    if(doc):
        _delete_object(doc)
        return _confirm_delete()  # returns a 200
    else:
        raise EonError('Document not found.', 404)

def check_seals_lock_status(data):
    """
    When the server starts, check all the currently present seals and lock them

    This should be done everytime the node goes down, because lock/unlock txos is an in-memory property.
    We assume that when the node goes down we also restart Eonbasics
    """
    local_docs = Document.query.all()
    total_docs = len(local_docs)
    exception_docs_ids = []
    for doc in local_docs:
        try:
            _lock_txos(doc.current_seal, doc.next_seal, doc.current_seal, doc.next_seal)
        except Exception as e:
            exception_docs_ids.append(doc.public_id)
            print(e)

    return _confirm_operation(total_docs,exception_docs_ids)


def _delete_object(data):
    """
    commit the delete of the object
    """
    db.session.delete(data)
    db.session.commit()


def _save_changes(data):
    """
    commit the new data in the db
    """
    db.session.add(data)
    db.session.commit()

def _check_hashed_message(doc_data, current_seal_txo, next_seal_txo):
    """ 
    check if the document_with_seals_hash is matching the currente data inside the doc and the seals.


    if the document_with_seals_hash field is empty, try to fill it with a side-effect.
    Notice that doc_data is coming from the request payload, seals instead come from SQLAlchemy, i.e.
    the first is a dict, the other are class instances

    Parameters
    ----------
    doc_data: dict
        doc_data is the request payload that is passed to the update_document (see above)

    current_seal_txo: object
        the sqlalchemy object retrieved from db, the current_seal

    next_seal_txo: object
        the sqlalchemy object retrieved from db, the next_seal

    Returns
    -------
    bool
        True when everything checks out

    Raises
    ------
    Exception
        when the hashed message doesn't match the seals and the clear text 

    """
    # if yo don't have the seals, there is no need to check the message
    if(not current_seal_txo or not next_seal_txo):
        return True
    message = EonUtils().concatenate_hash_and_txos(doc_data['document_hash'], current_seal_txo.txid, current_seal_txo.voutn, next_seal_txo.txid, next_seal_txo.voutn)
    
    hashed_message = cu.hexdigest(message)
    if doc_data.get('document_with_seals_hash') and doc_data.get('document_with_seals_hash') != hashed_message:
        raise Exception(
            'Hash of document with seals does not match, leave it empty so that Eonbasics computes it')
    elif doc_data.get('document_with_seals_hash') is None:
        doc_data['document_with_seals_hash'] = hashed_message #side-effect
        return True

def _lock_txos(old_current_seal, old_next_seal, new_current_seal, new_next_seal):
    """
    lock the new current and next seal, if the document had different seals, unlock them

    """
    operation_success = False
    if(old_current_seal and old_current_seal!= new_current_seal):
        operation_success = unlock_a_txo(old_current_seal)
    if(old_next_seal and old_next_seal!= new_next_seal):
        operation_success = unlock_a_txo(old_next_seal)

    operation_success = lock_a_txo(new_current_seal)
    if(new_next_seal):
        operation_success = lock_a_txo(new_next_seal) #this may fail because we may not control the next seal

    return

def _confirm_creation(new_document):
    """ create the new document ok payload for the controller to send as response """
    try:
        #notice that the document sttus can be retrieved only after querying, at this stage we don't know its status
        response_object = {
            'status': 'success',
            'message': 'Successfully created.',
            'public_id': new_document.public_id
        }
        return response_object, 201
    except Exception as e:
        print(e)
        raise EonError(e, 500)


def _confirm_update(updated_document):
    """ create the updated document ok payload for the controller to send as response """
    try:
        # return the update result
        # if you want to see the status of the document you must query first, it's not available here
        response_object = {
            'status': 'success',
            'message': 'Successfully updated.',
            'public_id': updated_document.public_id
        }
        return response_object, 200
    except Exception as e:
        print(e)
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.',
            'details': e
        }
        return response_object, 401


def _confirm_delete():
    """ create the document destroyed ok payload for the controller to send as response """
    response_object = {
        'status': 'success',
        'message': 'Successfully deleted.',
    }
    return response_object, 200

def _confirm_operation(total_docs, exception_docs_ids):
    """ confirm the outcome of an rpc request for documents """
    response_object = {
        'status':'success',
        'message':"%s out of %s docs successfully processed, unhandled exceptions for doc ids: %s" % ((total_docs-len(exception_docs_ids)),total_docs,(",".join(exception_docs_ids)))
    }
    return response_object, 200
