import uuid
import datetime
import math
import traceback

from app.main.services import db
from app.main.util.crypto import cu
import app.main.util.schnorr as schnorr
import app.main.util.address as address
from app.main.model.dlcsession import DlcSession
from app.main.model.dlcoutcome import DlcOutcome
from app.main.util.eonerror import EonError
from app.main.service.dlcsession_service import get_a_dlcsession
from app.main.util.keymanagementutils import KeyManagementClient
#from app.main.util.decorator import log_error
# TODO: set the logging preference in the config file

from app.main.util.eonrust import create_p2sh_address, create_spending_tx

def save_new_dlcoutcome(data):
    """
    Save a new DLC session in the local db

    the same R point can be used for more than one session for now, although discouraged

    Parameters
    ----------
    data: dict
        an object with the following fields, as described in dto dlc_outcome
        'message', in clear message that will be signed by the oracle
        'winning_address', destination addres in case this will be the winning outcome
        'winning_pubkey', pub_key of the winner in case this will be the winning outcome


    Returns
    -------
        [dict, int]
            an array which contains:
            0: the response_object with the following fields: 'status' success/fail,
            'message' and in case of success the 'public_id' of the new txo
            1: the return code for the successfull operation, 201

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        40x: when there is something strange in the args (e.g. r out of bound)
    """
    try:

        #TODO: prepare public key of the mesage+pubkey

        #1: transform pubkey from BIP encoded to ECPoint
        A = address.deserialize_pubkey(data.get("winner_pubkey"))
        #2: transform message to message pubkey
        #dlcsession = get_a_dlcsession(data.get("dlc_session_id"))
        dlcsession = DlcSession.query.filter_by(public_id=data.get("dlc_session_id")).first() #can't use get because it reshapes k in hex format
        if(not dlcsession):
            raise EonError("Wrong DLC session id", 404)

        #check if dlc session already has its outcome and it's in the correct status:
        if dlcsession.status!="pending":
            raise EonError("DLC Session is already ongoing", 400)

        if(dlcsession and dlcsession.outcomes and len(dlcsession.outcomes)>=1):
            raise EonError("DLC Session already has an outcome", 400)

        O = address.deserialize_pubkey(dlcsession.oracle_pubkey)
        k_int = int.from_bytes(dlcsession.k, "big")
        R = schnorr.G.mul(k_int)
        n = schnorr.getCurve()[1]
        ei = (schnorr.hashThis(data.get("message"), R) %n)
        eiTimesOraclePublicKey = O.mul(ei)
        negEiTimesOraclePublicKey = eiTimesOraclePublicKey.negation()
        oiG = R.add(negEiTimesOraclePublicKey)
        #add the winner pubkey
        Aoig = A.add(oiG)
        hexpk_aoig = address.serialize_pubkey(Aoig)
        #get the p2sh address for this outcome (cashback to default key after timelock)
        addr = create_p2sh_address(hexpk_aoig.decode("utf-8"), data.get("default_pubkey"), str(data.get("locktime")) ) #locktime here must be string

        new_dlc_outcome = DlcOutcome(
            public_id=str(uuid.uuid4()),
            dlc_session_id=data.get("dlc_session_id"),
            message=data.get("message"),
            winner_address=data.get("winner_address"),
            winner_pubkey=data.get("winner_pubkey"),
            default_pubkey=data.get("default_pubkey"),
            locktime=data.get("locktime"), #locktime here must be int
            combined_pubkey=hexpk_aoig.decode("utf-8"),
            escrow_address=addr
        )
        _save_changes(new_dlc_outcome)
        response_object = {
            'status': 'success',
            'message': 'New Dlc Outcome registered.',
            'public_id': new_dlc_outcome.public_id
        }
        return response_object, 201
    except EonError as eerr:
        raise eerr
    except Exception as e:
        raise EonError('Something is wrong, e.g. invalid pubkey.'+str(traceback.format_exc()), 404)


def get_all_dlcoutcomes(args):
    """ 
    Get all the DLC sessions
    
    Parameters
    ----------
    args is requet.args passed from the API controller, a dictionary

    args.get("page"): str
        page number, will be cast to int
    args.get("page_size"): str
        records per page, will be cast to int

    Returns:
    --------
    dict
        a dict with the pagianted resources:
        'total_record_count', the total number of resources on the lcoal db
        'page_size', how many records per page
        'page', pg number
        'records', [obj], array of ORM objects

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        400: when there is something strange in the args (e.g. cannot cast to int)
    """
    try:

        local_dlc_outcomes = DlcOutcome.query.all()
        payload = {}
        payload['total_record_count']=len(local_dlc_outcomes)
        payload['page_size']= 20 if not args.get("page_size") else int(args.get("page_size"))
        payload['page']= 1 if not args.get("page") else int(args.get("page"))
        payload['records']=[]
        payload['page_size'] = payload['page_size'] if not payload['page_size'] > 200 else 200
        if(payload['page']<=0):
            payload['page']=1
        if(payload['page_size']*payload['page'] > payload['total_record_count']):
            #if indexes are out of bound, just return the last page
            payload['page'] = math.ceil(payload['total_record_count']/payload['page_size'])
        start_index = payload['page_size']*(payload['page']-1)
        end_index = (payload['page_size']*payload['page'])-1
        for outcome in local_dlc_outcomes[start_index:end_index]:
            payload['records'].append(outcome)
        return payload
    except ValueError as verr:
        # casting args to int fails
        raise EonError(verr, 400)
    except Exception as e:
        # get_utxos throws Exception when the node has connection problems for instance
        print(e)
        raise EonError(e, 500)

def get_a_dlcoutcome(public_id):
    """
    Returns details of a dlc outcome

    Parameters
    ----------
    public_id: str
        the id on the db of the desired dlc outcome

    Returns
    -------
    object
        the dlc outcome object

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is no matching public_id in the local db
    """
    dlcoutcome = DlcOutcome.query.filter_by(public_id=public_id).first()
    if not dlcoutcome:
        raise EonError('Dlc Outcome not found.', 404)
    return dlcoutcome

def update_an_outcome(data):
    """
    if an outcome is still pending and the session is still ongoing, update it's winner address

    Parameters
    ----------
    data: dict
        an object with the following fields, as described in dto dlc_outcome
        'message', in clear message that will be signed by the oracle
        'winning_address', destination addres in case this will be the winning outcome


    Returns
    -------
        [dict, int]
            an array which contains:
            0: the response_object with the following fields: 'status' success/fail,
            'message' and in case of success the 'public_id' of the new txo
            1: the return code for the successfull operation, 201

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is something strange in the args
        400: when the outcome cannot be changed
    """
    public_id = data.get("public_id")
    dlcoutcome = DlcOutcome.query.filter_by(public_id=public_id).first()
    if not dlcsession:
        raise EonError('Dlc Outcome not found.', 404)

    if(dlcoutcome.status!="pending"):
        raise EonError('Dlc Outcome cannot be changed.', 400)

    if(data.get("winner_pubkey") and data.get("winner_pubkey")!=dlcoutcome.winner_pubkey):
        raise EonError('Can only update the winner address.', 400)

    if(data.get("message") and data.get("message")!=dlcoutcome.message):
        raise EonError('Can only update the winner address.', 400)

    dlcoutcome.winner_address = data.get("winner_address")
    _save_changes(dlc_outcome)
    response_object = {
        'status': 'success',
        'message': 'Dlc outcome updated.',
        'public_id': new_dlc_outcome.public_id
    }
    return response_object, 201


def confirm_outcome(public_id, signed_message):
    """
    Add the signed message to the outcome

    check if the signed message is indeed the correct one (pub key, clear text)
    add the signed message to the object and chagne the state

    Parameters
    ----------
    public_id: str
        the id on the db of the desired dlc outcome
    signed_message: str
        the signed message by the oracle, hex representation

    Returns
    -------
    object
        the updated dlc outcome object

    Raises
    ------
    EonError:   
        409: wrong signature
        404: when there is no matching public_id in the local db

    """
    
    try:
        dlcoutcome = _check_message(public_id, signed_message)
        dlcoutcome.signed_message = signed_message
        _save_changes(dlcoutcome)
    except EonError as ee:
        raise ee
    except Exception as e:
        raise EonError("Internal Server Error while checking signature", 500)

    
    
    return dlcoutcome

def pay_outcome(public_id, *args):
    """
    Check that the message is there and is correct, then send the payment to the winning address

    the winner can execute this transaction on its own, we expect to use this either as a DEMO function or as a function called by the winner node which knows it's own private key

    Parameters
    ----------
    public_id: str
        the id on the db of the desired dlc outcome
    priv_int_winner: str (optional)
        standard wif encoded private key

    Returns
    -------
    object
        the updated dlc outcome object

    Raises
    ------
    EonError:   
        409: wrong signature
        404: when there is no matching public_id in the local db
    """
    try:
        dlcoutcome = _check_message(public_id)
        if not dlcoutcome:
            raise EonError("Dlc Outcome not found", 404)
        #dlcsession = get_a_dlcsession(dlcoutcome.dlc_session_id)
        dlcsession = DlcSession.query.filter_by(public_id=dlcoutcome.dlc_session_id).first() #can't use get because it reshapes k in hex format
        if not dlcsession:
            raise EonError("Realted DlcSession not found", 404)

        priv_int=0
        #if you don't have a priv int, then try to use the local node key
        if(len(args)==0):
            kmc = KeyManagementClient()
            priv_key = kmc._get_kmic()._get_key()
            priv_numbers = priv_key.private_numbers()
            priv_int = priv_numbers.private_value
        else:
            wif=args[0]
            priv_int = int.from_bytes(address.wif_to_privkey(wif), byteorder="big")

        #create the combined wif: signed message + priv int
        n = schnorr.getCurve()[1]
        message_int = int.from_bytes(bytes.fromhex(dlcoutcome.signed_message), byteorder="big")
        priv_ia = (message_int + priv_int)%n
        priv_bytes = address.int_to_32_bytes(priv_ia)
        wif = address.privkey_to_wif(priv_bytes)

        #TODO: check that the winner pubkey computed by the priv key matches, raise 409 if not

        #use the rust library to create the tx:
        escrow_txid = dlcsession.escrow_txid
        escrow_voutn = dlcsession.escrow_voutn
        fee_estimante = _estimate_fee(2); #2: number of blocks expected for confirmation
        sats_value = int(dlcsession.value*(10**8))
        hex_tx = create_spending_tx(wif, escrow_txid, str(escrow_voutn), str(sats_value), str(fee_estimante), dlcoutcome.combined_pubkey, dlcoutcome.default_pubkey, str(dlcoutcome.locktime))        
        final_tx = cu.spend_signed_hex_tx(hex_tx)
        dlcoutcome.sent_with_tx = final_tx
        _save_changes(dlcoutcome)
        return dlcoutcome

    except EonError as ee:
        raise ee
    except Exception as e:
        print(e) #I should catch here the node error and put the correct EE message: "Wrong private key, escrow address is still locked"
        if("mandatory-script-verify-flag-failed" in str(e)):
            raise EonError("Wrong private key, escrow address is still locked", 409)    
        if("non-BIP68-final" in str(e)):
            raise EonError("Wrong private key, escrow address is still locked", 409)  
        if("fee greater than value" in str(e)):
            raise EonError("Fee greater than value!", 500) 
        raise EonError("Internal Server Error while checking signature", 500)


def default_outcome(public_id, *args):
    """
    Try to spend the outcome with the default key (after the timelock)

    the default party can execute this transaction on its own, we expect to use this either as a DEMO function 
    or as a function called by the default node which knows it's own private key

    Parameters
    ----------
    public_id: str
        the id on the db of the desired dlc outcome
    priv_int_winner: str (optional)
        standard wif encoded private key

    Returns
    -------
    object
        the updated dlc outcome object

    Raises
    ------
    EonError:   
        409: wrong signature/timelock not passed
        404: when there is no matching public_id in the local db
    """
    try:
        #dlcoutcome = _check_message(public_id)
        dlcoutcome = DlcOutcome.query.filter_by(public_id=public_id).first()
        if not dlcoutcome:
            raise EonError("Dlc Outcome not found", 404)
        #dlcsession = get_a_dlcsession(dlcoutcome.dlc_session_id)
        dlcsession = DlcSession.query.filter_by(public_id=dlcoutcome.dlc_session_id).first() #can't use get because it reshapes k in hex format
        if not dlcsession:
            raise EonError("Realted DlcSession not found", 404)

        priv_int=0
        wif=""
        #if you don't have a priv int, then try to use the local node key
        if(len(args)==0):
            kmc = KeyManagementClient()
            priv_key = kmc._get_kmic()._get_key()
            priv_numbers = priv_key.private_numbers()
            priv_int = priv_numbers.private_value
        else:
            wif=args[0]

        #TODO: check that the default pubkey matches the given wif, raise 409 if not

        #use the rust library to create the tx:
        escrow_txid = dlcsession.escrow_txid
        escrow_voutn = dlcsession.escrow_voutn
        fee_estimante = _estimate_fee(2); #2: number of blocks expected for confirmation
        sats_value = int(dlcsession.value*(10**8)) #this could create some dust..!
        hex_tx = create_spending_tx(wif, escrow_txid, str(escrow_voutn), str(sats_value), str(fee_estimante), dlcoutcome.combined_pubkey, dlcoutcome.default_pubkey, str(dlcoutcome.locktime))        
        final_tx = cu.spend_signed_hex_tx(hex_tx)
        dlcoutcome.sent_with_tx = final_tx
        _save_changes(dlcoutcome)
        return dlcoutcome

    except EonError as ee:
        raise ee
    except Exception as e:
        if("mandatory-script-verify-flag-failed" in str(e)):
            raise EonError("Wrong private key, escrow address is still locked", 409)    
        if("non-BIP68-final" in str(e)):
            raise EonError("Timelock still active, wait more blocks", 409)  
        if("fee greater than value" in str(e)):
            raise EonError("Fee greater than value!", 500)    
        raise EonError("Internal Server Error while checking signature", 500)
    


def _check_message(*args):
    """
    Check the Schnorr signature, raise exception is something is wrong

    Parameters
    ----------
    public_id: str
        the id on the db of the desired dlc outcome
    signed_message: str (optional)
        the signed message by the oracle, hex representation, if this is missing, load the one present in the db

    Returns
    -------
    str
        the signed_message

    Raises
    ------
    EonError:   
        409: wrong signature
        404: when there is no matching public_id in the local db
    """
    public_id = args[0]
    dlcoutcome = DlcOutcome.query.filter_by(public_id=public_id).first()
    if not dlcoutcome:
        raise EonError('Dlc Outcome not found.', 404)

    signed_message=""
    if(len(args)==1):   
        signed_message=dlcoutcome.signed_message
    else:
        signed_message=args[1]

    #dlcsession = get_a_dlcsession(dlcoutcome.dlc_session_id)
    dlcsession = DlcSession.query.filter_by(public_id=dlcoutcome.dlc_session_id).first() #can't use get because it reshapes k in hex format
    m = dlcoutcome.message
    O = address.deserialize_pubkey(dlcsession.oracle_pubkey)
    k_int = int.from_bytes(dlcsession.k, "big")
    R = schnorr.G.mul(k_int)
    n = schnorr.getCurve()[1]

    #signed message s is an int, expressed as bytes in hex form
    s_bytes = bytes.fromhex(signed_message)
    s = int.from_bytes(s_bytes, "big")

    #verification:
    #sG = R - hash(R, m)O
    sG = schnorr.G.mul(s)
    e = (schnorr.hashThis(m, R) %n)
    eTimesPublicKey = O.mul(e)
    negETimesPublicKey = eTimesPublicKey.negation()
    expected_sG = R.add(negETimesPublicKey)

    if(sG.get_x() != expected_sG.get_x() or sG.get_y() != expected_sG.get_y()):
        raise EonError("Invalid signature", 409)

    return dlcoutcome

def _estimate_fee(blocks_before_confirm):
    """
    Call the RPC node and get an estimate for the fee
    """
    fee_estimante = cu.estimatesmartfee(blocks_before_confirm); #: number of blocks expected for confirmation
    average_tx_size = 0.35 #350 bytes
    sats_per_tx=100
    if(not "errors" in fee_estimante):
        #raise EonError("Fee estiamte failed: "+str(fee_estimante["errors"]), 500)
        sats_per_kB = float(fee_estimante["feerate"])*10**8;
        sats_per_tx = int(sats_per_kB*average_tx_size)
    if(sats_per_tx < 100):
        sats_per_tx = 100
    return sats_per_tx

def _save_changes(data):
    db.session.add(data)
    db.session.commit()
