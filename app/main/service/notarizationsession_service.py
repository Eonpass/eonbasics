import uuid
import datetime
import math

from app.main.services import db
from app.main.util.crypto import cu
from app.main.util.keymanagementutils import kmc
from app.main.model.notarizationsession import NotarizationSession
from app.main.model.notarizationitem import NotarizationItem
from app.main.util.eonutils import eonutils
from app.main.util.eonerror import EonError
from app.main.util.stamperutils import stamper

from app.main.service.document_service import send_a_document, save_new_document, get_a_document
from app.main.service.txo_service import get_all_txos, get_fresh_utxos


def save_new_notarization_session(data):
    """
    Inserts a new notarization session

    Parameters:
    -----------
    data: dict
        the body of the POST request

    Returns:
    --------
    [dict, int]
        an array containing
        0: a dict with the result of the operation and the id of the newly created doc
        1: the success code for the operation, 201

    Raises:
    -------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is something strange in the args (e.g. unkown previous session)
        409: conflict, duplicated previous session

    """
    if(data.get('previous_session_id', None)):
        prev_session = NotarizationSession.query.filter_by(public_id=data['previous_session_id']).first()
        if not prev_session:
            raise EonError('Unknown previous session', 404)
        conflict_session = NotarizationSession.query.filter_by(previous_session_id=data['previous_session_id']).first()
        if conflict_session:
            raise EonError('You already have a session open with the same previous session', 409)
    notartype = data.get('stamper_type', "ebsi")
    if notartype not in ["ebsi", "std"]:
        notartype = "ebsi"
    new_notarization_session = NotarizationSession(
        public_id=str(uuid.uuid4()),
        stamper_type=data.get('stamper_type', notartype),
        previous_session_id=data.get('previous_session_id', None),
    ) #all other fields are empty, there are not items yet
    _save_changes(new_notarization_session)
    return _confirm_creation(new_notarization_session)

def get_notarisation_items_ordered_by_id(session_id):
    """
    get all the items of a session, 

    be sure they are ordered by public_id, this is an unambigous way to get exact order (created_on may have the same timestamp in weird scenarios)
    this function is here to avoid ciruclar import (item_service uses session_service)
    """
    return NotarizationItem.query.filter_by(notarization_session_id=session_id).order_by(NotarizationItem.public_id.asc()).all()

def update_session(data):
    """
    Update the given session, in practice you can only remove or change the previous session if the session is still new
    """
    notarization_session = NotarizationSession.query.filter_by(public_id=data["public_id"]).first()
    if not notarization_session:
        raise EonError('Session not found', 404)
    if(notarization_session.status!="new"):
        raise EonError('Session not in the correct status', 409)

    if(data.get('previous_session_id', None)):
        conflict_session = NotarizationSession.query.filter_by(previous_session_id=data['previous_session_id']).first()
        if conflict_session:
            raise EonError('You already have a session open with the same previous session', 409)
        else:
            notarization_session.previous_session_id=data.get('previous_session_id', None)
    else:    
        notarization_session.previous_session_id=None 

    _save_changes(notarization_session)
    notarization_session = NotarizationSession.query.filter_by(public_id=data["public_id"]).first()
    return _confirm_update(notarization_session)
    


def lock_notarization_session(public_id):
    """
    Lock a session, create the merkle tree and it's not longer possible to add items 
    
    Use EonUtils to build the merkleroot, save it, then create the document for it
    """
    notarization_session = NotarizationSession.query.filter_by(public_id=public_id).first()
    if not notarization_session:
        raise EonError('Session not found', 404)
    stamper_type = notarization_session.stamper_type if notarization_session.stamper_type else 'STD'
    notarization_session = stamper.lock(public_id, stamper_type.upper())
    _save_changes(notarization_session)
    return _confirm_update(notarization_session)


def notarize_session(public_id):
    """
    Once a session is locked and ready to go, send the document

    If the keys are empty in the sender/receiver doc, the current node key will be used as signature

    Raises
    ------
    EonError
        409, notarisation session or document not in the correct state
        404, notarisation of related document not found
        500, crypto utils not responding
    """
    notarization_session = NotarizationSession.query.filter_by(public_id=public_id).first()
    if not notarization_session:
        raise EonError('Session not found', 404)
    if(notarization_session.status!="locked and waiting notarization"):
        raise EonError('Session not in the correct status', 409)

    stamper_type = notarization_session.stamper_type if notarization_session.stamper_type else 'STD'
    sent_with_tx = stamper.stamp(public_id, stamper_type.upper())
    notarization_session.notarization_txid = sent_with_tx
    _save_changes(notarization_session)
    return _confirm_update(notarization_session)

def extract_hash(item):
    return item.message_hash

def delete_notarization_session(public_id):
    """
    destroy a notarization, this of course doesn't change the state of txos on the blockchain
    """
    notarization_session = NotarizationSession.query.filter_by(public_id=public_id).first()
    if(notarization_session):
        _delete_object(notarization_session)
        return _confirm_delete()  # returns a 200
    else:
        raise EonError('Session not found.', 404)

def get_a_notarization_session(public_id):
    """
    get a single notarization session
    """
    return NotarizationSession.query.filter_by(public_id=public_id).first()

def get_all_notarization_sessions(args):
    """ 
    Get all the Notarization sessions
    
    Parameters
    ----------
    args is requet.args passed from the API controller

    args.get("page"): str
        page number, will be cast to int
    args.get("page_size"): str
        records per page, will be cast to int

    Returns:
    --------
    dict
        a dict with the pagianted resources:
        'total_record_count', the total number of resources on the lcoal db
        'page_size', how many records per page
        'page', pg number
        'records', [obj], array of ORM objects

    Raises
    ------
    EonError:   
        500: server error
        400: when there is something strange in the args (e.g. cannot cast to int)
    """
    try:

        local_notarization_sessions = NotarizationSession.query.all()
        payload = {}
        payload['total_record_count']=len(local_notarization_sessions)
        payload['page_size']= 20 if not args.get("page_size") else int(args.get("page_size"))
        payload['page']= 1 if not args.get("page") else int(args.get("page"))
        payload['records']=[]
        payload['page_size'] = payload['page_size'] if not payload['page_size'] > 200 else 200
        if(payload['page']<=0):
            payload['page']=1
        if(payload['page_size']*payload['page'] > payload['total_record_count']):
            #if indexes are out of bound, just return the last page
            payload['page'] = math.ceil(payload['total_record_count']/payload['page_size'])
        start_index = payload['page_size']*(payload['page']-1)
        end_index = (payload['page_size']*payload['page'])-1
        for notarizationsession in local_notarization_sessions[start_index:end_index]:
            payload['records'].append(notarizationsession)
        return payload
    except Exception as e:
        print(e)
        raise EonError(e, 500)

def get_availabe_tips():
    """
    Get the a notar session which is NOT prev_session, so that I can use it as prev_session for a new notar. session
    
    To make this efficient I should add a boolean field that keeps track whether a notar session is parent to another
    or at least have the relationship Parent -> Children between notarizations

    TODO: to be more specific, get the sessions with no children OR all children are not closed (meaning their current UTXO is not spent yet)
    """
    local_tips = NotarizationSession.query.filter(NotarizationSession.children==None).all()
    #TODO: filter out sessions which have a spent closed seals for whatever reason:
    return local_tips


def _confirm_creation(new_notarization_session):
    """ create the new session ok payload for the controller to send as response """
    try:
        response_object = {
            'status': 'success',
            'message': 'Successfully created.',
            'public_id': new_notarization_session.public_id
        }
        return response_object, 201
    except Exception as e:
        print(e)
        raise EonError(e, 500)

def _confirm_update(notarization_session):
    """ Confirm an udpate by showing all the fields """
    return notarization_session, 200

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _delete_object(data):
    """
    commit the delete of the object
    """
    db.session.delete(data)
    db.session.commit()

def _confirm_delete():
    """ create the document destroyed ok payload for the controller to send as response """
    response_object = {
        'status': 'success',
        'message': 'Successfully deleted.',
    }
    return response_object, 200