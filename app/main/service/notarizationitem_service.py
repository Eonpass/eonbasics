import uuid
import datetime
import math
import re
import json

from cryptography.hazmat.backends import default_backend 
from cryptography.hazmat.primitives import hashes

from app.main.services import db
from app.main.util.crypto import cu
from app.main.util.keymanagementutils import kmc
from app.main.model.notarizationitem import NotarizationItem
from app.main.util.eonutils import eonutils
from app.main.util.eonerror import EonError

from app.main.service.notarizationsession_service import get_a_notarization_session, get_notarisation_items_ordered_by_id, extract_hash
from app.main.service.document_service import get_a_document
from app.main.service.txo_service import get_a_txo

def save_new_notarization_item(data):
    """
    Inserts a new notarization item connected to a session

    Parameters:
    -----------
    data: dict
        the body of the POST request

    Returns:
    --------
    [dict, int]
        an array containing
        0: a dict with the result of the operation and the id of the newly created doc
        1: the success code for the operation, 201

    Raises:
    -------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is something strange in the args (e.g. unkown session)
        409: conflict, already locked sessions, etc..

    """
    
    notarization_session = get_a_notarization_session(data['notarization_session_id'])
    if not notarization_session:
        raise EonError('Unknown session', 404)
    if notarization_session.status!='new':
        raise EonError('Session is already locked', 409)

    #check that the message_hash is actually an hash
    data['message_hash'] = data['message_hash'].lower()
    match = re.search(r'\b[A-Fa-f0-9]{64}\b', data['message_hash'])
    if not match:
        raise EonError('Only SHA256 hashes accepted, 64 hex digits', 409)

    new_notarization_item = NotarizationItem(
        public_id=str(uuid.uuid4()),
        notarization_session_id=data['notarization_session_id'],
        message_hash=data['message_hash'],
        notes=data.get('notes', None),
        ebsi_notes = data.get('ebsi_notes', None),
        created_on=datetime.datetime.utcnow()
    ) 
    _save_changes(new_notarization_item)
    return _confirm_creation(new_notarization_item)

def get_a_notarization_item(public_id):
    """
    get a single notarization item
    """
    return NotarizationItem.query.filter_by(public_id=public_id).first()

def get_notarization_item_proof(public_id):
    """
    After a session is notarised, build the proof for this item and return it

    Returns
    -------
    [dict]
        ordered array of proof steps, each step has:
        level: the ordering of the steps
        item_of_interest: the item that is relevant to the proof, for level 0 this is the leaf
        concat_position: left or right, how the concatenation should be done, position of the item_of_interest
        concat_companion: the other item to concatenate to create the hash for the level
    """
    item = NotarizationItem.query.filter_by(public_id=public_id).first()
    if not item:
        raise EonError('Unknown item', 404)
    notarization_session = get_a_notarization_session(item.notarization_session_id)
    if not notarization_session:
        raise EonError('item linked to unknown session', 404)

    if notarization_session.status=="new":
        raise EonError("Lock the session first", 409)

    items = get_notarisation_items_ordered_by_id(notarization_session.public_id)
    merkle_leaves = list(map(extract_hash, items))

    proof_payload = {}
    proof = []
    if notarization_session.stamper_type=="ebsi":
        notes = item.ebsi_notes
        config = json.loads(notes)
        shipment_id = config.get("shipment_id")
        authorised_dids = config.get("auth_dids")
        digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        digest.update(shipment_id.encode('utf-8'))
        document_hash = "0x"+digest.finalize().hex()
        try:
            proof = eonutils.create_merkle_proof(item.message_hash, merkle_leaves, notarization_session.merkle_root)
            ebsi_env = if(os.environ.get("EBSI_ENV") != 'TEST'): 'pilot' else 'test'
            proof_payload = {
                "merkle_proof": proof,
                "merkle_item": item.message_hash,
                "notarised_item": item.message_hash,
                "notariser_pubkey": kmc.get_didkey_from_local_key(),
                "notarisation_txid": notarization_session.notarization_txid,
                "ebsi_document": "https://api-"+ebsi_env+".ebsi.eu/track-and-trace/v1/documents/"+document_hash
            }
        except EonError as ee:
            raise ee 
    else:
        try:
            proof = eonutils.create_merkle_proof(item.message_hash, merkle_leaves, notarization_session.merkle_root)

            #get the document
            doc = get_a_document(notarization_session.document_id, "true") #cached is fine, just need the seals
            current_seal = get_a_txo(doc.current_seal, "false")
            next_seal = get_a_txo(doc.next_seal, "true")
            #get the seals and the pre_signed_payload
            pre_signed_payload = eonutils.concatenate_hash_and_txos(doc.document_hash, 
                current_seal.txid if current_seal else "", 
                current_seal.voutn if current_seal else "", 
                next_seal.txid if next_seal else "", 
                next_seal.voutn if next_seal else "")

            proof_payload = {
                "merkle_proof": proof,
                "merkle_item": item.message_hash,
                "notarised_item": pre_signed_payload,
                "notariser_pubkey": doc.sender_public_key,
                "next_notariser_pubkey": doc.receiver_public_key,
                "signed_notarised_item": doc.sender_and_receiver_signed_hash,
                "notarisation_txid": current_seal.spent_with_tx
            }

        except EonError as ee:
            raise ee 

    return proof_payload


def _confirm_creation(new_notarization_session):
    """ create the new session ok payload for the controller to send as response """
    try:
        response_object = {
            'status': 'success',
            'message': 'Successfully created.',
            'public_id': new_notarization_session.public_id
        }
        return response_object, 201
    except Exception as e:
        print(e)
        raise EonError(e, 500)


def _save_changes(data):
    db.session.add(data)
    db.session.commit()
