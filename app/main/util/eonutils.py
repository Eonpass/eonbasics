from app.main.util.crypto import cu
from app.main.util.eonerror import EonError

class Singleton(object):
    _instance = None  # Keep instance reference

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance


class EonUtils(Singleton):
    """ Collection of methods used in the Eonpass protocol"""
    def concatenate_hash_and_txos(self, text, current_seal_txid, current_seal_voutn, next_seal_txid, next_seal_voutn):
        """ 
        get the formatted eonpass message: text followed by current and next seal details
        
        concatenation of different parts is done with ";", concatenation of txid and vout index with ","
        this is the convention for creating the message to be signed.

        Parameters
        ----------
        text: str
            the message, usuallt it's a hex string of a digest

        current_seal_txid: str
            txid of the utxo used as current seal

        current_seal_voutn: int
            voutn of the utxo used as current seal

        next_seal_txid: str
            txid of the utxo used as next seal

        next_seal_voutn: int
            voutn of the utxo used as next seal

        Returns
        -------
        str
            the concatenation of all the inputs

        Raises
        ------
        Exception
            when one of the parameters is missing

        """
        if(not current_seal_txid or not str(current_seal_voutn) or not next_seal_txid or not str(next_seal_voutn) or not text):
            raise Exception('Concatenation of hash and txos is missing arguments')
        message = text+';'+current_seal_txid+',' +str(current_seal_voutn)+';'+next_seal_txid+','+str(next_seal_voutn)
        return message

    def create_merkle_root(self, items):
        """
        create the merkle root for a list of items (each items has the message_hash property with the message to use in the leaves)

        As per BIP standard, if itmes are even, go on, if they are odd, duplicate the last leaf on any level

        Parameters
        ----------
        items: array[str]
            array of notarizationitem items, we expect them to be already hexstring of hashes

        Returns
        -------
        str
            the merkle root

        Raises
        ------
        Exception
            hashing not available, messages are not hashes, etc..

        """
        while len(items)!=1:
            #duplicate last if odd items:
            if len(items)%2 != 0:
                items.append(items[len(items)-1])
            temp = []
            for i in range(0,len(items),2):
                parent_value = bytes.fromhex(items[i])+bytes.fromhex(items[i+1]) #bytes concat
                parent_hash = cu.hexdigest(parent_value)
                temp.append(parent_hash)
            items=temp
        return items[0]

    def create_merkle_proof(self, leaf, items, root):
        """
        given a leaf and the items, build the merkle tree but also store the item concatenated with the leaf at each level -> produce the proof of inclusion

        Additionally, check if the root matches with the given one

        Parameters
        ----------
        leaf: str
            the starting leaf for which you want the proof
        items: array[str]
            array of notarizationitem items, we expect them to be already hexstring of hashes
        root: str
            the expected merkle root

        Returns
        -------
        [dict]
            ordered array of proof steps, each step has:
            level: the ordering of the steps
            item_of_interest: the item that is relevant to the proof, for level 0 this is the leaf
            concat_position: left or right, how the concatenation should be done, position of the item_of_interest
            concat_companion: the other item to concatenate to create the hash for the level

        Raises
        ------
        Exception
            hashing not available, messages are not hashes, etc..
        """
        operations = []
        item_of_interest = leaf
        concat_position = ''
        concat_companion = ''
        level = 0
        while len(items)!=1:
            #duplicate last if odd items:
            if len(items)%2 != 0:
                items.append(items[len(items)-1])
            temp = []
            for i in range(0,len(items),2):
                parent_value = bytes.fromhex(items[i])+bytes.fromhex(items[i+1]) #bytes concat
                parent_hash = cu.hexdigest(parent_value)

                if(items[i]==item_of_interest or items[i+1]==item_of_interest):
                    item_of_interest = items[i] if items[i]==item_of_interest else items[i+1]
                    concat_position = "left" if items[i]==item_of_interest else "right"
                    concat_companion = items[i+1] if items[i]==item_of_interest else items[i]
                    operations.append({'item_of_interest':item_of_interest, 'concat_position': concat_position, 'concat_companion': concat_companion, 'level':level})
                    item_of_interest=parent_hash
                    #item_of_interest is relevant only for the first level, where it's the leaf, later on it's just computed as the concat hash of the two items of the prev level
                temp.append(parent_hash)
            items=temp
            level+=1
        if(items[0]!=root):
            raise EonError ('Unexpected root, corrupted root '+root+' '+items[0], 500)
        return operations

    def execute_proof(self, proof, root, leaf):
        """
        Execute a proof and see if the root matches the desired one starting from the desired leaf. Raises exception in case of failure

        Parameters
        ----------
        proof: array[dict]
            array of notarizationitem steps, see create_merkle_proof
        leaf: str
            the starting leaf for which you want the proof
        root: str
            the expected merkle root

        Returns
        -------
        bool
            True is proof checks out

        Raises
        ------
        Exception EonError
            with details of why the proof failed
        """
        current_level = 0
        if(proof[0]["item_of_interest"]!=leaf):
            raise EonError("Wrong proof, the desired leaf is not included", 500)
        parent_hash = ''
        for step in proof:
            if step["level"]!=current_level:
                raise EonError("Corrupted proof, unordered levels", 500)
            if(parent_hash):
                if(step["item_of_interest"]!=parent_hash):
                    raise EonError("Corrupted proof, level "+str(current_level)+" has item_of_interest "+step["item_of_interest"]+" rather than "+parent_hash, 500)
            parent_value=''
            if(step["concat_position"]=="left"):
                parent_value = bytes.fromhex(step["item_of_interest"])+bytes.fromhex(step["concat_companion"])
            else:
                parent_value = bytes.fromhex(step["concat_companion"])+bytes.fromhex(step["item_of_interest"])
            parent_hash = cu.hexdigest(parent_value)
            current_level+=1

        if(parent_hash!=root):
            raise EonError("Corrupted proof, proof generated a different root "+parent_hash, 500)
        return True


eonutils = EonUtils()    

