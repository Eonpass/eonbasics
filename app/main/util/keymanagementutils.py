
from flask import current_app

from app.main.util.keymanagementclientfactory import KeyManagementClientFactory

class Singleton(object):
    _instance = None  # Keep instance reference

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance


class KeyManagementClient(Singleton):
    _kmic = None

    def _get_kmic(self):
        """ get the currently creates KMIC or request one to the factory """
        if not self._kmic:
            factory = KeyManagementClientFactory();
            self._kmic = factory.create(current_app.config['KMI_TYPE'])
        return self._kmic

    def sign_message(self, message):
        """
        sign a message with one of the keys avaialble on the node

        before doing the signature this methods checks if the message
        needs to be converted to bytes with "encode(utf-8)", pay attenion
        that signed messages and digests are passed in hex string 
        in the eonpass protocol.
        
        Parameters
        ----------
        message: str/bytes
            the message to be signed by the node, if str, it gets encoded first

        Returns
        --------
        dict
            {signed, serialized_public, address}, a JSON containind the
            signed messaged and its respective serialized public key and
            the address from which the confidential key was taken

        Raises
        ------
        Exception
            sign_bytes_message uses celery to retrieve keys from the node
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content

            if the message is not a string, not a bytes array,
            a general Exception is raised complaining about the message type
        """
        if isinstance(message, str):
            return self._get_kmic().sign_bytes_message(message.encode())
        elif isinstance(message, bytes):
            return self._get_kmic().sign_bytes_message(message)
        else:
            raise Exception('Corrupt message type')

    def sign_jwt(self, payload, headers):
        """
        Use the local priv key to sign a jwt

        Parameters
        ----------
        payload: dict, body of the jwt
        headers: dict, headers of the jwt
        alg: type of EC sig to use, can only be ES256 or ES256K (EBSI compatible)

        Returns
        -------
        str
            the decoded token
        """
        return self._get_kmic().sign_jwt(payload, headers)

    def verify_signed_message(self, signed, message, serialized_public):
        """
        check if a message and a signature are matching given the pub key 

        Parameters
        ----------
        signed: bytes
            the signed version

        message: bytes
            the original message that was signed

        serialized_public: bytes
            the public kley

        Returns
        --------
        bool
            True if the signed message check out, False otherwise.
            Note that anything going wrong is catched and 
            the method just returns False
        """
        return self._get_kmic().verify_signed_message(signed, message, serialized_public)

    def get_serialized_pub_key(self):
        """
        Get the serialised public key with PEM encoding of this eonpass node

        Params
        ------

        Returns
        -------
        bytes
            the serialised data with the PEM encoding

        Raises
        ------

        """
        return self._get_kmic().get_serialized_pub_key()

    def get_jwk_pubkey(self):
        """
        Get the local key but in JWK format

        Params
        ------

        Returns
        -------
        dict, json object with the pub key in jwk format compatible with EBSI

        """
        return self._get_kmic().get_jwk_pubkey()

    def get_eth_account(self):
        """
        Return the eth account for the local priv key

        Returns
        -------
        str, the eth address 
        """
        return self._get_kmic().get_eth_account()

    def sign_ebsi_tx(self, unsigned_tx):
        """
        Use the local key to sign the tx

        Returns
        -------
        str, the signed tx 
        """
        return self._get_kmic().sign_ebsi_tx(unsigned_tx)

    def overwrite_local_key(self, pemkey):
        """
        Overwrite the local pem file

        Params
        ------
        key: PEM encoded key
            the new key you want to use on this node

        Returns
        -------
        pubkey: PEM serialized key
            the enw public key of the node
        
        Raises
        ------
        EonError
            Somethign went wrong
        """
        return self._get_kmic().overwrite_local_key(pemkey)

    def convert_jwk_to_pem(self, key):
        """
        Transform a JWK into a classic PEM representation

        Params
        ------
        key: dict
            JSON of the JWK, it should have EC secpt256k1 and d. Double checks that x and y returns the same point

        Returns
        -------
        key: serialized priv key in pem format
        """
        return self._get_kmic().convert_jwk_to_pem(key)

    def get_didkey_from_local_key(self):
        """
        Build the did:key for the local key
        """
        return self._get_kmic().get_didkey_from_local_key()

kmc = KeyManagementClient()