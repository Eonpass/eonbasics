import app.main.util.schnorr as schnorr

# n is the order of the curve
n = schnorr.getCurve()[1]

class OracleCommitment:
    def __init__(self, k, R, V, signed_r):
        """
        Just a wrapper for the committed values
        """
        self.k=k
        self.R=R 
        self.V=V 
        self.signed_r=signed_r
        return

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)

class Oracle:
    def __init__(self):
        return

    def commit_to_point(priv_int):
        """
        Commit to a random int k and return the package [k,R,V, signed(r)] for the participants

        Choose a random point on the curve that will be used in the signatures with the private key v:
        k = nonce
        kG = R
        vG = V
        signed_r = k - hash(k, R)v

        the signed_r is an insurance that the oracle indeed commited to point R, this is non relevant
        for the signature and verification process, but it's helpful for participants that needs to agree
        on the parameters for the contract while they are setting it up.
        
        Parameters
        ----------
        priv_int: int
            private number of the Oracle

        Returns
        --------
        Object
            {k, R, V, signed(r)}, class containing
            - k, random nonce 256 bit int
            - R, EC Point
            - V, public key of the Oracle, EC Point
            - signed_r, 256 bit int
            
        Raises
        ------
        ValueError
            During math dragons may happen, we expect a general Excpetion can possilby occur
        """
        #V is the oracle public key
        V = schnorr.determine_pubkey(priv_int)
        #k nonce
        k = schnorr.generate_random()
        #R committed point
        R = schnorr.G.mul(k)
        e = (schnorr.hashThis(k, R) %n)
        #signed nonce
        signed_r = ((k - e*priv_int)%n)

        commitment = OracleCommitment(k,R,V,signed_r)
        return commitment.toJson()

    def sign(m, k, R, priv_int):
        """
        Sign a message m with the commited point R and private key

        s = k - hash(m, R)

        Parameters
        ----------
        m: str
            message to be signed
        R: [int, int]
            X,Y representation of the EC point
        priv_int: private number that will sign

        Returns
        -------
        int
            the signed message

        Raises
        ------
        ValueError
            During math dragons may happen, we expect a general Excpetion can possilby occur

        """
        V = schnorr.determine_pubkey(priv_int)
        e = (schnorr.hashThis(k, R) %n)
        s = ((k - e*priv_int)%n)
        return s

class Participant:
    def __init__(self):
        return

    def compute_publickey_for_message(R, m, V):
        """
        Given a message, a committed point and the oracle public key, prepare the message public key       

        sG = R - hash(m, R)V

        Parameters
        ----------
        R: ECPoint
            X,Y representation of the committed point
        m: str
            message
        V: ECPoint
            X,Y representation of the Oracle's public key 

        Returns
        -------
        ECPoint
            X,Y representation of the public key associated with the message

        Raises
        ------
        ValueError
            During math dragons may happen, we expect a general Excpetion can possilby occur
        
        """
        em = (schnorr.hashThis(m, R) %n)
        emTimesOraclePublicKey = V.mul(em)
        negEmTimesOraclePublicKey = emTimesOraclePublicKey.negation()
        sG = R.add(negEmTimesOraclePublicKey)
        return sG 

    def verify_signed_message(s, sG, R, m, V):
        """
        Once the Oracle returns a signed message, the particpants check is that it is indeed the ones he expectes

        sG = R - h(m, R)V

        Parameters
        ----------
        s: int
            signed message
        sG: ECPoint
            X,Y representation of the public key associated with the message
        R: ECPoint
            X,Y representation of the committed point
        m: str
            message that was signed
        V: ECPoint
            X,Y representation of the Oracle's public key

        Returns
        -------
        bool
            is it verified or not

        Raises
        ------
        ValueError
            During math dragons may happen, we expect a general Excpetion can possilby occur

        """    
        #check that the signed message returns the expected public key  
        expected_sG = schnorr.G.mul(s)
        fail = expected_sG.get_x()!=sG.get_x() or expected_sG.get_y()!=sG.get_y()
        if(fail):
            return False

        #check the signature
        em = (schnorr.hashThis(m, R) %n)
        emTimesOraclePublicKey = V.mul(em)
        negEmTimesOraclePublicKey = emTimesOraclePublicKey.negation()
        computed_sG = R.add(negEmTimesOraclePublicKey)
        #compare the stored sG with the computed one from the orcale signed message
        fail = computed_sG.get_x()!=sG.get_x() or computed_sG.get_y()!=sG.get_y()
        return not fail

    def compose_public_key(M, A):
        """
        A particpant creates public keys by adding two public keys, one from the message, one from his set

        MA = M + A
        
        Parameters
        ----------
        M: ECPoint
            X,Y representation of the public key associated with the message
        A: ECPoint
            X,Y representation of the participant's public key

        Returns
        -------
        MA:
            composed key

        Raises
        ------
        ValueError
            During math dragons may happen, we expect a general Excpetion can possilby occur

        """
        #should probably check that points are on the curve for safety (lol)
        MA = M.add(A)

    def deduce_and_check_private_key(s, priv_int, MA):
        """
        Once the particpant receives the signed message s, it can use it to creaste the priv key for MA

        priv_ma = s + priv_int

        Parameters
        ----------
        MA: ECPoint
            X,Y representation of the composed public key 
        s: int
            signed message by the Oracle
        priv_int: int
            private key of the participant

        Returns
        -------
        int:
            private number for the composed key MA

        Raises
        ------
        ValueError 
            If the composed key does not match the expected public key

        """
        priv_ma = (s + priv_int)%n
        expected_MA = schnorr.G.mul(priv_ma)

        fail = expected_MA.get_x()!=MA.get_x() or expected_MA.get_y()!=MA.get_y()
        if fail:
            raise ValueError("Composed public key MA does not match the composed priv_int + signed message key")

        return priv_ma


