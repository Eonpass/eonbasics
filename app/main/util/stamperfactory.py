from os import path

from app.main.util.keymanagementutils import kmc
from cryptography.exceptions import InvalidSignature 

from app.main.model.notarizationsession import NotarizationSession
from app.main.model.notarizationitem import NotarizationItem
from app.main.util.eonutils import eonutils
from app.main.service.document_service import send_a_document, save_new_document, get_a_document
from app.main.service.txo_service import get_all_txos, get_fresh_utxos
from app.main.util.ebsistamper import EbsiStamperBuilder


"""
This class manages the creation of the requested verifier

A verifier checks payloads, the STD type just inspects signature, advanced types can do custom functions like 
resolving DIDs and checking VCs.

"""
class StamperFactory:
    def __init__(self):
        self._builders = {}

    def register_builder(self, key, builder):
        self._builders[key] = builder

    def create(self, key, **kwargs):
        builder = self._builders.get(key)
        if not builder:
            if key == 'STD':
                builder = StandardStamperBuilder(**kwargs)
            elif key == 'EBSI':
                builder = EbsiStamperBuilder(**kwargs)
            else:
                raise Exception('Wrong or unsupported stamper type')
            #lazy build of the builder registry
            self.register_builder(key, builder)
        return builder(**kwargs)


class StandardStamperBuilder:
    def __init__(self, **_ignored):
        #TODO: **ignored params will be useful to decide which builder to build, standard is default
        self._instance = None

    def __call__(self, **ignored):
        if not self._instance:
            self._instance = StandardStamper()
        return self._instance


class StandardStamper:
    #basic verifier, will just check signatures against public keys, doesn't check external VC, etc..
    _type = 'STD'
    def __init__(self):
        """
        Standard Stamper doesn't need any init, it will just call eonbasics methods
        """
        print('STD Stamper Init')

    def lock(self, public_id):
        """
        Create the merkle tree with all the items of the notarisation session and save it

        Parameters
        ----------
        public_id: str
            public_id of the notarisation session to lock

        Returns
        --------
        obj
            notarisation session with updated fields

        Rasies
        ------
        EonError
            with speaking description of the error and http code
        """
        notarization_session = NotarizationSession.query.filter_by(public_id=public_id).first()
        if not notarization_session:
            raise EonError('Session not found', 404)

        #prepare the root
        items = NotarizationItem.query.filter_by(notarization_session_id=public_id).order_by(NotarizationItem.public_id.asc()).all()
        merkle_leaves = list(map(extract_hash, items))
        merkle_root = eonutils.create_merkle_root(merkle_leaves)
        notarization_session.merkle_root = merkle_root

        #get prev session is any:
        prev_session = NotarizationSession.query.filter_by(public_id=notarization_session.previous_session_id).first()
        current_seal = None
        next_seal = None 
        if(prev_session):
            prev_doc = get_a_document(prev_session.document_id, "true") #cached is fine, jsut need the id of the seal
            current_seal = prev_doc.next_seal

        #get the txos and create the document:
        fresh_utxos_count = 2 if not current_seal else 1
        utxos = get_fresh_utxos(fresh_utxos_count)
        current_seal = current_seal if current_seal else utxos[0]
        next_seal = utxos[0] if fresh_utxos_count==1 else utxos[1]

        doc_payload={
            'document_hash': merkle_root,
            'current_seal': current_seal,
            'next_seal':next_seal
        }
        doc_res = save_new_document(doc_payload)
        notarization_session.document_id=doc_res[0]["public_id"]
        return notarization_session

    def stamp(self, public_id):
        """
        notarise a session, send the hash to the respective blockchain

        The standard version uses elements as blockchain, other stampers may user other blockchains (e.g. EBSI)

        Parameters (STD)
        ----------
        public_id: str
            public_id of the notarisation session to lock

        Returns
        -------
        str 
            The txid of the related notarization
        """
        notarization_session = NotarizationSession.query.filter_by(public_id=public_id).first()
        if not notarization_session:
            raise EonError('Session not found', 404)
        sent_with_tx = send_a_document(notarization_session.document_id)
        return sent_with_tx

def extract_hash(item):
    return item.message_hash