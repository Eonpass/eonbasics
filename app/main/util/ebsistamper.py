from os import path
import json
import base58
import base64
import time

from app.main.util.keymanagementutils import kmc
from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.backends import default_backend 
from cryptography.hazmat.primitives import hashes

from app.main.model.notarizationsession import NotarizationSession
from app.main.model.notarizationitem import NotarizationItem
from app.main.util.eonutils import eonutils
from app.main.util.eonerror import EonError
from app.main.util.ebsi_client import EBSIClient


class EbsiStamperBuilder:
    def __init__(self, **_ignored):
        #TODO: **ignored params will be useful to decide which builder to build, standard is default
        self._instance = None

    def __call__(self, **ignored):
        if not self._instance:
            self._instance = EbsiStamper()
        return self._instance


class EbsiStamper:
    #basic verifier, will just check signatures against public keys, doesn't check external VC, etc..
    _type = 'EBSI'
    _client = None
    def __init__(self):
        """
        EBSI Stamper creates and EBSI Client
        """
        self._client = EBSIClient()

    def lock(self, public_id):
        """
        Create the merkle tree with all the items of the notarisation session and save it

        Parameters
        ----------
        public_id: str
            public_id of the notarisation session to lock

        Returns
        --------
        obj
            notarisation session with updated fields

        Rasies
        ------
        EonError
            with speaking description of the error and http code
        """
        notarization_session = NotarizationSession.query.filter_by(public_id=public_id).first()
        if not notarization_session:
            raise EonError('Session not found', 404)

        #prepare the root
        items = NotarizationItem.query.filter_by(notarization_session_id=public_id).order_by(NotarizationItem.public_id.asc()).all()
        merkle_leaves = list(map(extract_hash, items))
        merkle_root = eonutils.create_merkle_root(merkle_leaves)
        notarization_session.merkle_root = merkle_root

        return notarization_session

    def stamp(self, public_id):
        """
        notarise a session, send the hash to the respective blockchain

        The standard version uses elements as blockchain, EBSI uses tnt registry on EBSI

        Parameters (STD)
        ----------
        public_id: str
            public_id of the notarisation session to lock

        Returns
        -------
        str 
            The txid of the related notarization
        """
        notarization_session = NotarizationSession.query.filter_by(public_id=public_id).first()
        if not notarization_session:
            raise EonError('Session not found', 404)

        items = NotarizationItem.query.filter_by(notarization_session_id=public_id).order_by(NotarizationItem.public_id.asc()).all()
        if len(items)<1:
            raise EonError('Empty session', 404)
        notes = items[0].ebsi_notes
        config = json.loads(notes)
        shipment_id = config.get("shipment_id")
        authorised_dids = config.get("auth_dids")
        digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        digest.update(shipment_id.encode('utf-8'))
        document_hash = "0x"+digest.finalize().hex()

        did = self._client.get_imported_did() #did:ebsi imported from env variable, if empty, compute did:key from local key
        if not did:
            did = kmc.get_didkey_from_local_key()

        #EBSI FLOW:
        """
        #1) check if document with given hash exists on EBSI:
            1.1) get the item(s), in the note you'll find: shipment-id and additional did:key to authorise
            1.2) with the shipment-id, get the hash, and call GET on EBSI to see if doc exists already
        #- IT DOESN'T EXIST
            A.1) self authorise with VC (only brand owners can)
            A.2) create doc
            A.3) grant access to you and all other did:key in the items
        # - IT EXISTS
            B.1) assume someone created it and granted access to you

        #2) writeEvent with the merkle root value of the notarization session
            2.1) get the write access token
            2.2) add the event to the document
        """
        #1) check doc
        doc = self._client.get_document(document_hash)
        print(doc)
        nonce_delta = 0
        if not doc or not doc.get("metadata"):
            #A) - try to create the doc from scratch
            try:
                print("#1A")
                vc = self._client.get_imported_vc()
                print(did)
                print(vc)
                if not vc:
                    raise EonError('Please import your onboarding VC as an env variable before trying to create documents on EBSI', 404) 
                #authorise yourself on the tnt service
                print("---------------------------------------SELF AUTHORISE")
                access_token = self._client.get_tnt_access_token(vc, did)
                print("ACCESS TOKEN OK", access_token)
                if not access_token:
                    raise EonError('Expired or invalid VC', 500)
                authorise_usigned_tx = self._client.create_authorise_did_unsignedtx(access_token, did, did)
                print("UNSIGNED TX OK")
                signed_tx = self._client._sign_unsigned_tx(authorise_usigned_tx)
                confirmation_tx = self._client._send_signed_tx(access_token, authorise_usigned_tx, signed_tx)
                print("SIGNED TX OK")
                nonce_delta += 1
                time.sleep(60)
                #once did is AUTH, create doc
                print("---------------------------------------CREATE_NEW DOC")
                access_token = self._client.get_tnt_create_token(did)
                unsigned_tx = self._client.create_new_document_unsignedtx(document_hash, access_token, did)
                print(unsigned_tx)
                hex_value = int(unsigned_tx["nonce"], 16)
                #hex_value += nonce_delta
                unsigned_tx["nonce"] = hex(hex_value)
                signed_tx = self._client._sign_unsigned_tx(unsigned_tx)
                confirmation_tx = self._client._send_signed_tx(access_token, unsigned_tx, signed_tx)
                nonce_delta += 1
                time.sleep(16)
                #grant access to your did and other dids as required
                print("---------------------------------GET TNT WRITE ACCESS TOKEN FOR GRANTING ACCESS:")
                access_token = self._client.get_tnt_write_token(did)
                targets = [did]
                for auth_did in authorised_dids:
                    contructed_key = self._extract_pubkey_from_didkey(auth_did)
                    targets.append(contructed_key)
                for target_did in targets:
                    print("---------------------------------GRANTING ACCESS TO: ", target_did)
                    grant_access_unsignedtx = self._client.create_grant_access_unsignedtx(document_hash, target_did, access_token, did)
                    hex_value = int(grant_access_unsignedtx["nonce"], 16)
                    hex_value += nonce_delta
                    grant_access_unsignedtx["nonce"] = hex(hex_value)
                    signed_tx = self._client._sign_unsigned_tx(grant_access_unsignedtx)
                    confirmation_tx = self._client._send_signed_tx(access_token, grant_access_unsignedtx, signed_tx)
                    nonce_delta += 1
                time.sleep(16)
            except Exception as e:
                raise EonError('Currently the node cannot create documents on EBSI, detail: '+str(e), 500)
        #2) go on, doc exists now, write the event:
        print("------------------------------------------#2")
        access_token = self._client.get_tnt_write_token(did) #if did:key, keyId for the jwt header is did:key;zYOURKEY#YOURKEY, repeat the did
        event_hash = notarization_session.merkle_root if notarization_session.merkle_root.startswith("0x") else "0x"+notarization_session.merkle_root
        writer_did = did if "ebsi" in did else self._extract_pubkey_from_didkey(did)
        unsigned_tx = self._client.create_new_event_unsignedtx(document_hash,event_hash, access_token, writer_did) #did if did:ebsi, 0x+x+y is did:key
        hex_value = int(unsigned_tx["nonce"], 16)
        hex_value += nonce_delta
        unsigned_tx["nonce"] = hex(hex_value)
        signed_tx = self._client._sign_unsigned_tx(unsigned_tx)
        confirmation_tx = self._client._send_signed_tx(access_token, unsigned_tx, signed_tx)

        return confirmation_tx

    def _extract_pubkey_from_didkey(self, didkey):
        jwk_json_bytes = base58.b58decode(didkey[9:]) #remove "did:key:z" prefix
        jwk_json = jwk_json_bytes[3:] # Remove the special prefix bytes
        jwk = json.loads(jwk_json)
        #now extract x and y coordinates from base64url encoding
        padding_factor = (4 - len(jwk['x']) % 4) % 4
        padded_x = jwk['x']+ '='*padding_factor
        x_bytes = base64.urlsafe_b64decode(padded_x)
        padding_factor = (4 - len(jwk['y']) % 4) % 4
        padded_y = jwk['y']+ '='*padding_factor
        y_bytes = base64.urlsafe_b64decode(padded_y)
        constructed = x_bytes+y_bytes
        return ("0x"+constructed.hex())

def extract_hash(item):
    return item.message_hash
