
from flask import current_app

from app.main.util.stamperfactory import StamperFactory

class Singleton(object):
    _instance = None  # Keep instance reference

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance


class Stamper(Singleton):

    def __init__(self):
        self._stampers = {}

    def register_stamper(self, key, stamper):
        self._stampers[key] = stamper

    def _get_stamper(self, stamper_type='STD'):
        """
        Different sessions may have different stampers, create a dictionary of them
        """
        #force default if missing
        if not stamper_type:
            stamper_type='STD'
        if not self._stampers.get(stamper_type, None):
            factory = StamperFactory()
            stamper = factory.create(stamper_type)
            self.register_stamper(stamper_type, stamper)
        return self._stampers.get(stamper_type)

    def lock(self, public_id, stamper_type='STD'):
        """
        Create the merkle tree with all the items of the notarisation session and save it

        Parameters
        ----------
        public_id: str
            public_id of the notarisation session to lock

        stamper_type: str
            type of stamper "STD" is the standard for elements blockchain

        Returns
        --------
        obj
            notarisation session with updated fields
        """
        return self._get_stamper(stamper_type).lock(public_id)

    def stamp(self, public_id, stamper_type='STD'):
        """
        notarise a session, send the hash to the respective blockchain

        The standard version uses elements as blockchain, other stampers may user other blockchains (e.g. EBSI)

        Parameters (STD)
        ----------
        public_id: str
            public_id of the notarisation session to lock

        stamper_type: str
            type of stamper "STD" is the standard for elements blockchain

        Returns
        -------
        str 
            The txid of the related notarization
        """
        return self._get_stamper(stamper_type).stamp(public_id)


stamper = Stamper()
