import os
import requests
import uuid
import jwt
import urllib.parse
import json
import binascii
from datetime import datetime, timedelta

from app.main.util.keymanagementutils import KeyManagementClient

class EBSIClient:
    def __init__(self):
        self.open_id_config_url = 'https://api-test.ebsi.eu/authorisation/v4/.well-known/openid-configuration'
        self.did_registry_url = 'https://api-test.ebsi.eu/did-registry/v5'
        self.ti_registry_url = 'https://api-test.ebsi.eu/trusted-issuers-registry/v4'
        self.ts_registry_url = 'https://api-test.ebsi.eu/trusted-schemas-registry/v2'
        self.tnt_url = 'https://api-test.ebsi.eu/track-and-trace/v1'
        if(os.environ.get("EBSI_ENV") != 'TEST'):
            self.set_pilot_urls() #default to pilot

    def set_open_id_config_url(self, url):
        self.open_id_config_url = url

    def set_did_registry_url(self, url):
        self.did_registry_url = url

    def set_ti_registry_url(self, url):
        self.ti_registry_url = url

    def set_ts_registry_url(self, url):
        self.ts_registry_url = url

    def set_tnt_url(self, url):
        self.tnt_url = url

    def set_pilot_urls(self):
        self.open_id_config_url = 'https://api-pilot.ebsi.eu/authorisation/v4/.well-known/openid-configuration'
        self.did_registry_url = 'https://api-pilot.ebsi.eu/did-registry/v5'
        self.ti_registry_url = 'https://api-pilot.ebsi.eu/trusted-issuers-registry/v5'
        self.ts_registry_url = 'https://api-pilot.ebsi.eu/trusted-schemas-registry/v2'
        self.tnt_url = 'https://api-pilot.ebsi.eu/track-and-trace/v1'


    def get_imported_vc(self):
        return os.environ.get("EBSI_FULL_ONBOARDING_VC_FOR_YOUR_DID")

    def get_imported_did(self):
        return os.environ.get("YOUR_EBSI_DID")

    def get_open_id_config(self):
        """
        Call openId and get the config

        Returns:
            json, the open id config
        """
        try:
            print(self.open_id_config_url)
            response = requests.get(self.open_id_config_url)
            response.raise_for_status()  # Raise an exception for HTTP errors (4xx or 5xx)
            return response.json()  # Assuming the response is JSON
        except requests.exceptions.RequestException as e:
            print(f"An error occurred: {e}")
            return None

    def get_document(self, document_hash):
        """
        get a document from the tnt 
        """
        try:
            response = requests.get(self.tnt_url+"/documents/"+document_hash) 
            response.raise_for_status()  # Raise an exception for HTTP errors (4xx or 5xx)
            return response.json()  # Assuming the response is JSON
            """
            {"metadata":"0x00","timestamp":{"datetime":"0x65cffc04","source":"block","proof":"0x0000000000000000000000000000000000000000000000000000000000a1659d"},"events":["0xcb8b8717b51232e2fda9b865f6c3f93253cabb8c5a86d100536f903f8d09a704"],"creator":"did:ebsi:zbccVnwoJhCf3yku5EPqPqL"}
            """
        except requests.exceptions.RequestException as e:
            print(f"An error occurred: {e}")
            return None

    def create_onboarding_vc(self, issuer_did_id, target_did_id, key ="", vc_schema_url="https://api-test.ebsi.eu/trusted-schemas-registry/v2/schemas/z3MgUFUkb722uq4x3dv5yAJmnNmzDFeK5UC8x83QoeLJM", tao_attribute_url="https://api-test.ebsi.eu/trusted-issuers-registry/v4/issuers/did:ebsi:zZfWt82jcWoAf4b3JCncjZG/attributes/8a716077441af2a10dc36af22445177b3ea86774722a6904c2d17cc26b1f8599"):
        """
        Create an onboarding VC

        Args:
            vc (String): verifiable credential created for your DID
            did (String): did id "did:ebsi:z...", no need for the full json body
            key (dict): jwk key, if empty, load the local one
        """
        return ""

    def get_tnt_access_token(self, vc, did_id, key=""):
        """
        Call ebsi auth and get an onboarding access token

        Args:
            vc (String): verifiable credential created for your DID
            did (String): did id "did:ebsi:z...", no need for the full json body
            key (dict): jwk key, if empty, load the local one
        """
        kmc = KeyManagementClient()
        jwk = kmc.get_jwk_pubkey()
        key_id = jwk["kid"]
        config = self.get_open_id_config()
        audience = config["issuer"]
        token_endpoint = config["token_endpoint"]
        current_time = datetime.now()
        one_year_later = current_time + timedelta(days=365)
        one_second_before = current_time - timedelta(seconds=1)
        jti = "urn:did:" + str(uuid.uuid4())
        vp_payload = {
            "id": jti,
            "@context":["https://www.w3.org/2018/credentials/v1"],
            "type":["VerifiablePresentation"],
            "holder":did_id,
            "verifiableCredential":[vc]
        }

        jwt = self._build_and_sign_jwt(vp_payload, jti, did_id, did_id, current_time, one_year_later, one_second_before, audience, key_id)
        print(jwt)
        definition_id = "tnt_authorise_presentation"
        scope = "openid tnt_authorise"
        pres_submission = self._build_presentation_sub(definition_id)
        print("JWT")
        print(jwt)
        print("PRES_SUB")
        print(pres_submission)
        return self._get_access_token_from_endpoint(scope, jwt, pres_submission, token_endpoint)

    def get_tnt_create_token(self, did_id, key=""):
        """
        Call ebsi auth and get an onboarding access token

        Args:
            did (String): did id "did:ebsi:z...", no need for the full json body
            key (dict): jwk key, if empty, load the local one
        """
        kmc = KeyManagementClient()
        jwk = kmc.get_jwk_pubkey()
        key_id = jwk["kid"]
        config = self.get_open_id_config()
        audience = config["issuer"]
        token_endpoint = config["token_endpoint"]
        current_time = datetime.now()
        one_year_later = current_time + timedelta(days=365)
        one_second_before = current_time - timedelta(seconds=1)
        jti = "urn:did:" + str(uuid.uuid4())
        vp_payload = {
            "id": jti,
            "@context":["https://www.w3.org/2018/credentials/v1"],
            "type":["VerifiablePresentation"],
            "holder":did_id,
            "verifiableCredential":[]
        }

        jwt = self._build_and_sign_jwt(vp_payload, jti, did_id, did_id, current_time, one_year_later, one_second_before, audience, key_id)
        print(jwt)
        definition_id = "tnt_create_presentation"
        scope = "openid tnt_create"
        pres_submission = self._build_presentation_sub(definition_id)
        return self._get_access_token_from_endpoint(scope, jwt, pres_submission, token_endpoint)

    def get_tnt_write_token(self, did_id):
        """
        Call ebsi auth and get an onboarding access token

        Args:
            did (String): did id "did:ebsi:z...", no need for the full json body
            key (dict): jwk key, if empty, load the local one
        """
        kmc = KeyManagementClient()
        jwk = kmc.get_jwk_pubkey()
        key_id = jwk["kid"]
        config = self.get_open_id_config()
        audience = config["issuer"]
        token_endpoint = config["token_endpoint"]
        current_time = datetime.now()
        one_year_later = current_time + timedelta(days=365)
        one_second_before = current_time - timedelta(seconds=1)
        jti = "urn:did:" + str(uuid.uuid4())
        vp_payload = {
            "id": jti,
            "@context":["https://www.w3.org/2018/credentials/v1"],
            "type":["VerifiablePresentation"],
            "holder":did_id,
            "verifiableCredential":[]
        }
        #the key id to put in the jwt header changes according to the did:ebsi or did:key type, this is handled by the _build_and_sign_jwt:
        jwt = self._build_and_sign_jwt(vp_payload, jti, did_id, did_id, current_time, one_year_later, one_second_before, audience, key_id)
        print(jwt)
        definition_id = "tnt_write_presentation"
        scope = "openid tnt_write"
        pres_submission = self._build_presentation_sub(definition_id)
        return self._get_access_token_from_endpoint(scope, jwt, pres_submission, token_endpoint)

    def create_authorise_did_unsignedtx(self, access_token, target_did_id, approver_did_id, approver_key=""):
        """
        Authorise a did to create documents

            access_token (String): access token obtained by the previous step
            target_did_id (String): did id "did:ebsi:z...", no need for the full json body, to be authorised
            approver_did_id (String): did id of the auhtoriser who just got the access_token
            key (dict): jwk key, if empty, load the local one
        """
        print("CREATE AUTH UNSIGNED TX")
        kmc = KeyManagementClient()
        eth_address = kmc.get_eth_account()
        payload = {
            "jsonrpc":"2.0",
            "method":"authoriseDid",
            "params":[
                {
                    "from": eth_address,
                    "senderDid": approver_did_id,
                    "authorisedDid": target_did_id,
                    "whiteList":True
                }
            ],
            "id":str(uuid.uuid4())
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        }
        url = self.tnt_url + '/jsonrpc'
        print(payload)
        response = requests.post(url, headers=headers, json=payload)
        print(response)
        print("Status Code:", response.status_code)
        print("Response Body:", response.text)
        post_response_body = response.json()
        return post_response_body.get("result")

    def create_new_document_unsignedtx(self, document_hash, access_token, did_id):
        """
        Get an unsigned tx from EBSI for the "new document"

        """
        kmc = KeyManagementClient()
        eth_address = kmc.get_eth_account()
        did_in_hex = "0x"+binascii.hexlify(did_id.encode('utf-8')).decode('utf-8')
        payload = {
            "jsonrpc":"2.0",
            "method":"createDocument",
            "params":[
                {
                    "from": eth_address,
                    "documentHash": document_hash,
                    "documentMetadata":"0x00",
                    "didEbsiCreator":did_id
                }
            ],
            "id":str(uuid.uuid4())
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        }
        url = self.tnt_url + '/jsonrpc'
        response = requests.post(url, headers=headers, json=payload)
        print("Endpoint: ", url)
        print("Payload:", payload)
        print("Status Code:", response.status_code)
        print("Response Body:", response.text)
        print("Response Headers:", response.headers)
        post_response_body = response.json()
        return post_response_body.get("result")

    def create_new_event_unsignedtx(self, document_hash, event_hash, access_token, did_id):
        """
        Get an unsigned tx from EBSI for the "new document"

        """
        kmc = KeyManagementClient()
        eth_address = kmc.get_eth_account()
        did_in_hex = "0x"+binascii.hexlify(did_id.encode('utf-8')).decode('utf-8') if did_id[:2] != "0x" else did_id
        payload = {
            "jsonrpc":"2.0",
            "method":"writeEvent",
            "params":[
                {
                    "from": eth_address,
                    "eventParams":{
                        "documentHash": document_hash,
                        "externalHash": event_hash,
                        "origin":"0x00",
                        "metadata":"0x00",
                        "sender":did_in_hex
                    }
                }
            ],
            "id":str(uuid.uuid4())
        }
        print(payload)
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        }
        url = self.tnt_url + '/jsonrpc'
        response = requests.post(url, headers=headers, json=payload)
        print("Status Code:", response.status_code)
        print("Response Body:", response.text)
        print("Response Headers:", response.headers)
        post_response_body = response.json()
        return post_response_body.get("result")

    def create_grant_access_unsignedtx(self, document_hash, target_did_id, access_token, did_id):
        """
        Get an unsigned tx from EBSI for the "new document"

        """
        kmc = KeyManagementClient()
        eth_address = kmc.get_eth_account()
        did_in_hex = "0x"+binascii.hexlify(did_id.encode('utf-8')).decode('utf-8')
        subject_in_hex = "0x"+binascii.hexlify(target_did_id.encode('utf-8')).decode('utf-8') if target_did_id[:2] != "0x" else target_did_id
        payload = {
            "jsonrpc":"2.0",
            "method":"grantAccess",
            "params":[
                {
                    "from": eth_address,
                    "documentHash": document_hash,
                    "grantedByAccount": did_in_hex,
                    "subjectAccount": subject_in_hex,
                    "grantedByAccType": "0x00", #only brand owners can grant?
                    "subjectAccType": "0x00" if "ebsi" in target_did_id else "0x01",
                    "permission": "0x01"
                }
            ],
            "id":str(uuid.uuid4())
        }
        print(payload)
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        }
        url = self.tnt_url + '/jsonrpc'
        response = requests.post(url, headers=headers, json=payload)
        print("Status Code:", response.status_code)
        print("Response Body:", response.text)
        print("Response Headers:", response.headers)
        post_response_body = response.json()
        return post_response_body.get("result")

    def _sign_unsigned_tx(self, unsigned_tx):
        """
        Sign an unsigned tx with the local key
        """
        kmc = KeyManagementClient()
        signed_tx = kmc.sign_ebsi_tx(unsigned_tx)
        return signed_tx

    def _build_and_sign_jwt(self, vp_payload, jti, issuer_did_id, subject_did_id, iat_time, exp_time, nbf_time, audience, key_id):
        vp = {
            "jti": jti,
            "iss": issuer_did_id,
            "sub": subject_did_id,
            "nbf": nbf_time,
            "iat": iat_time,
            "exp": exp_time,
            "aud": audience,
            "nonce": str(uuid.uuid4()),
            "vp":vp_payload
        }
        parts = issuer_did_id.split(":")
        headers = {
            "alg":"ES256K",
            "typ":"JWT",
            "kid": issuer_did_id+"#"+key_id if "ebsi" in issuer_did_id else issuer_did_id+"#"+parts[2] #did:keys just need to repeat the did twice
        }
        kmc = KeyManagementClient()
        return kmc.sign_jwt(vp, headers)
        
    def _build_presentation_sub(self, definition_id):
        switch_cases = {
            "didr_invite_presentation": "didr_invite_credential",
            "didr_write_presentation": "didr_write_credential",
            "tir_invite_presentation": "tir_invite_credential",
            "tir_write_presentation": "tir_write_credential",
            "tnt_authorise_presentation": "tnt_authorise_credential",
            "tnt_create_presentation": "tnt_create_credential"
        }
        credential_id = switch_cases.get(definition_id, None)
        descriptor_map_list = []
        if "invite" in definition_id or "authorise" in definition_id:
            descripto_map_dict = {
                "id":credential_id,
                "format": "jwt_vp",
                "path": "$",
                "path_nested":{
                    "id": credential_id,
                    "format":"jwt_vc",
                    "path": "$.vp.verifiableCredential[0]"
                }
            }
            descriptor_map_list.append(descripto_map_dict)
        pres_sub = {
            "id": str(uuid.uuid4()),
            "definition_id": definition_id,
            "descriptor_map": descriptor_map_list
        }
        return pres_sub

    def _get_access_token_from_endpoint(self, scope, jwt, pres_submission, token_endpoint):
        parameters = {
            "grant_type": "vp_token",  # vp_token for real, authorization_code for mock-auth
            "scope": scope,
            "vp_token": jwt,  # obtained from step 2 above
            "presentation_submission": json.dumps(pres_submission)
        }
        form_data = urllib.parse.urlencode(parameters)
        url = token_endpoint
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        response = requests.post(url, data=form_data, headers=headers)
        if response.status_code == 200:
            json_response = response.json()
            access_token = json_response.get("access_token")
            if access_token:
                return access_token
            else:
                # Access token not found in response
                print("Error: Access token not found in response")
        else:
            # Handle error response
            print("Error:", response.status_code, response.text)

    def _send_signed_tx(self, access_token, unsigned_tx, signed_tx):
        payload = {
            "jsonrpc":"2.0",
            "method":"sendSignedTransaction",
            "id":str(uuid.uuid4()),
            "params":[
                {
                "protocol":"eth",
                "unsignedTransaction":unsigned_tx,
                "r":hex(signed_tx.r),
                "s":hex(signed_tx.s),
                "v":hex(signed_tx.v),
                "signedRawTransaction":"0x" + binascii.hexlify(signed_tx.rawTransaction).decode("utf-8")
                }
            ]
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        }
        url = self.tnt_url + '/jsonrpc'
        response = requests.post(url, headers=headers, json=payload)
        print("Endpoint: ", url)
        print("Payload: ", payload)
        print("Status Code:", response.status_code)
        print("Response Body:", response.text)
        print("Response Headers:", response.headers)
        post_response_body = response.json()
        return post_response_body.get("result")
