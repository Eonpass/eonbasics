from flask_restplus import Namespace, fields

"""
Data Transfer Objects

these are used by the controllers to create the swagger documentation
and to cast the dict received by the services into their respective JSONs

"""

class UserDto:
    api = Namespace('user', description='user related operations')
    user = api.model('user', {
        'email': fields.String(required=True,
                               description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'public_id': fields.String(description='user Identifier')
    })
    new_user = api.model('new_user', {
        'email': fields.String(required=True,
                               description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'password': fields.String(required=True, description='user password')
    })

class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True,
                                  description='The user password'),
    })

class TxoDto:
    api = Namespace('txo', description='txo related operations')
    txo = api.model('txo', {
        'public_id': fields.String(description='document Identifier'),
        'txid': fields.String(required=True,
                              description='The tx id of the output'),
        'voutn': fields.Integer(required=True,
                                description='The index in the output array '
                                            'of the tx'),
        'value': fields.Fixed(decimals=8, require=False,
                              description='Value of the specified output'),
        'last_checked_status': fields.String(required=False,
                                             description='Status of the txo '
                                                         'last time it was '
                                                         'checked - spent or '
                                                         'unspent'),
        'last_checked_time': fields.DateTime(dt_format=u'iso8601',
                                             required=False,
                                             description='Last time this txo '
                                                         'was checked on the '
                                                         'blockchain'),
        'last_checked_blockhash': fields.String(required=False,
                                             description='Most recent blockchash last time the txo was checked'),
        'spent_with_tx': fields.String(required=False, description='txid which spent this output, aka notarisation tx')
    })
    new_txo = api.model('new_txo', {
        'txid': fields.String(required=True,description='The tx id of the output'),
        'voutn': fields.Integer(required=True,description='The index in the outputs array of the tx'),
    })
    paged_txo_list = api.model('paged_txo_list', {
        'page': fields.Integer(description='Page number in the paged list'),
        'page_size': fields.Integer(description='Records per page'),
        'total_record_count': fields.Integer(description='Total records in the list'),
        'records':fields.List(fields.Nested(txo)),
    })
    spend_txo = api.model('spend_txo', {
        'amount': fields.String(required=False,description='The amount to spend out of the txo amount, defaults to all'),
        'msg_hex': fields.String(required=False,description='The hex representation of a string to put in the OP_RETURN'),
        'target_address': fields.String(required=False,description='The addres where to send amount, the rest is sent to the change address'),
    })

class DocumentDto:
    api = Namespace('document', description='document related operations')
    document = api.model('document', {
        'public_id': fields.String(description='document Identifier'),
        'document_hash': fields.String(
            required=True, description='hash of the document to be notarised'),
        'current_seal': fields.String(
            required=True, description='id of the txo which is current seal'),
        'status':fields.String(
            required=False, description='status of the document', readOnly=True),
        'next_seal': fields.String(
            required=False, description='id of the txo which is next seal'),
        'document_with_seals_hash': fields.String(
            required=False, description='document concatenated with the seals detail and hashed, this is the field to be signed by sender and receiver'),
        'sender_signed_hash': fields.String(
            required=False, description='document hash signed by the sender '
                                        'with the respective public key'),
        'sender_public_key': fields.String(
            required=False, description='public key used to check if the '
                                        'sender actually signed the message'),
        'sender_and_receiver_signed_hash': fields.String(
            required=False, description='document hash signed by the sender '
                                        'and subsequently by the receiver '
                                        'with the respective public key'),
        'receiver_public_key': fields.String(
            required=False, description='public key used to check if the '
                                        'receiver actually signed the message')
    })
    new_document = api.model('new_document', {
        'document_hash': fields.String(
            required=True, description='hash of the document to be notarised'),
        'current_seal': fields.String(
            required=True, description='id of the txo which is current seal '),
        'next_seal': fields.String(
            required=False, description='id of the txo which is next seal ')
    })
    document_to_update = api.model('document_to_update', {
        'public_id': fields.String(description='document Identifier'),
        'document_hash': fields.String(
            required=True, description='hash of the document to be notarised'),
        'current_seal': fields.String(
            required=True, description='id of the txo which is current seal'),
        'next_seal': fields.String(
            required=False, description='id of the txo which is next seal'),
        'sender_signed_hash': fields.String(
            required=False, description='document hash signed by the sender '
                                        'with the respective public key'),
        'sender_public_key': fields.String(
            required=False, description='public key used to check if the '
                                        'sender actually signed the message'),
        'sender_and_receiver_signed_hash': fields.String(
            required=False, description='document hash signed by the sender '
                                        'and subsequently by the receiver '
                                        'with the respective public key'),
        'receiver_public_key': fields.String(
            required=False, description='public key used to check if the '
                                        'receiver actually signed the message')
    })
    paged_document_list = api.model('paged_document_list', {
        'page': fields.Integer(description='Page number in the paged list'),
        'page_size': fields.Integer(description='Records per page'),
        'total_record_count': fields.Integer(description='Total records in the list'),
        'records':fields.List(fields.Nested(document)),
    })

class NotarizationItemDto:
    api = Namespace('notarizationitem', description='Notarization Item related operations')
    notarizationitem = api.model('notarizationitem', {
        'public_id': fields.String(required=True, description='document Identifier'),
        'notarization_session_id': fields.String(required=True,
                              description='Id of the session of the notarization'),
        'message_hash': fields.String(required=True, description='hex representation of the hash of the message to be notarised with others in the session, a lead of the merkle tree'),
        'notes': fields.String(required=False, description='notes about the source of the message'),
        'ebsi_notes': fields.String(required=False, description='jsonstring of metadata to notarize on EBSI, shipment_id and authorised_dids'),
    })
    new_notarizationitem = api.model('new_notarizationitem', {
        'notarization_session_id': fields.String(required=True,
                              description='Id of the previous session of the notarization cycle'),
        'message_hash': fields.String(required=True, description='hex representation of the hash of the message to be notarised with others in the session, a lead of the merkle tree'),
        'notes': fields.String(required=False, description='notes about the source of the message, e.g. external ids, will not enter notarization, it is only for ease of use'),
        'ebsi_notes': fields.String(required=False, description='jsonstring of metadata to notarize on EBSI, shipment_id and authorised_dids')
    })

class NotarizationSessionDto:
    api = Namespace('notarizationsession', description='Notarization Session related operations')
    notarizationsession = api.model('notarizationsession', {
        'public_id': fields.String(required=True, description='document Identifier'),
        'previous_session_id': fields.String(required=False,
                              description='Id of the previous session of the notarization cycle, if empty, this is the first session of the chain'),
        'notarization_txid': fields.String(required=False,readonly=True, description='txid with the OP_RETURN of the notarised document'),
        'merkle_root': fields.String(required=False, readonly=True, description='hash of the merkle root built from the items'),
        'status': fields.String(required=False, readonly=True, description='Status of the session: new (can add items to it), locked (document ready to be notarised), closed (related document notarized, meaning seal is spent)'),
        'stamper_type': fields.String(required=False, description='Type of notarization, values: ebsi, default'),
        'items': fields.List(fields.Nested(NotarizationItemDto.notarizationitem)),
    })
    new_notarizationsession = api.model('new_notarizationsession', {
        'previous_session_id': fields.String(required=False,
                              description='Id of the previous session of the notarization cycle'),
        'stamper_type': fields.String(required=False, description='Type of notarization, values: ebsi, std')
    })
    paged_notarizationsession_list = api.model('paged_notarizationsession_list', {
        'page': fields.Integer(description='Page number in the paged list'),
        'page_size': fields.Integer(description='Records per page'),
        'total_record_count': fields.Integer(description='Total records in the list'),
        'records':fields.List(fields.Nested(notarizationsession)),
    })

class DlcSessionDto:
    api = Namespace('dlcsession', description='DLC Session related operations')
    dlcsession = api.model('dlcsession', {
        'public_id': fields.String(description='document Identifier'),
        'k': fields.String(required=True,
                              description='hex string of the 32 bytes of random number used as fixed point for the signatures'),
        'oracle_pubkey': fields.String(required=True, description='public key (BIP39 compressed pubkey, 66 hex digits) used by the Oracle'),
        'value': fields.Fixed(decimals=8, require=False,
                              description='Value of the specified output - in BTC, will be converted to Sats'),
        'status': fields.String(required=False, readonly=True, description='Status of the session: pending (new), locked (money sent to the escrow), outcome confirmed (oracle singed an outcome), paid (tx sent to winning address) - todo: expired'),
        'escrow_txid': fields.String(required=False, description='txid of the escrow payment'),
        'escrow_voutn': fields.String(required=False, description='voutn of the escrow payment'),

    })
    new_dlcsession = api.model('new_dlcsession', {
        'k': fields.String(required=False,
                              description='hex string of the 32 bytes of random number used as fixed point for the signatures (created on the serve if left empty)'),
        'oracle_pubkey': fields.String(required=True, description='public key (BIP39 compressed pubkey, 66 hex digits) used by the Oracle'),
        'value': fields.Fixed(decimals=8, require=False,
                              description='Value of the specified output'),
    })
    paged_dlcsession_list = api.model('paged_dlcsession_list', {
        'page': fields.Integer(description='Page number in the paged list'),
        'page_size': fields.Integer(description='Records per page'),
        'total_record_count': fields.Integer(description='Total records in the list'),
        'records':fields.List(fields.Nested(dlcsession)),
    })

class DlcOutcomeDto:
    api = Namespace('dlcoutcome', description='DLC Outcomes related operations')
    dlcoutcome = api.model('dlcoutcome', {
        'public_id': fields.String(description='Outcome Identifier'),
        'dlc_session_id': fields.String(required=True, description='public id of the dlc session related to this outcome'),
        'locktime': fields.Integer(required=True, description='Number of blocks for the locktime'),
        'message': fields.String(required=True, description='message that corresponds to this outcome and that will be eventually signed by the oracle'),
        'winner_pubkey': fields.String(required=True, description='public key (BIP39 compressed pubkey, 66 hex digits) for which the winning user knows the private key and will use to spend'),
        'winner_address': fields.String(required=True, description='BTC or Liquid address where the amount present in the escrow will be sent'),
        'combined_pubkey': fields.String(required=True, description='public key (PEM encoded or BIP encoded) which is the combination of the message public key and the winner public key'),
        'default_pubkey': fields.String(required=True, description='public key (BIP encoded) which is the default fallback in case the oracle will not sign the message (this outcome is not winning)'),
        'escrow_address': fields.String(required=True, description='Escrow address where the Bitcoin will be put meanwhile the Oracle decides which is the outcome'),
        'sent_with_tx': fields.String(required=False, description='Txid of the transaction used to pay the winner of this outcome (or the default address if after the timelock)'),
        'status': fields.String(required=False, readonly=True, description='Status of the outcome: pending (new), confirmed (message signed by oracle), paid (tx sent to winning address), default (paid to default after timelock expired)')
    })
    update_dlcoutcome = api.model('upd_dlcoutcome', {
        'public_id': fields.String(description='Outcome Identifier'),
        'winner_address': fields.String(required=True, description='BTC or Liquid address where the amount present in the escrow will be sent'),
    })
    new_dlcoutcome = api.model('new_dlcoutcome', {
        'dlc_session_id': fields.String(required=True, description='public id of the dlc session related to this outcome'),
        'message': fields.String(required=True, description='message that corresponds to this outcome and that will be eventually signed by the oracle'),
        'locktime': fields.Integer(required=True, description='Number of blocks for the locktime'),
        'winner_pubkey': fields.String(required=True, description='public key (BIP39 compressed pubkey, 66 hex digits) for which the winning user knows the private key and will use to spend'),
        'winner_address': fields.String(required=True, description='BTC or Liquid address where the amount present in the escrow will be sent'),
        'default_pubkey': fields.String(required=True, description='public key (BIP encoded) which is the default fallback in case the oracle will not sign the message (this outcome is not winning)'),

    })
    paged_dlcoutcome_list = api.model('paged_dlcoutcome_list', {
        'page': fields.Integer(description='Page number in the paged list'),
        'page_size': fields.Integer(description='Records per page'),
        'total_record_count': fields.Integer(description='Total records in the list'),
        'records':fields.List(fields.Nested(dlcoutcome)),
    })
    confirm_dlcoutcome = api.model('confirm_dlcoutcome', {
        'public_id': fields.String(description='Outcome Identifier', required=True),
        'signed_message': fields.String(description='Schnorr signed message', required=True)
    })
    pay_dlcoutcome = api.model('pay_dlcoutcome', {
        'public_id': fields.String(description='Outcome Identifier', required=True),
        'wif': fields.String(description='WIF fromatted private key of the outcome winner, this should be used only for DEMO purposes (when empty, the key handled by the node is used)', required=False)
    })

class AssetDto: 
    api = Namespace('asset', description='asset related operations')
    asset = api.model('asset', {
        'public_id': fields.String(description='document Identifier'),
        'description': fields.String(required=False,
                              description='Description saved inside the asset'),
        'external_url': fields.String(required=True,
                                description='External url of the asset'),
        'image': fields.String(decimals=8, require=False,
                              description='Image of the asset'),
        'name': fields.String(required=True,
                                             description='Name of the asset'),
        'attributes': fields.String(required=False,
                                            description='Attributes of the asset'),
        'blockchain': fields.String(required=False,
                                            description='Blockchain of the asset'),
        'last_checked_time': fields.DateTime(dt_format=u'iso8601',
                                             required=False,
                                             description='Last time this asset '
                                                         'was checked on the '
                                                         'blockchain'),
        'last_checked_status': fields.String(required=False,
                                             description='Most recent status last time the asset was checked'),
        'last_checked_blockhash': fields.String(required=False,
                                             description='Most recent blockchash last time the asset was checked'),
    })
    new_asset = api.model('new_asset', {
        'description': fields.String(required=False,description='The description of the asset'),
        'external_url': fields.String(required=True,description='The external url of the asset'),
        'image': fields.String(required=False,description='The image of the asset'),
        'name': fields.String(required=True,description='The name of the asset'),
        'attributes': fields.String(required=False,description='The attributes of the asset')
    })

class MethodResultDto:
    api = Namespace('rpc_services', description='special rpc-like methods')
    method_result = api.model('method_result', {
        'method': fields.String(required=True, description='The called method'),
        'result': fields.String(required=True, description='The result'),
    })
    schnorr_sign_body = api.model("schnorr_sign_body", {
        'message': fields.String(requires=True, description='utf-8 text message to be signed'),
        'k': fields.String(required=True,
                              description='hex string of the 32 bytes of random number used as fixed point for the signatures'),
        'priv': fields.String(required=True,
                            description='hex string of the 32 bytes of private number used for the signature. NEVER TO BE USED OUTSIDE OF DEMO PURPOSES.')

    })
    schnorr_verify_body = api.model("schnorr_verify_body", {
        'message': fields.String(requires=True, description='utf-8 text message to be signed'),
        'k': fields.String(required=True,
                              description='hex string of the 32 bytes of random number used as fixed point for the signatures'),
        'pub': fields.String(required=True, description='public key (BIP39 compressed pubkey, 66 hex digits) which signed the message'),
        'signed_message': fields.String(required=True,
                              description='hex string of the 32 bytes of the number output of the signature process'),

    })
    new_key = api.model('new_key', {
        'kty': fields.String(required=True,
                              description='Type of cryptography JWK parameter, must be "EC"'),
        'crv': fields.String(required=True,
                                description='Selected curve, must be "secp256k1"'),
        'x': fields.String(required=True,
                                description='Public key X coordinate - base64 url encoded as per JWK standard'),
        'y': fields.String(required=True,
                                description='Public key Y coordinate - base64 url encoded as per JWK standard'),
        'd': fields.String(required=True,
                                description='Secret number - based64 url encoded as per JWK standard')
    })