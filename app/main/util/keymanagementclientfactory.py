import os.path
import jwt
import base64
import base58
from eth_account import Account
import json
#from eth_account.transactions import LegacyTransaction
#from eth_keys import keys

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.asymmetric import ec

"""
TODO: This class will handle the connection to KMIP-compliant key management server

at the moment the chosen reference is: https://pykmip.readthedocs.io/en/latest/index.html

TODO: once the functions from pykmip are clear, define the abastract methods
"""
#from kmip.pie import objects
#from kmip.pie import client
#from kmip import enums

class KeyManagementClientFactory:
    def __init__(self):
        self._builders = {}

    def register_builder(self, key, builder):
        self._builders[key] = builder

    def create(self, key, **kwargs):
        builder = self._builders.get(key)
        if not builder:
            # return default
            builder = DevKeyManagementClientBuilder(**kwargs)
        return builder(**kwargs)

class DevKeyManagementClientBuilder:
    def __init__(self, **_ignored):       
        self._instance = None

    def __call__(self, **ignored):
        #TODO: **ignored could be the path where to look for and create the key file
        if not self._instance:
            self._instance = DevKeyManagementClient()
        return self._instance

class DevKeyManagementClient:
    """ basic KM tool with a local private key saved in the __file__ folder
    for developing, testing, sandbox usage only """
    _type = 'DEV'
    _priv_key = None
    def __init__(self):
        """ get the priv key from the .pem file, if not there, create it """
        try:
            self._priv_key = self._get_key();
        except OSError as e:
            self._priv_key = self._generate_key()
        except Exception as e:
            print(e)
            raise Exception('Something wrong with Key Management Init')

    def _get_key(self, *args, **kwargs):
        """
        Get the key from the default path 

        the default path for now is the same folder as the __file__,
        if the .pem is not there, OSError is raised so that
        the caller can act accordingly. Notice that DEVClient uses EC.

        Params:
        -------

        Returns:
        --------
        EllipticCurvePrivateKey
            as for the cryptography library

        Raises:
        -------
        OSError
            When the file is not there
        ValueError, TypeError, UnsupportedAlgorithm
            When there is something wrong in the file

        """
        filename = os.path.join(os.path.dirname(__file__), 'privkey.pem')
        with open(filename, 'rb') as pem_in:
            pemlines = pem_in.read()
        priv_key = load_pem_private_key(pemlines, None, default_backend())
        pub_key = priv_key.public_key()
        serialized_pub_key = pub_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        return priv_key

    def _generate_key(self, *args, **kwargs):
        """
        Generate a new key 

        the default path for now is the same folder as the __file__,
        a new file will be saved there and it will be a PEM
        representation of an EC private key

        Params:
        -------

        Returns:
        --------
        EllipticCurvePrivateKey
            as for the cryptography library

        Raises:
        -------
        OSError
            When there are problems in writing the file

        """
        priv_key = ec.generate_private_key(ec.SECP256K1(), default_backend())
        filename = os.path.join(os.path.dirname(__file__), 'privkey.pem')
        pem = priv_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption()
        )
        with open(filename, 'wb') as pem_out:
            pem_out.write(pem)
        return priv_key

    def get_serialized_pub_key(self):
        """
        Get the serialised public key with PEM encoding of this eonpass node

        Params
        ------

        Returns
        -------
        bytes
            the serialised public key with the PEM encoding

        Raises
        ------

        """
        priv_key = self._priv_key
        pub_key = priv_key.public_key()
        serialized_key = pub_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        return serialized_key

    def get_jwk_pubkey(self):
        """
        Get the local key but in JWK format

        Params
        ------

        Returns
        -------
        dict, json object with the pub key in jwk format compatible with EBSI

        """
        priv_key = self._priv_key
        pub_key = priv_key.public_key()
        public_numbers = pub_key.public_numbers()
        # Generate thumbprint for the key as keyId
        x_bytes = int.to_bytes(public_numbers.x, length=(public_numbers.x.bit_length() + 7) // 8, byteorder='big')
        y_bytes = int.to_bytes(public_numbers.y, length=(public_numbers.y.bit_length() + 7) // 8, byteorder='big')
        x_base64url = base64.urlsafe_b64encode(x_bytes).decode('utf-8').rstrip('=')
        y_base64url = base64.urlsafe_b64encode(y_bytes).decode('utf-8').rstrip('=')
        jwk_pre_thmb = {
            "crv": "secp256k1",
            "kty": "EC",
            "x": x_base64url,
            "y": y_base64url
        }
        thumb = self._calculate_jwk_thumbprint(jwk_pre_thmb)   
        jwk_dict = {
            "kty": "EC",
            "crv": "secp256k1",
            "use": "sig",
            "x": x_base64url,
            "y": y_base64url,
            "kid": thumb
        }
        return jwk_dict

    def sign_bytes_message(self, message):
        """
        sign a message with the local key

        use the priv key generate during init, for testing purposes
        there is only one key, real KMIC may have more
        
        Parameters
        ----------
        message: bytes
            the message to be signed by the node

        Returns
        --------
        dict
            {signed, serialized_public}, a JSON containind the
            signed messaged and its respective serialized public key
            in bytes


        Raises
        ------
        Exception
            when message is not in bytes

        """

        priv_key = self._priv_key
        pub_key = priv_key.public_key()
        serialized_key = pub_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        signed = priv_key.sign(message, ec.ECDSA(hashes.SHA256()))
        return {'signed': signed, 'serialized_public': serialized_key}

    def sign_jwt(self, payload, headers):
        """
        Use the local priv key to sign a jwt

        Parameters
        ----------
        payload: dict, body of the jwt
        headers: dict, headers of the jwt
        alg: type of EC sig to use, can only be ES256 or ES256K (EBSI compatible)

        Returns
        -------
        str
            the decoded token
        """
        if headers["alg"] not in ["ES256", "ES256K"]:
            raise ValueError("Invalid algorithm. Supported algorithms are 'ES256' and 'ES256K'")

        priv_key = self._priv_key
        return jwt.encode(payload, priv_key, algorithm=headers["alg"], headers=headers)

    def get_eth_account(self):
        """
        EBSI needs to identify keys with their respective eth addresses

        Returns
        -------
        str
            the address
        """
        priv_key_bytes = self._priv_key.private_numbers().private_value.to_bytes(32, byteorder='big')
        client_address = Account.from_key(priv_key_bytes).address
        return client_address

    def sign_ebsi_tx(self, unsigned_tx):
        """
        Use the local key to sign the tx

        Returns
        -------
        a SignedTransaciton object
            with all the necessary fields: rawTransaction, hash, r, s, v
        """
        raw_tx = {
            'nonce': int(unsigned_tx.get("nonce"), 16),
            'gasPrice': int(unsigned_tx.get("gasPrice"),16),
            'gas': int(unsigned_tx.get("gasLimit"),16),
            'to': unsigned_tx.get("to"),
            'value': int(unsigned_tx.get("value"),16),
            'data': unsigned_tx.get("data"),
            'chainId': int(unsigned_tx.get("chainId"),16)
        }
        priv_key_bytes = self._priv_key.private_numbers().private_value.to_bytes(32, byteorder='big')
        signed_tx = Account.sign_transaction(raw_tx, priv_key_bytes)
        """
        legacy_tx = LegacyTransaction(nonce=raw_tx["nonce"], gas_price=raw_tx["gasPrice"], gas=raw_tx["gasLimit"],
                               to=raw_tx["to"], value=raw_tx["value"], data=raw_tx["data"], chain_id=raw_tx["chainId"])
        signed_tx = legacy_tx.sign(keys.PrivateKey(priv_key_bytes))
        raw_signed_tx = signed_tx.rawTransaction
        """
        return signed_tx


    def verify_signed_message(self, signed, message, serialized_public):
        """
        check if a message and a signature are matching given the pub key 

        note that the signature may be sent over by another node, 
        therefore we also need to know the public_key

        Parameters
        ----------
        signed: bytes
            the signed version

        message: bytes
            the original message that was signed

        serialized_public: bytes
            the public key in PEM encoding

        Returns
        --------
        bool
            True if the signed message check out, False otherwise.
            Note that anything going wrong is catched and 
            the method just returns False
        """
        try:
            loaded_public_key = serialization.load_pem_public_key(
                serialized_public,
                backend=default_backend()
            )
            # this raises exception if the signature is not valid
            loaded_public_key.verify(
                signed, message, ec.ECDSA(hashes.SHA256()))
            return True
        except Exception:
            return False

    def convert_jwk_to_pem(self, key):
        """
        Transform a JWK into a classic PEM representation

        Params
        ------
        key: dict
            JSON of the JWK, it should have EC secpt256k1 and d. Double checks that x and y returns the same point

        Returns
        -------
        key: serialized priv key in pem format
        """
        curve = ec.SECP256K1()
        signature_algorithm = ec.ECDSA(hashes.SHA256())
        #convert base64 to hex
        padding_factor = (4 - len(key['d']) % 4) % 4
        padded_secret = key['d']+ '='*padding_factor
        secret_bytes = base64.urlsafe_b64decode(padded_secret)
        secret_int =  int.from_bytes(secret_bytes, 'big')
        priv_key = ec.derive_private_key(secret_int, curve, default_backend())
        pem = priv_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption()
        )
        return pem


    def overwrite_local_key(self, pemkey):
        """
        Overwrite the local pem file

        Params
        ------
        key: PEM encoded key
            the new key you want to use on this node

        Returns
        -------
        pubkey: PEM serialized key
            the enw public key of the node
        
        Raises
        ------
        EonError
            Somethign went wrong

        """
        filename = os.path.join(os.path.dirname(__file__), 'privkey.pem')
        with open(filename, 'wb') as pem_out:
            pem_out.write(pemkey)

        self._priv_key = self._get_key();

        return self.get_serialized_pub_key()

    def _calculate_jwk_thumbprint(self, jwk):
        """
        For EBSI compatibility KeyIds are always set to the thmbprint of the key

        Returns
        -------
        thumbprint: str
            the thumbprint of this jwk payload
        """
        jwk_json = json.dumps(jwk, sort_keys=True, separators=(',', ':')).encode('utf-8')
        digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        digest.update(jwk_json)
        thumbprint = base64.urlsafe_b64encode(digest.finalize()).decode('utf-8').rstrip('=')
        return thumbprint

    def get_didkey_from_local_key(self):
        """
        Build the did:key for the local key
        """
        public_key = self._priv_key.public_key()
        """
        compressed_public_key_bytes = public_key.public_bytes(
            encoding=serialization.Encoding.X962,
            format=serialization.PublicFormat.CompressedPoint
        )
        encoded_public_key = base58.b58encode(compressed_public_key_bytes)
        """
        private_key = self._priv_key
        x_coordinate = private_key.public_key().public_numbers().x
        y_coordinate = private_key.public_key().public_numbers().y
        x_bytes = int.to_bytes(x_coordinate, length=(x_coordinate.bit_length() + 7) // 8, byteorder='big')
        y_bytes = int.to_bytes(y_coordinate, length=(y_coordinate.bit_length() + 7) // 8, byteorder='big')
        x_base64url = base64.urlsafe_b64encode(x_bytes).decode('utf-8').rstrip('=')
        y_base64url = base64.urlsafe_b64encode(y_bytes).decode('utf-8').rstrip('=')
        # Prefix for the DID
        prefix = bytes.fromhex("d1d603")
        jwk = {
            "crv": "secp256k1",
            "kty": "EC",
            "x": x_base64url,
            "y": y_base64url
        }
        jwk_json = json.dumps(jwk, sort_keys=True, separators=(',', ':')).encode('utf-8')
        # Encode with base58 zQ3s
        did2 = "did:key:z" + base58.b58encode(prefix+jwk_json).decode()
        return did2


