from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import ec
import json

from app.main.util.tasks import make_rpc_call
from app.main.util.keymanagementutils import KeyManagementClient
import app.main.util.pypy_sha256 as pypy_sha256

#PYPY_SHA256 parameters:
BLOCK_LEN = 512
BLOCK_BYTES = BLOCK_LEN // 8
HASH_LEN = 256
HASH_BYTES = HASH_LEN // 8


class Singleton(object):
    _instance = None  # Keep instance reference
    _kmc = KeyManagementClient()

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance


class CryptoUtils(Singleton):
    def get_best_blockhash(self, *args, **kwargs):
        """
        query the blockchain node to get the latest block

        it uses the getblòockchaininfo rpc method and when it
        doesn't fail, the key bestblockchash is returned

        Parameters
        ----------

        Returns
        -------
        str
            the blockchash of the latest block

        Raises
        ------
            Exception
                every time a response from celery if checked (_check_rpc_error)
                if an error was reported by the node, an Exception is raised
                with the error message as content

        """
        t = make_rpc_call.delay('getblockchaininfo')
        res = self._check_rpc_error(t.wait())
        return res['bestblockhash']


    def check_txout(self, *args, **kwargs):
        """
        returns details about an unspent transaction output,

        it uses gettxout which responds correctly only if the node
        recognizes the txid and voutn,
        to do so the node needs to have in its config:
        txindex=1
        If you don't have or can't have
        the node configured like that, you need to parse the blockchain
        which will be done in another method.
        The order of inputs must be: (txid, n), we leave it open
        to add another parameter in the future to decide whether to
        query the node or parse the blockchain.

        Parameters
        ----------
        args[0]: str
            txid
        args[1]: int
            voutn
        args[2]: int
            txindex value of the node, 0 or 1, not suported yet

        Returns
        -------
        str
            'spent' or 'unspent', the status of the txout

        Raises
        ------
            Exception
                every time a response from celery if checked (_check_rpc_error)
                if an error was reported by the node, an Exception is raised
                with the error message as content
        """
        txid = ''
        n = 0
        if args and isinstance(args[0], str):
            txid = args[0]
        else:
            raise Exception('malformed txid arg')
        if args and isinstance(args[1], int):
            n = args[1]
        else:
            raise Exception('malformed n arg')
        # gettxout returns something  if the output is "unspent"
        # and the node knows about it (i.e. txindex=1)
        t = make_rpc_call.delay('gettxout', txid, n)
        res = t.wait()
        raw_out = self._check_rpc_error(res)
        if raw_out:
            return 'unspent'
        else:
            return 'spent'

    def get_utxos(self, *args, **kwards):
        """
        uses listunspent to get the unspent outputs on the current node

        by default it checks for at least 1 confirmation,
        the minconf is an optional parameter passed
        to the method. It returns all unspent txos above
        a value treshold.
        The value treshold should be used to make sure you can
        easily spent the tx, for now it's hardcoded, TBD

        Parameters
        ----------
        args[0]: int
            minconf

        Returns
        -------
        [dict]
            an array of JSON objects returned by the node
            with the vout parameter duplicated also on the 'n' key
            for retrocompatibility. Should be 'voutn' anyways.

        Raises
        ------
            Exception
                every time a response from celery if checked (_check_rpc_error)
                if an error was reported by the node, an Exception is raised
                with the error message as content
        """
        minconf = 1
        if args and isinstance(args[0], int):
            minconf = args[0]

        t = make_rpc_call.delay('listunspent', minconf)
        utxos = self._check_rpc_error(t.wait())

        available_utxos = []
        # filter by value and add the old property n
        for utxo in utxos:
            if float(utxo['amount']) >= 0.0001:
                utxo['n'] = utxo['vout']
                available_utxos.append(utxo)
        return available_utxos

    def get_locked_utxos(self, *args, **kwards):
        """
        uses listlockunspent to get the unspend outputs locked on the current node

        locked utxos are an in-memory thing, so nodes will reset them
        when going down.

        Parameters
        ----------

        Returns
        -------
        [dict]
            an array of JSON objects returned by the node
            they only have txid and voutn as properties

        Raises
        ------
            Exception
                every time a response from celery if checked (_check_rpc_error)
                if an error was reported by the node, an Exception is raised
                with the error message as content
        """
        t = make_rpc_call.delay('listlockunspent')
        utxos = self._check_rpc_error(t.wait())
        return utxos

    def spend_txo(self, txid, voutn, msg_hex):
        """
        spend (at least) the given input to create an OP_RETURN output

        the input is the one declared in a document, the message is
        the concatenation of the message and the seals signed by both parties.
        Before spending we have to fundrawtransaction, this may add other
        inputs to the transaction. We should check that such inputs are not
        seals for other documents, if so, remove them from the document.
        The msg_hex will be truncated to maximum length admissable

        Parameters
        ----------
        txid: str
            the txid of the input
        voutn: int
            the voutn of the input
        msg_hex:
            the .hex() representation of bytes

        Returns
        --------
        dict
            the JSON description of the submitted tx to the mempool

        Raises
        ------
        Exception
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content. E.g. msg_hex too long.
        """
        ins = [{'txid': txid, 'vout': voutn}]
        outs = [{'data': msg_hex}]

        t = make_rpc_call.delay('createrawtransaction', ins, outs)
        raw_tx = self._check_rpc_error(t.wait())

        t = make_rpc_call.delay('fundrawtransaction', raw_tx) #fundrawtransaction può prendere in inpuit gli output che vuoi
        funded_tx = self._check_rpc_error(t.wait())  # fix fees
        # here I should check that eventual other inputs added by the
        # fundrawtx are not utxos I am using on other documents
        t = make_rpc_call.delay('blindrawtransaction', funded_tx['hex'])
        blinded_tx = self._check_rpc_error(t.wait())
        t = make_rpc_call.delay('signrawtransactionwithwallet', blinded_tx)
        signed_tx = self._check_rpc_error(t.wait())
        t = make_rpc_call.delay('sendrawtransaction', signed_tx['hex'])
        sent_tx = self._check_rpc_error(t.wait())
        return (sent_tx)

    def create_simple_tx_no_spend(self, txid, voutn, msg_hex, target_address, value):
        """
        create a simple raw tx to compare with the ones form RUST
        """
        ins = [{'txid': txid, 'vout': voutn}]
        outs = [{'data': msg_hex },{target_address:value}]

        t = make_rpc_call.delay('createrawtransaction', ins, outs)
        raw_tx = self._check_rpc_error(t.wait())
        t = make_rpc_call.delay('decoderawtransaction', raw_tx)
        decoded_raw_tx = self._check_rpc_error(t.wait())
        print("DECODED RAW")
        print(decoded_raw_tx)

        t = make_rpc_call.delay('fundrawtransaction', raw_tx) #fundrawtransaction può prendere in inpuit gli output che vuoi
        funded_tx = self._check_rpc_error(t.wait())  # fix fees
        t = make_rpc_call.delay('decoderawtransaction', funded_tx["hex"])
        decoded_tx = self._check_rpc_error(t.wait())
        print("FUNDED TX")
        print(funded_tx)

        # here I should check that eventual other inputs added by the
        # fundrawtx are not utxos I am using on other documents
        t = make_rpc_call.delay('blindrawtransaction', funded_tx['hex'])
        blinded_tx = self._check_rpc_error(t.wait())
        t = make_rpc_call.delay('decoderawtransaction', blinded_tx)
        decoded_tx = self._check_rpc_error(t.wait())
        print("BLINDED TX")
        print(decoded_tx)


        t = make_rpc_call.delay('signrawtransactionwithwallet', blinded_tx)
        signed_tx = self._check_rpc_error(t.wait())
        print("SIGNED TX")
        print(blinded_tx)

        return (signed_tx)

    def decode_script(self, script_hex):
        """
        decode a script
        """
        t = make_rpc_call.delay('decodescript', script_hex) #fundrawtransaction può prendere in inpuit gli output che vuoi
        decoded_script = self._check_rpc_error(t.wait())  # fix fees
        return decoded_script

    def decode_raw_transaction(self, hexraw):
        """
        decode a raw transaction
        """
        t = make_rpc_call.delay('decoderawtransaction', hexraw) #fundrawtransaction può prendere in inpuit gli output che vuoi
        decoded_tx = self._check_rpc_error(t.wait())  # fix fees
        return decoded_tx

    def spend_signed_hex_tx(self, hextx):
        """
        Get a hex of a tx from the RUST library and try to spend it
        """
        t = make_rpc_call.delay('sendrawtransaction', hextx)
        sent_tx = self._check_rpc_error(t.wait())
        return (sent_tx)

    def create_simple_transaction(self, txid, voutn, msg_hex, target_address, value):
        ins = [{'txid': txid, 'vout': voutn}]
        outs = [{'data': msg_hex },{target_address:value}]

        t = make_rpc_call.delay('createrawtransaction', ins, outs)
        raw_tx = self._check_rpc_error(t.wait())
        t = make_rpc_call.delay('decoderawtransaction', raw_tx)
        decoded_raw_tx = self._check_rpc_error(t.wait())

        t = make_rpc_call.delay('fundrawtransaction', raw_tx) #fundrawtransaction può prendere in unpuit gli output che vuoi
        funded_tx = self._check_rpc_error(t.wait())  # fix fees

        t = make_rpc_call.delay('blindrawtransaction', funded_tx['hex'])
        blinded_tx = self._check_rpc_error(t.wait())

        t = make_rpc_call.delay('signrawtransactionwithwallet', blinded_tx)
        signed_tx = self._check_rpc_error(t.wait())

        signed_tx = self._check_rpc_error(t.wait())
        t = make_rpc_call.delay('sendrawtransaction', signed_tx['hex'])
        sent_tx = self._check_rpc_error(t.wait())
        return (sent_tx)

    def create_simple_transaction_sign_with_wif(self, txid, voutn, msg_hex, target_address, value, wif):
        ins = [{'txid': txid, 'vout': voutn}]
        outs = [{'data': msg_hex },{target_address:value}]

        t = make_rpc_call.delay('createrawtransaction', ins, outs)
        raw_tx = self._check_rpc_error(t.wait())
        t = make_rpc_call.delay('decoderawtransaction', raw_tx)
        decoded_raw_tx = self._check_rpc_error(t.wait())

        t = make_rpc_call.delay('fundrawtransaction', raw_tx) #fundrawtransaction può prendere in unpuit gli output che vuoi
        funded_tx = self._check_rpc_error(t.wait())  # fix fees

        t = make_rpc_call.delay('blindrawtransaction', funded_tx['hex'])
        blinded_tx = self._check_rpc_error(t.wait())

        t = make_rpc_call.delay('signrawtransactionwithkey', blinded_tx, [wif])
        signed_tx = self._check_rpc_error(t.wait())

        t = make_rpc_call.delay('decoderawtransaction', signed_tx['hex'])
        decoded_raw_tx2 = self._check_rpc_error(t.wait())

        t = make_rpc_call.delay('sendrawtransaction', signed_tx['hex'])
        sent_tx = self._check_rpc_error(t.wait())
        return (sent_tx)

    def rescan_blockchain(self):
        t = make_rpc_call.delay('rescanblockchain')
        rescan_result = self._check_rpc_error(t.wait())
        return rescan_result

    def derive_address(self, descriptor_with_checksum):
        t = make_rpc_call.delay('deriveaddresses', descriptor_with_checksum)
        addr = self._check_rpc_error(t.wait())
        return addr

    def get_descriptor_info(self, descriptor):
        t = make_rpc_call.delay('getdescriptorinfo', descriptor)
        descinfo = self._check_rpc_error(t.wait())
        return descinfo

    def import_priv_key(self, wif_key, label, rescan=False):
        t = make_rpc_call.delay('importprivkey', wif_key, label, rescan) #otherwise default rescan = true.. warning!!
        wallet = self._check_rpc_error(t.wait())
        return wallet

    def import_pub_key(self, pubkey, label, rescan=False):
        t = make_rpc_call.delay('importpubkey', pubkey, label, rescan) #otherwise default rescan = true.. warning!!
        wallet = self._check_rpc_error(t.wait())
        return wallet

    def create_wallet(self, label, blank=True):
        t = make_rpc_call.delay('createwallet', label, False, blank) #default rescan = true.. warning!!
        wallet = self._check_rpc_error(t.wait())
        return wallet

    def unload_wallet(self, wallet_label):
        t = make_rpc_call.delay('unloadwallet', wallet_label) #default rescan = true.. warning!!
        wallet = self._check_rpc_error(t.wait())
        return wallet


    def get_address_info(self, addr):
        t = make_rpc_call.delay('getaddressinfo', addr) #default rescan = true.. warning!!
        wallet = self._check_rpc_error(t.wait())
        return wallet
        

    def get_raw_tx(self, txid):
        """
        get the raw tx from the node

        raw tx is blinded by default but returns many useful info
        like the number of confirmations, blockhash, etc..

        Parameters
        ----------
        txid: str
            the txid of the input

        Returns
        --------
        dict
            the JSON description of the tx as returned by the node

        Raises
        ------
        Exception
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content
        """
        t = make_rpc_call.delay('getrawtransaction', txid, True)
        return self._check_rpc_error(t.wait())

    def get_transaction(self, txid):
        """
        get transaction details for transactions which ahve been submitted by this wallet, this has "confirmations" for instance
        """
        t = make_rpc_call.delay('gettransaction', txid, True)
        return self._check_rpc_error(t.wait())

    def was_seal_spent_in_tx(self, seal_txid, seal_voutn, txid):
        """
        Check if in the given txid there was indeed an input described by the seal

        This is useful when importing documents to check their history is correct.
        The transaction may or may not be addressed to us, therefore we may or may
        not be able to de-blind it, nonetheless we have the vins

        Parameters
        ----------
        seal_txid: str
            txid of the seal

        seal_voutn: int
            voutn of the seal

        txid: str
            txid to inspect

        Returns
        -------
        dict
            'result':bool
                True/False, was the seal spent in the tx?
            'confirmations':int
                if true, how many blocks ago

        Raises
        ------
        Exception
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content
        """
        t = make_rpc_call.delay('getrawtransaction', txid, True)
        raw_tx = self._check_rpc_error(t.wait())
        vins = raw_tx["vin"]
        for vin in vins:
            if(vin['txid']==seal_txid and vin['vout']==seal_voutn):
                return {"result":True,"confirmations":raw_tx['confirmations']}
        return {"result":False,"confirmations":0}


    def lock_utxo(self, txid, voutn):
        """
        try to lock the utxo

        a locked utxo cannot be spent by the automatic input aggregation.
        notice that the paremeter is "unlock", so to lock the node
        must receive lockunspent false. (who even did that..)

        Parameters
        ----------
        txid: str
            the txid of the input
        voutn: int
            the index of the output in the txid

        Returns
        --------
        bool
            true for opierations successfull, false for failuyres

        Raises
        ------
        Exception
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content
        """
        payload = [{"txid":txid, "vout": voutn}]
        t = make_rpc_call.delay('lockunspent', False, payload)
        return self._check_rpc_error(t.wait())

    def unlock_utxo(self, txid, voutn):
        """
        try to unlock the utxo

        a locked utxo cannot be spent by the automatic input aggregation.
        notice that the paremeter is "unlock", so to lock the node
        must receive lockunspent false. (who even did that..)

        Parameters
        ----------
        txid: str
            the txid of the input
        voutn: int
            the index of the output in the txid

        Returns
        --------
        bool
            true for opierations successfull, false for failuyres

        Raises
        ------
        Exception
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content
        """
        payload = [{"txid":txid, "vout": voutn}]
        t = make_rpc_call.delay('lockunspent', True, payload)
        return self._check_rpc_error(t.wait())

    def estimatesmartfee(self, target):
        """
        query the node and get an estimate fee BTC/kB

        Parameters
        ----------
        target: int
            the number of blocks after which the tx should be confirmed

        Returns
        -------
        dict
            {feerate, errors, blocks}
            feerate: fee estimate BTC/kB
            errors: array of errors
            blocks: blocknumber where estimate was found

        """
        t = make_rpc_call.delay('estimatesmartfee', target)
        return self._check_rpc_error(t.wait())

    def issue_asset(self, assetamount, reissuancetokens, blind):
        """
        -issue asset

        Parameters
        ----------
        assetamount: int
            the amount of assets, for NFT this should be  0.00000001
        reissuancetokens: int
            the token used to issue new amouint of this token, for NFT should be 0
        blind: bool
            will the transaction be blinded or not

        Returns
        --------
        dict
            {txid, vin, entroy, asset, token}
            txid (hex) of the issuance
            vin (int) input to the txid
            entropy (hex)
            asset (hex), id of the asset
            token (hex), id of the token for re-issuance

        Raises
        ------
        Exception
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content

        """
        return

    def get_new_address(self, label):
        """
        get a new address from the node


        Parameters
        ----------
        label: str
            label to attach to the address

        Returns
        -------
        str
            the new address

        Raises
        ------
        Exception
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content

        """
        t = make_rpc_call.delay('getnewaddress')
        return self._check_rpc_error(t.wait())

    def issue_lnft_asset(self, blind, asset_address, name, ticker, domain):
        """
        issue NFT asset

        the function sends the transaction to the network (i.e. spends the inputs) and then returns the assetId

        Parameters
        ----------
        name: str
            name of the asset for the asset registry
        ticker: str (max 5 chars)
            ticker for the asset in the asset registry
        domain: str
            domain that the asset registry will look to find the ./well-known/liquid-asset-proof-<asset-id>
        asset_address: str
            the address where the new issued asset will be deposited
        blind: bool
            will the transaction be blinded or not

        Returns
        --------
        dict
            {txid, vin, entroy, asset, token}
            txid (hex) of the issuance
            vin (int) input to the txid
            entropy (hex)
            asset (hex), id of the asset
            token (hex), id of the token for re-issuance

        Raises
        ------
        Exception
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content

        """

        pubkey = self.get_serialized_pub_key().hex()

        contract_obj = {
                        'name': name,
                        'ticker': ticker,
                        'precision': 0,
                        'entity': {'domain': domain},
                        'issuer_pubkey': pubkey,
                        'version': 0
                        }
        contract = json.dumps(contract_obj, separators=(',',':'), sort_keys=True)

        digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        digest.update(contract.encode('ascii'))
        bytes_digest = digest.finalize()
        contract_hash_rev = bytes_digest[::-1].hex()

        #prepare the tx
        ins = []
        outs = [{'data': '4E4654'}] #NFT OP_RETURN
        t = make_rpc_call.delay('createrawtransaction', ins, outs)
        raw_tx = self._check_rpc_error(t.wait())
        t = make_rpc_call.delay('fundrawtransaction', raw_tx) #fundrawtransaction può prendere in unpuit gli output che vuoi
        funded_tx = self._check_rpc_error(t.wait())  # fix fees

        #add the NFT issuance
        t = make_rpc_call.delay('rawissueasset', funded_tx['hex'], [{'asset_amount':0.00000001, 'asset_address':asset_address, 'blind':blind, 'contract_hash':contract_hash_rev}] )
        raw_issuance = self._check_rpc_error(t.wait())

        #blind sign and send
        t = make_rpc_call.delay('blindrawtransaction', raw_issuance[0]['hex'])
        blinded_tx = self._check_rpc_error(t.wait())
        t = make_rpc_call.delay('signrawtransactionwithwallet', blinded_tx)
        signed_tx = self._check_rpc_error(t.wait())
        t = make_rpc_call.delay('sendrawtransaction', signed_tx['hex'])
        sent_tx = self._check_rpc_error(t.wait())
        return (raw_issuance[0])


    def list_issuances(self, *asset):
        """
        return all the assets issued or details of a specific asset

        Note that as of now if you subit an asset that doesn't exist all assets are returned anyway

        Parameters
        ----------
        asset: str [optional]
            hex, the id of the asset

        Returns
        --------
        [dict]
            {isresissuance, .... txid, vin, entroy, asset, token}
            txid (hex) of the issuance
            vin (int) input to the txid
            entropy (hex)
            asset (hex), id of the asset
            token (hex), id of the token for re-issuance

        Raises
        ------
        Exception
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content

        """
        t = make_rpc_call.delay('listissuances', *asset)
        return self._check_rpc_error(t.wait())

    def sign_message(self, message):
        """
        sign a message  with one of the keys avaialble on the node

        before doing the signature this methods checks if the message
        needs to be converted to bytes

        Parameters
        ----------
        message: str/bytes
            the message to be signed by the node, if str, it gets encoded first

        Returns
        --------
        dict
            {signed, serialized_public, address}, a JSON containind the
            signed messaged and its respective serialized public key and
            the address from which the confidential key was taken

        Raises
        ------
        Exception
            sign_bytes_message uses celery to retrieve keys from the node
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content

            if the message is not a string, not a bytes array,
            a general Exception is raised complaining about the message type
        """
        if isinstance(message, str):
            return self.sign_bytes_message(message.encode())
        elif isinstance(message, bytes):
            return self.sign_bytes_message(message)
        else:
            raise Exception('Corrupt message type')

    def sign_bytes_message(self, message):
        """
        sign a message with the local liquid node

        get a local address, use its embedded.confidential_key as private key
        and return the signed message and its serialized public_key

        Parameters
        ----------
        message: bytes
            the message to be signed by the node

        Returns
        --------
        dict
            {signed, serialized_public}, a JSON containind the
            signed messaged and its respective serialized public key both in bytes

        Raises
        ------
        Exception
            sign_bytes_message uses celery to retrieve keys from the node
            every time a response from celery if checked (_check_rpc_error)
            if an error was reported by the node, an Exception is raised
            with the error message as content

            if the node is new and does not have addresses this fails

        """
        return self._kmc.sign_message(message)

    def check_message(self, signed, message, serialized_public):
        """
        check if a message and a signature are matching given the pub key

        Parameters
        ----------
        signed: bytes
            the signed version

        message: str or bytes
            the original message that was signed

        serialized_public: bytes
            the public kley

        Returns
        --------
        bool
            True if the signed message check out, False otherwise.
            Note that anything going wrong is catched and
            the method just returns False
        """
        return self._kmc.verify_signed_message(signed, message, serialized_public)

    def get_serialized_pub_key(self):
        """
        Get the serialised public key with PEM encoding of this eonpass node

        Params
        ------

        Returns
        -------
        bytes
            the serialised data with the PEM encoding

        Raises
        ------

        """
        return self._kmc.get_serialized_pub_key()

    def digest(self, message):
        """
        digest a message, perform SHA256

        first check if the message is a string, if not .encode() it,
        then perform the digest operation

        Parameters
        ----------
        message: str or bytes
            the message to be hashed, default encode(utf-8) used if string

        Returns
        --------
        bytes
            the hashed message in bytes

        Raises
        ------
        Exception
            if the input message is not a string or bytes raise a
            general exception

        """
        digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        if(isinstance(message, str)):
            digest.update(message.encode())
        elif(isinstance(message, bytes)):
            digest.update(message)
        else:
            raise Exception('Corrupt message type')
        bytes_digest = digest.finalize()
        return bytes_digest

    def digest_from_hex(self, message):
        """
        digest a message, perform SHA256

        first check if the message is a string, if not .encode() it,
        then perform the digest operation

        Parameters
        ----------
        message: str 
            the message to be hashed, expected to be hex format

        Returns
        --------
        bytes
            the hashed message in bytes

        Raises
        ------
        Exception
            if the input message is not a hex string

        """
        digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        message_bytes = bytes.fromhex(message)
        digest.update(message)
        bytes_digest = digest.finalize()

    def hexdigest(self, message):
        """
        return the hexadecimal representation of the message digest

        it uses the method "digest" which will check for the message
        type, fix it to bytes eventually, and hash it. Once that is done
        just hex the result and return.

        Parameters
        ----------
        message: str or bytes
            the message (utf-8 encoded if it's string) to be hashed and then cast to hex

        Returns
        --------
        str
            the hexadecimal representation of the hash

        Raises
        ------
        Exception
            if the input message is not a string or bytes digest
            raises exception
        """
        return self.digest(message).hex()

    def provable_sha256_transform(self, msg_bytes):
        """
        Execute only one sha_transform operation, which is the one we can build zkp with libsnark for

        -get the clear text message in input
        -cast it to bytes and take the first 512 bits
        -split in left and right
        -execute the sha_transfrom
        -return a dict with left, right, output, formatted in 32bit hexadecimal words

        Parameters
        ----------
        msg_bytes: bytes
            the message to be split in 2 and then passed to sha_transform

        Returns
        --------
        dict
            the hexadecimal representation of the hash

        Raises
        ------
        Exception
            when assrtions fail

        """
        #prepare the message:
        if len(msg_bytes)>64:
            msg_bytes = msg_bytes[:64]
        else:
            n = 64 - len(msg_bytes)
            for i in range(n):
                msg_bytes+=bytes([0])

        if(len(msg_bytes)!=64): raise Exception("invalid message size")
        left = msg_bytes[:32]
        right = msg_bytes[32:]
        #execute one sha_transform
        state = pypy_sha256.sha_init()
        state['data'] = self.words_to_bytes(self.bytes_to_words(msg_bytes))
        pypy_sha256.sha_transform(state)
        res = self.words_to_bytes(self.bytes_to_words(self.words_to_bytes(state['digest'])))
        #prepare dict to be returned
        pack = {
            'left': self.cpp_val(left),
            'right': self.cpp_val(right),
            'output': self.cpp_val(res)
        }
        return pack

    def words_to_bytes(self, arr):
        """
        transform an array of integers to an array of integer representing 8bits each

        this is used to transform an integer representing 32bits to 4 int representing 8bit each
        all little-endiands. So the first number in  arr will be converted into 4 numbers, the first
        is the first 8 bits, the send the bits from 8 to 16, etc...
        e,g, (1,2) -> (0,0,0,1,0,0,0,2)

        Parameters
        ----------
        arr [int]
            the array of ints representing 32bits numbers to transform

        Returns
        -------
        [int]
            the array with each entry representing 8 bits

        """
        return sum(([x >> 24, (x >> 16) & 0xff, (x >> 8) & 0xff, x & 0xff] for x in arr), [])

    def bytes_to_words(self, arr):
        """
        transform an array of integers to an array of integers representing 32bits each big-endian
        """
        l = len(arr)
        assert l % 4 == 0
        return [(arr[i*4 + 3] << 24) + (arr[i*4+2] << 16) + (arr[i*4+1] << 8) + arr[i*4] for i in range(l//4)]

    def bytes_to_words_little_endian(self, arr):
        """
        transform an array of integers to an array of integers representing 32bits each little-endian
        """
        l = len(arr)
        assert l % 4 == 0
        return [(arr[i*4 + 3]) + (arr[i*4+2] << 8) + (arr[i*4+1] << 16) + (arr[i*4] << 24) for i in range(l//4)]

    def cpp_val(self, s, log_radix=32):
        """
        create a str representing the concatenation of the 8 32-bit words expressed in hex format as digested by cpp
        """
        if log_radix == 8:
            hexfmt = '0x%02x'
        elif log_radix == 32:
            hexfmt = '0x%08x'
            s = self.bytes_to_words(s)
        else:
            raise
        return (','.join(hexfmt % x for x in s))


    def check_block(self, blockhash):
        """
        return the block info from the hash

        Parameters
        ----------
        blockhash: str
            the block hash of the block to retrieve

        Returns
        --------
        dict
            the json representation of the block

        Raises
        ------
        Exception
            if an error was reported by the node, an Exception is raised
            with the error message as content
        """
        t = make_rpc_call.delay('getblock', blockhash)
        return self._check_rpc_error(t.wait())

    def _check_rpc_error(self, response):
        """
        celery task returns the the response from the node wrapped, unwrap it

        if there is an error, raise an exception with the error as body

        Parameters
        ----------
        response: dict
            it has in 'response' the response payload from the celery task, which is
            composed of a result key with the actual result and an optional error key
            which has the eventual error

        Returns
        --------
        dict
            the result from the node

        Raises
        ------
        Exception
            if the reponse has the error field, raise Exception and report the value
        """
        if 'error' in response:
            raise Exception(response['error'])
        return response['result']

cu = CryptoUtils()
