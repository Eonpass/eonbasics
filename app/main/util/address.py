import math
from binascii import unhexlify, hexlify

import app.main.util.base58 as base58
import app.main.util.schnorr as schnorr

from app.main.util.crypto import cu


def orderlen(order):
    return (1 + len("%x" % order)) // 2  # bytes


def int_to_32_bytes(x):
    return x.to_bytes(32, 'big')

def int_to_bytes(x):
    l = orderlen(x)
    return x.to_bytes(l, 'big')


def privkey_to_wif(privkey, is_test=True):
	"""
    Create the WIF representation of a private key

	Run the routing designed here: https://en.bitcoin.it/wiki/Wallet_import_format
    Uses "+" for bytes concatenation, works only with bytes
	
    Parameters
    ----------
    privkey: bytes
        bytes of the 256 bits privkey

    is_test: bool
        True for test environment, False for Production (mainnet)

    Returns
    --------
    string
        base58encoded

    Raises
    ------
    Exception
        If key is not bytes, the concatenation doesn't work anymore

	"""
	prefix = bytes.fromhex("ef") if is_test else bytes.fromhex("80") #sets key for testnet or mainnet
	suffix = bytes.fromhex("01") #01 means this will correspond to a compressed public key
	extended_key = prefix+privkey+suffix
	hash_1 = cu.digest(extended_key)
	hash_2 = cu.digest(hash_1)

	check_sum = hash_2[0:4]

	pre_encode = extended_key+check_sum

	encoded_wif = base58.encode(pre_encode)

	return encoded_wif

def wif_to_privkey(wif, is_test=True):
    """
    From the wif string, build back the bytes representing the private key

    Parameters
    ----------
    wif: str
        wif represented private key

    is_test: bool
        True for test environment, False for Production (mainnet)

    Returns
    --------
    bytes
        256 bits of private key

    Raises
    ------
    Exception
        If the deserialisation fails
    """

    decoded_wif = base58.decode(wif)
    extended_key = decoded_wif[:-4]
    #TODO: check the checksum
    key = extended_key[1:-1]
    return key


def serialize_pubkey(pubkey):
    """ 
    Serializes an ECPoint (public key).

    Creates the compressed hex bytes representation of the public key

    Parameters
    ----------
    pubkey: ECPoint

    Returns
    --------
    bytes
        hexlified bytes of the compressed public key (NOTE: you need to .decode("utf-8") this before passing it to deserialize!)

    Raises
    ------
    Exception
        in case the object is not of the expected type and does not have get_x get_y
    
    """
    order = schnorr.getCurve()[1]
    l = orderlen(order)

    x_bytes = pubkey.get_x().to_bytes(l, byteorder="big")

    if pubkey.get_y() & 1:
        prefix = b'03'
    else:
        prefix = b'02'

    s_key = prefix + hexlify(x_bytes)

    return s_key

def deserialize_pubkey(pubkey):
    """
    Convert a compressed serialized pubkey to ECPoint

    Parameters
    ----------
    pubkey: string, 66 hex digits representing the compressed public key

    Returns
    --------
    ECPoint
        the point on the curve

    Raises
    ------
    Exception
        in case the point is not on the curve?
    
    """
    order = schnorr.getCurve()[1]
    p = schnorr.set_secp256p1().p

    prefix = pubkey[0:2]
    x_str = pubkey[2:]
    x_bytes = unhexlify(x_str)
    x = int.from_bytes(x_bytes, "big")

    y_sq = (pow(x, 3, p) + 7) % p
    y = pow(y_sq, (p + 1) // 4, p)
    if (prefix=="02" and y & 1 or prefix == "03" and not y & 1):
        y = (-y%p)
    
    return schnorr.ECpoint(schnorr.set_secp256p1(),x,y,1)



