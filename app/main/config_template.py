import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_precious_secret_key')
    DEBUG = False

class DevelopmentConfig(Config):
    KMI_TYPE = 'DEV'
    ADMIN_EMAIL = 'dev@somedomain.com'
    ADMIN_USERNAME = 'admindev'
    ADMIN_PASSWORD = 'secretpassword'
    DEBUG = True
    LOG_ON_FILE = False
    LOCAL_FLASK_PORT = os.getenv('FLASK_PORT', 5000)
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'dev.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    RPC_PASSWORD = 'YOUR_PWD'
    RPC_USER = 'YOUR_USER'
    RPC_HOST = '0.0.0.0'
    RPC_PORT = 18886
    TEST_EXPLORER_PASSWORD = 'YOUR_PWD'
    CELERY_BROKER_URL = 'redis://localhost:6379'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379'
    EONBRIDGE_EXEC_PATH = ''
    EONBRIDGE_FILES_PATH = ''

class TestingConfig(Config):
    KMI_TYPE = 'DEV'
    ADMIN_EMAIL = 'test@somedomain.com'
    ADMIN_USERNAME = 'admintest'
    ADMIN_PASSWORD = 'secretpassword'
    DEBUG = True
    TESTING = True
    LOG_ON_FILE = False
    LOCAL_FLASK_PORT = os.getenv('FLASK_PORT', 5000)
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'test.db')
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    RPC_PASSWORD = 'YOUR_PWD'
    RPC_USER = 'YOUR_USER'
    RPC_HOST = '0.0.0.0'
    RPC_PORT = 18886
    TEST_EXPLORER_PASSWORD = 'YOUR_PWD'
    CELERY_BROKER_URL = 'redis://localhost:6379'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379'
    EONBRIDGE_EXEC_PATH = ''
    EONBRIDGE_FILES_PATH = ''

class ProductionConfig(Config):
    KMI_TYPE = 'DEV'
    DEBUG = False
    LOG_ON_FILE = True
    LOCAL_FLASK_PORT = os.getenv('FLASK_PORT', 5000)
    # uncomment the line below to use postgres
    # postgres_local_base = os.environ['DATABASE_URL']
    # SQLALCHEMY_DATABASE_URI = postgres_local_base
    RPC_PASSWORD = ''
    RPC_USER = ''
    RPC_HOST = ''
    RPC_PORT = ''
    CELERY_BROKER_URL = ''
    CELERY_RESULT_BACKEND = ''
    EONBRIDGE_EXEC_PATH = ''
    EONBRIDGE_FILES_PATH = ''


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)

key = Config.SECRET_KEY
