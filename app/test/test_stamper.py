import unittest
import os.path
import json
import uuid
import base58
import base64
import time
from flask import current_app

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.asymmetric import ec

from app.main.service.notarizationsession_service import save_new_notarization_session
from app.main.service.notarizationitem_service import save_new_notarization_item, get_notarization_item_proof
from app.main.util.stamperutils import stamper
from app.main.util.ebsi_client import EBSIClient
from app.main.util.keymanagementutils import KeyManagementClient
from app.test.base import BaseTestCase

def create_new_key():
    private_key = ec.generate_private_key(
        ec.SECP256K1(), 
        default_backend()
    )
    pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )
    compressed_public_key_bytes = private_key.public_key().public_bytes(
        encoding=serialization.Encoding.X962,
        format=serialization.PublicFormat.CompressedPoint
    )
    serialized_public_key = private_key.public_key().public_bytes(
        encoding=serialization.Encoding.X962,
        format=serialization.PublicFormat.UncompressedPoint
    )
    x_coordinate = private_key.public_key().public_numbers().x
    y_coordinate = private_key.public_key().public_numbers().y
    constructed = x_coordinate.to_bytes(32, byteorder='big')+y_coordinate.to_bytes(32, byteorder='big')

    x_bytes = int.to_bytes(x_coordinate, length=(x_coordinate.bit_length() + 7) // 8, byteorder='big')
    y_bytes = int.to_bytes(y_coordinate, length=(y_coordinate.bit_length() + 7) // 8, byteorder='big')
    x_base64url = base64.urlsafe_b64encode(x_bytes).decode('utf-8').rstrip('=')
    y_base64url = base64.urlsafe_b64encode(y_bytes).decode('utf-8').rstrip('=')
    # Prefix for the DID
    prefix = bytes.fromhex("d1d603")
    jwk = {
        "crv": "secp256k1",
        "kty": "EC",
        "x": x_base64url,
        "y": y_base64url
    }
    jwk_json = json.dumps(jwk, sort_keys=True, separators=(',', ':')).encode('utf-8')
    # Encode with base58 zQ3s
    did2 = "did:key:z" + base58.b58encode(prefix+jwk_json).decode()
    return pem, did2

def get_ebsi_registered_test_jwk():
    secret = os.environ.get("EBSI_TEST_KEY_TEST") if os.environ.get("EBSI_TEST_KEY_TEST") else ""
    keyNetCompany = {"kty":"EC","d":secret,"use":"sig","crv":"secp256k1","kid":"Z29zmAyNmdrkH8FthU5TozZtoaG5JJayg8w_39LVImo","x":"LVrz7CSxGWNlJcfEpdUAL2i3RoPb4hXZ66QpgrzXuNk","y":"ZPWxleejHdM-M5FsBXGzt6fRIXLVuMJjelpavNDfd3Q"}
    return keyNetCompany;

def get_ebsi_registered_pilot_jwk():
    secret = os.environ.get("EBSI_PILOT_KEY_TEST") if os.environ.get("EBSI_PILOT_KEY_TEST") else ""
    key ={"kty":"EC","d":secret,"use":"sig","crv":"secp256k1","kid":"aGXnMHQ9mDfJebLOsPuP2de77BhaG2AMfiCpH5Nl4a4","x":"g2THhNx51kOmzAKPxLl4fHQ9L938FfrUX0S3zF6MqRw","y":"PrRPWAwfzLMrXwNt3mpLzvRny4RTVNlsH-MgBRCFICA"}
    return key;

class TestStamperFactory(BaseTestCase):

    def test_stamper_factory(self):
        """ Just get the std stampter, the notarisation steps are teste in test_notarization """
        #test the loaded type of KMC according to config
        self.assertTrue(stamper._get_stamper()._type == 'STD')

    def test_ebsi_stamper(self):
        """
        Use the ebsi stamper factory to create a stamper, 
        create a notarization session with proper data and try to call send and lock
        """
        #set up owner node, brand owner:
        key = get_ebsi_registered_pilot_jwk()
        #key = get_ebsi_registered_test_jwk()
        kmc = KeyManagementClient()
        test_priv_key = kmc.convert_jwk_to_pem(key)
        kmc.overwrite_local_key(test_priv_key)
        pub_key = kmc.get_jwk_pubkey()
        #setup a ff:
        ffpem, ffdid = create_new_key() #create random keys simulating a FF

        #setup notarization
        new_notarization = {
            "type":"ebsi"
        }
        res = save_new_notarization_session(new_notarization)[0]
        notarization_session_id = res.get("public_id", None)
        self.assertTrue(notarization_session_id)
        notes_payload = {
            "shipment_id":str(uuid.uuid4()),
            "auth_dids":[ffdid]
        }
        message = str(uuid.uuid4())
        digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        digest.update(("some more test"+message).encode('utf-8'))
        item_payload = {
            'notarization_session_id':notarization_session_id,
            'message_hash': digest.finalize().hex(),
            'ebsi_notes':json.dumps(notes_payload, separators=(',', ':')).encode('utf-8')
        }
        print(item_payload)
        res = save_new_notarization_item(item_payload)[0]
        item_id = res["public_id"]
        self.assertTrue(item_id)

        updated_session = stamper.lock(notarization_session_id, stamper_type="EBSI")
        self.assertTrue(updated_session.merkle_root)
        final_confirmation_tx = stamper.stamp(notarization_session_id, stamper_type="EBSI")
        self.assertTrue(final_confirmation_tx)
        proof = get_notarization_item_proof(item_id)
        print(proof)
        self.assertTrue(proof.get("merkle_item",None))




if __name__ == '__main__':
    unittest.main()
