import datetime
import unittest

from app.test.base import BaseTestCase
from app.main.util.eonutils import eonutils

class TestMerkleTools(BaseTestCase):

    DEBUG = False

    def test_merkle_tree_comp_and_proof(self):
        """
        create a merkle tree and then request the proof and test it
        """
        items = ['9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a07', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a06']
        merkle_root = eonutils.create_merkle_root(items)
        leaf = '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08'
        merkle_proof = eonutils.create_merkle_proof(leaf, items, merkle_root)
        self.assertTrue(len(merkle_proof)==2)

        wrong_root = False
        try:
            #wrong root on proof creation:
            corrupt_root = '1337'
            merkle_proof = eonutils.create_merkle_proof(leaf, items, corrupt_root)
        except Exception as e:
            wrong_root = True
        self.assertTrue(wrong_root)

        #test the proof:
        self.assertTrue(eonutils.execute_proof(merkle_proof, merkle_root, leaf))

        #wrong root on proof execution:
        wrong_root = False
        try:
            #wrong root on proof creation:
            corrupt_root = '1337'
            merkle_proof = eonutils.execute_proof(merkle_proof, corrupt_root, leaf)
        except Exception as e:
            wrong_root = True
        self.assertTrue(wrong_root)

        #wronf leaf on proof execution:
        wrong_leaf = False
        try:
            #wrong root on proof creation:
            corrupt_leaf = '1337'
            merkle_proof = eonutils.execute_proof(merkle_proof, merkle_root, corrupt_leaf)
        except Exception as e:
            wrong_leaf = True
        self.assertTrue(wrong_leaf)


    def test_merkle_tree_comp_and_proof_even(self):
        """
        create a merkle tree and then request the proof and test it
        """
        items = ['9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a07', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a06', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a05']
        merkle_root = eonutils.create_merkle_root(items)
        leaf = '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08'
        merkle_proof = eonutils.create_merkle_proof(leaf, items, merkle_root)
        self.assertTrue(len(merkle_proof)==2)
        
        wrong_root = False
        try:
            #wrong root on proof creation:
            corrupt_root = '1337'
            merkle_proof = eonutils.create_merkle_proof(leaf, items, corrupt_root)
        except Exception as e:
            wrong_root = True
        self.assertTrue(wrong_root)

        #test the proof:
        self.assertTrue(eonutils.execute_proof(merkle_proof, merkle_root, leaf))

        #wrong root on proof execution:
        wrong_root = False
        try:
            #wrong root on proof creation:
            corrupt_root = '1337'
            merkle_proof = eonutils.execute_proof(merkle_proof, corrupt_root, leaf)
        except Exception as e:
            wrong_root = True
        self.assertTrue(wrong_root)

        #wronf leaf on proof execution:
        wrong_leaf = False
        try:
            #wrong root on proof creation:
            corrupt_leaf = '1337'
            merkle_proof = eonutils.execute_proof(merkle_proof, merkle_root, corrupt_leaf)
        except Exception as e:
            wrong_leaf = True
        self.assertTrue(wrong_leaf)

    def test_three_levels(self):
        items = ['9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a07', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a06', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a05', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a04']
        merkle_root = eonutils.create_merkle_root(items)
        leaf = '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08'
        merkle_proof = eonutils.create_merkle_proof(leaf, items, merkle_root)
        self.assertTrue(len(merkle_proof)==3)
        
        #test the proof:
        self.assertTrue(eonutils.execute_proof(merkle_proof, merkle_root, leaf))


if __name__ == '__main__':
    unittest.main()