import unittest

from app.main.model.document import Document
from app.main.model.txo import Txo
from app.main.service.document_service import (delete_document, save_new_document, update_document, get_a_document, check_seals_lock_status)
from app.main.service.txo_service import (save_new_txo, get_a_txo, unlock_a_txo)
from app.main.util.crypto import cu
from app.main.util.eonerror import EonError

from app.test.base import BaseTestCase



class TestDocumentModel(BaseTestCase):
    DEBUG = False

    def test_new_document_creation_with_fake_data(self):
        """ Insert a document with an unkown seal id, then try to insert another one on the same seal id """
        payload = {
            'document_hash': 'test',
            'current_seal' : '2348ksdhksdhf34947892'
        }
        insert_response = save_new_document(payload)
        self.assertTrue(insert_response[0]['status'] == 'success')
        self.assertTrue(insert_response[1]==201)
        doc = get_a_document(insert_response[0]['public_id'], "true")
        self.assertTrue(doc.status=='corrupt current seal id')
        #try to insert again and check a doc with the same data cannot be inserted
        payload = {
            'document_hash': 'test',
            'current_seal' : '2348ksdhksdhf34947892'
        }
        try:
            insert_response = save_new_document(payload)
        except EonError as e:
            self.assertTrue(e.code == 409)
            self.assertTrue(e.message == 'Another document already exists with the given seals.')

    def test_new_document_update_with_fake_data(self):
        """ insert a blank document, then update id with unknown seal id"""
        payload = {
            'document_hash': 'test'
        }
        insert_response = save_new_document(payload)
        self.assertTrue(insert_response[0]['status'] == 'success')
        self.assertTrue(insert_response[1]==201)
        doc = get_a_document(insert_response[0]['public_id'], "true")
        self.assertTrue(doc.status=='new')
        #try to add a fake TXO id
        payload = {
            'public_id' : insert_response[0]['public_id'],
            'current_seal' : '2348ksdhksdhf34947892'
        }
        update_response = update_document(payload)
        self.assertTrue(update_response[0]['status'] == 'success')
        self.assertTrue(update_response[1] == 200)
        doc = get_a_document(insert_response[0]['public_id'], "true")
        self.assertTrue(doc.status=='corrupt current seal id')

    def test_new_document_creation_and_removal(self):
        """ Create a document with unknown seal id, then remove it """
        payload = {
            'document_hash': 'test',
            'current_seal' : '2348ksdhksdhf34947892'
        }
        insert_response = save_new_document(payload)
        self.assertTrue(insert_response[0]['status'] == 'success')
        self.assertTrue(insert_response[1]==201)
        doc = get_a_document(insert_response[0]['public_id'], "true")
        self.assertTrue(doc.status=='corrupt current seal id')
        #delete, then check you get 404 for the id
        delete_response = delete_document(insert_response[0]['public_id'])
        self.assertTrue(delete_response[0]['status']=='success')
        self.assertTrue(delete_response[0]['message']=='Successfully deleted.')
        try:
            doc = get_a_document(insert_response[0]['public_id'], "true")
        except EonError as e:
            self.assertTrue(e.code==404)

    def test_new_document_real_data_in_two_steps(self):
        """ Create a document with document text only, then updated it to add the current_seal and test it's still new """
        
        utxos = cu.get_utxos()
        self.assertTrue(len(utxos) >= 2)
        txo_public_ids = []
        for utxo in utxos:
            payload = {
                'txid': utxo['txid'],
                'voutn': utxo['n'],
                'value': utxo['amount']
            }
            new_txo_response = save_new_txo(payload)  
            txo_public_ids.append(new_txo_response[0]['public_id'])
            if(len(txo_public_ids)>2): 
                break
        payload = {
            'document_hash': 'test'
        }
        insert_response = save_new_document(payload)
        self.assertTrue(insert_response[0]['status'] == 'success')
        self.assertTrue(insert_response[1]==201)
        doc = get_a_document(insert_response[0]['public_id'], "true")
        self.assertTrue(doc.status=='new')
        payload = {
            'public_id' : insert_response[0]['public_id'],
            'current_seal' : txo_public_ids[0]
        }
        update_response = update_document(payload)
        self.assertTrue(update_response[0]['status'] == 'success')
        self.assertTrue(update_response[1] == 200)
        doc = get_a_document(insert_response[0]['public_id'], "false")
        self.assertTrue(doc.status=='new')
        #the txo is now locked on the node, unlock it to return to the initial state on the node:
        unlock_a_txo(doc.current_seal)

    def test_mass_utxo_lock(self):
        """ Lock all the current seals and unspent next seals """
        
        utxos = cu.get_utxos()
        self.assertTrue(len(utxos) >= 2)
        txo_public_ids = []
        for utxo in utxos:
            payload = {
                'txid': utxo['txid'],
                'voutn': utxo['n'],
                'value': utxo['amount']
            }
            new_txo_response = save_new_txo(payload)  
            txo_public_ids.append(new_txo_response[0]['public_id'])
            if(len(txo_public_ids)>2): 
                break
        payload = {
            'document_hash': 'test'
        }
        insert_response = save_new_document(payload)
        self.assertTrue(insert_response[0]['status'] == 'success')
        self.assertTrue(insert_response[1]==201)
        doc = get_a_document(insert_response[0]['public_id'], "true")
        self.assertTrue(doc.status=='new')
        rpc_response = check_seals_lock_status({})
        self.assertTrue(rpc_response[1]==200)
        self.assertTrue("1 out of 1" in rpc_response[0]['message'])#there 1 document and we expect to succeed
        res = unlock_a_txo(doc.current_seal)
        self.assertTrue(res) #returns true when it's already locked
        #TODO make either a call to check the lock status or return/raise something more meaningful rather than True/False
        
    def test_document_lifecycle_happy_flow(self):
        """ Get 2 utxos and use them as seals, prepare the message, the signatures and test the states from new to sent """
        #get the Txos:
        
        utxos = cu.get_utxos()
        self.assertTrue(len(utxos) >= 2)
        txo_public_ids = []
        for utxo in utxos:
            payload = {
                'txid': utxo['txid'],
                'voutn': utxo['n'],
                'value': utxo['amount']
            }
            new_txo_response = save_new_txo(payload)  
            txo_public_ids.append(new_txo_response[0]['public_id'])
            if(len(txo_public_ids)>2): 
                break
        #create new document:
        payload = {
            'document_hash' : '7e57',
            'current_seal' : txo_public_ids[0]
        }
        insert_response = save_new_document(payload)
        self.assertTrue(insert_response[0]['status'] == 'success')
        doc = get_a_document(insert_response[0]['public_id'], "true")
        #NOTE: if cached = true, 
        # current seal here has last_checked_status unknown, 
        #TODO: decide if it's useful to have a specific state for this scenario.
        doc = get_a_document(insert_response[0]['public_id'], "false")
        #current_seal is new, next_seal is missing => new
        self.assertTrue(doc.status=='new') 
        #add next_seal, by saving it the operation creates the document_with_seal_hash
        payload = {
            'public_id' : insert_response[0]['public_id'],
            'next_seal' : txo_public_ids[1],
        }
        update_response = update_document(payload)
        self.assertTrue(update_response[0]['status'] == 'success')
        self.assertTrue(update_response[1] == 200)
        doc = get_a_document(insert_response[0]['public_id'], "true")
        self.assertTrue(doc.document_with_seals_hash!=None)
        #now sign the message and insert the data in the fields dedicated to the sender
        signed_message = cu.sign_message(bytes.fromhex(doc.document_with_seals_hash))
        update_payload = {
            'public_id': insert_response[0]['public_id'],
            'sender_signed_hash': signed_message['signed'].hex(),
            'sender_public_key': signed_message['serialized_public'].decode('utf-8')
        }
        update_response = update_document(update_payload)
        #NOTE: the next_seal is new and the status for never checked, so it returns corrupt if we query with cached = true
        doc = get_a_document(insert_response[0]['public_id'], "true")
        #refresh the status of the seals with cached = false:
        doc = get_a_document(insert_response[0]['public_id'], "false")
        self.assertTrue(doc.status=='preflight')
        # double sign, using the receiver signature,
        # check that status is ready:
        receiver_signed_message = cu.sign_message(bytes.fromhex(doc.sender_signed_hash))
        update_payload = {
            'public_id': insert_response[0]['public_id'],
            'sender_and_receiver_signed_hash': receiver_signed_message['signed'].hex(),
            'receiver_public_key': receiver_signed_message['serialized_public'].decode('utf-8')
        }
        update_response = update_document(update_payload)
        doc = get_a_document(insert_response[0]['public_id'], "true") #here you can use the cached version since the two above did query the node and have the last_checked_status
        self.assertTrue(doc.status=='ready')
        # spend the seal with the signed hash and check that
        # the status is "sent":
        txo = get_a_txo(txo_public_ids[0], "true")
        sent_tx = cu.spend_txo(  # spentx
            txo.txid, txo.voutn,
            cu.hexdigest(doc.sender_and_receiver_signed_hash))
        doc = get_a_document(insert_response[0]['public_id'], "false")
        self.assertTrue(doc.status == 'sent')
        #create a block to confirm the transaction
        self.requestNewTestBlock()
        #unlock txos
        #current_seal is spent, can't unlock it
        unlock_a_txo(doc.next_seal)


    def test_document_lifecycle_spend_seal_at_wrong_time(self):
        """ Prepare a document, but spent the current seal before having the complete sender and receveir signatures """
        #get the Txos:
        
        utxos = cu.get_utxos()
        self.assertTrue(len(utxos) >= 2)
        txo_public_ids = []
        for utxo in utxos:
            payload = {
                'txid': utxo['txid'],
                'voutn': utxo['n'],
                'value': utxo['amount']
            }
            new_txo_response = save_new_txo(payload)  
            txo_public_ids.append(new_txo_response[0]['public_id'])
            if(len(txo_public_ids)>2): 
                break
        #create new document:
        payload = {
            'document_hash' : '7e57',
            'current_seal' : txo_public_ids[0]
        }
        insert_response = save_new_document(payload)
        self.assertTrue(insert_response[0]['status'] == 'success')
        doc = get_a_document(insert_response[0]['public_id'], "true")
        #NOTE: if cached = true, 
        # current seal here has last_checked_status unknown, 
        #TODO: decide if it's useful to have a specific state for this scenario.
        doc = get_a_document(insert_response[0]['public_id'], "false")
        #current_seal is new, next_seal is missing => new
        self.assertTrue(doc.status=='new') 
        #add next_seal, by saving it the operation creates the document_with_seal_hash
        payload = {
            'public_id' : insert_response[0]['public_id'],
            'next_seal' : txo_public_ids[1],
        }
        update_response = update_document(payload)
        self.assertTrue(update_response[0]['status'] == 'success')
        self.assertTrue(update_response[1] == 200)
        doc = get_a_document(insert_response[0]['public_id'], "true")
        self.assertTrue(doc.document_with_seals_hash!=None)
        #now sign the message and insert the data in the fields dedicated to the sender
        signed_message = cu.sign_message(bytes.fromhex(doc.document_with_seals_hash))
        update_payload = {
            'public_id': insert_response[0]['public_id'],
            'sender_signed_hash': signed_message['signed'].hex(),
            'sender_public_key': signed_message['serialized_public'].decode('utf-8')
        }
        update_response = update_document(update_payload)
        #NOTE: the next_seal is new and the status for never checked, so it returns corrupt if we query with cached = true
        doc = get_a_document(insert_response[0]['public_id'], "true")
        #refresh the status of the seals with cached = false:
        doc = get_a_document(insert_response[0]['public_id'], "false")
        self.assertTrue(doc.status=='preflight')
        # spend current seal before having the receiver signatures:
        txo = get_a_txo(txo_public_ids[0], "true")
        sent_tx = cu.spend_txo(  # spentx
            txo.txid, txo.voutn,
            cu.hexdigest(doc.sender_signed_hash)) #use the sender signed message for example
        doc = get_a_document(insert_response[0]['public_id'], "false") #cached = false, to get the new spent status of the seal
        self.assertTrue(doc.status == 'corrupt current seal')
        #create a block to confirm the transaction
        self.requestNewTestBlock()
        #unlock txos
        #current_seal is spent, can't unlock it
        unlock_a_txo(doc.next_seal)


    def test_document_lifecycle_missing_public_key(self):
        """ create a document and fail at upating it with the proper signature data, check the corrupt signature status """
        #prep Txos:
        
        utxos = cu.get_utxos()
        self.assertTrue(len(utxos) >= 2)
        txo_public_ids = []
        for utxo in utxos:
            payload = {
                'txid': utxo['txid'],
                'voutn': utxo['n'],
                'value': utxo['amount']
            }
            new_txo_response = save_new_txo(payload)  
            txo_public_ids.append(new_txo_response[0]['public_id'])
        #create new document:
        payload = {
            'document_hash' : '7e57',
            'current_seal' : txo_public_ids[0]
        }
        insert_response = save_new_document(payload)
        doc = get_a_document(insert_response[0]['public_id'], "true")
        #add next_seal, by saving it the operation creates the document_with_seal_hash
        payload = {
            'public_id' : insert_response[0]['public_id'],
            'next_seal' : txo_public_ids[1],
        }
        update_response = update_document(payload)
        doc = get_a_document(insert_response[0]['public_id'], "true")
        #now sign the message and insert the data in the fields dedicated to the sender
        signed_message = cu.sign_message(bytes.fromhex(doc.document_with_seals_hash))
        update_payload = {
            'public_id': insert_response[0]['public_id'],
            'sender_signed_hash': signed_message['signed'].hex()
        }
        try:
            update_response = update_document(update_payload)
        except EonError as e:
            self.assertTrue(e.code == 409)
            self.assertTrue(e.message == 'Sender signature is corrupt.') #there is no sender key, therefore signature cannot be right
        doc = get_a_document(insert_response[0]['public_id'], "true")

        #unlock txos
        unlock_a_txo(doc.current_seal)
        unlock_a_txo(doc.next_seal)



if __name__ == '__main__':
    unittest.main()
