import unittest

from flask import current_app

from app.main.util.serviceauthproxyfactory import ServiceAuthProxyFactory
from app.test.base import BaseTestCase


class TestAuthServiceProxyFactory(BaseTestCase):

    def test_factory(self):
        """ request a connection from the connection factory and test the node being responsive """
        factory = ServiceAuthProxyFactory()
        product = factory.create(
            'any',
            rpc_user=current_app.config['RPC_USER'],
            rpc_password=current_app.config['RPC_PASSWORD'],
            rpc_host=current_app.config['RPC_HOST'],
            rpc_port=current_app.config['RPC_PORT'])
        chain = product.test_connection()
        self.assertTrue(chain['chain'] == 'elementsregtest')


if __name__ == '__main__':
    unittest.main()
