import unittest
import random
import os

from flask import current_app

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import ec
import app.main.util.schnorr as schnorr
from app.main.util.dlcontract import Oracle, OracleCommitment, Participant
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.crypto import cu
from app.main.service.rpc_service import schnorr_sign, verify_schnorr_sign
import app.main.util.address as address
from app.test.base import BaseTestCase

class TestSchnorr(BaseTestCase):

    def test_schnorr_hazmat_pubkeys(self):
        """ double check that the implementation is getting the same values as the hazmat library for public keys """
        kmc = KeyManagementClient()
        priv_key = kmc._get_kmic()._get_key()
        pub_key = priv_key.public_key()

        priv_numbers = priv_key.private_numbers()
        priv_int = priv_numbers.private_value
        public_numbers = priv_numbers.public_numbers

        A = schnorr.determine_pubkey(priv_int)

        self.assertTrue(A.get_x()==public_numbers.x)
        self.assertTrue(A.get_y()==public_numbers.y)

        serialized_pub = address.serialize_pubkey(A).decode("utf-8") 
        deserialized_A = address.deserialize_pubkey(serialized_pub)

        self.assertTrue(A.get_x()==deserialized_A.get_x())
        self.assertTrue(A.get_y()==deserialized_A.get_y())

    def test_schnorr_sig(self):
        """ Test the manual schnorr signature"""
        kmc = KeyManagementClient()
        priv_key = kmc._get_kmic()._get_key()
        priv_numbers = priv_key.private_numbers()
        priv_int = priv_numbers.private_value 
        n = schnorr.getCurve()[1]
        A = schnorr.determine_pubkey(priv_int)

        #signature
        k = schnorr.generate_random(); 

        R = schnorr.G.mul(k)
        m = 'asd'
        e = (schnorr.hashThis(m, R) %n)
        s = ((k - e*priv_int)%n)

        #verification
        sG = schnorr.G.mul(s)
        eTimesPublicKey = A.mul(e)
        #sG = R - hashTimesPublicKey
        #sG + hashTimesPublicKey = R
        expected_R = schnorr.set_secp256p1().add(sG, eTimesPublicKey)    

        self.assertTrue(expected_R.get_x(), R.get_x())
        self.assertTrue(expected_R.get_y(), R.get_y())

        #test the RPC method
        k_hex = k.to_bytes((k.bit_length() + 7) // 8, 'big').hex() #e.g. 790ba62c2193cfc484d8693495837aac7e2bb4de3397a6d78c3289d8bc5b1a12 
        priv_hex = priv_int.to_bytes((priv_int.bit_length() + 7) // 8, 'big').hex() #e.g. 5585b043fdd515a02ca9eb0c9fb0b5ef9a743f1281e3301a37d4f2f332ad3440
        print(k_hex, priv_hex)
        res = schnorr_sign(m, k_hex, priv_hex)
        s_hex = res["result"]
        s_comp = int.from_bytes(bytes.fromhex(s_hex), "big")
        self.assertTrue(s_comp == s )
        pub = address.serialize_pubkey(A).decode("utf-8")
        res_verify = verify_schnorr_sign(m, k_hex, pub, s_hex)
        self.assertTrue(res_verify["result"]=="true")

    def test_schnorr_oracle(self):
        """ test Oracle """
        #create an oracle PublicKey, create an outcome_i PublicKey, compute the pubA+pubOut, then with the signed message by the oracle, create the privA+privOut and test
        oracle_priv_int = schnorr.generate_random()
        O = schnorr.determine_pubkey(oracle_priv_int)
        kmc = KeyManagementClient()
        priv_key = kmc._get_kmic()._get_key()
        priv_numbers = priv_key.private_numbers()
        priv_int = priv_numbers.private_value
        n = schnorr.getCurve()[1]
        A = schnorr.determine_pubkey(priv_int)

        #commit to the point R
        k = schnorr.generate_random();
        R = schnorr.G.mul(k)

        #Alice computes the outcome_i_PublicKey 
        outcome_i = "ok"
        ei = (schnorr.hashThis(outcome_i, R) %n)
        eiTimesOraclePublicKey = O.mul(ei)
        negEiTimesOraclePublicKey = eiTimesOraclePublicKey.negation()
        oiG = R.add(negEiTimesOraclePublicKey)
        
        #Alice creates the publicKey 
        Aoig = A.add(oiG)

        #The Oracle computes the signature of the outcome_i
        eiTimesOraclePrivInt = ((ei*oracle_priv_int)%n)
        si = ((k - eiTimesOraclePrivInt)%n)

        #Alice receives the signed message i by the Oracle, which is the key, check that the outcome_i_PublicKey
        expected_oiG = schnorr.G.mul(si)

        #build the private key that can spend:
        priv_ia = (si + priv_int)%n
        expected_Aoig = schnorr.G.mul(priv_ia)

        self.assertTrue(expected_Aoig.get_x(), Aoig.get_x())
        self.assertTrue(expected_Aoig.get_y(), Aoig.get_y())


    def test_address_from_composed_key(self):
        """ test Oracle """
        #create an oracle PublicKey, create an outcome_i PublicKey, compute the pubA+pubOut, then with the signed message by the oracle, create the privA+privOut and test
        oracle_priv_int = schnorr.generate_random()
        #oracle_priv_int = 66263699574289263131301875106398853252988772273302800014873407919715988453227
        O = schnorr.determine_pubkey(oracle_priv_int)
        kmc = KeyManagementClient()
        priv_key = kmc._get_kmic()._get_key()
        priv_numbers = priv_key.private_numbers()
        priv_int = priv_numbers.private_value
        n = schnorr.getCurve()[1]
        A = schnorr.determine_pubkey(priv_int)

        #commit to the point R
        k = schnorr.generate_random();
        R = schnorr.G.mul(k)

        #Alice computes the outcome_i_PublicKey 
        outcome_i = "okish"
        ei = (schnorr.hashThis(outcome_i, R) %n)
        eiTimesOraclePublicKey = O.mul(ei)
        negEiTimesOraclePublicKey = eiTimesOraclePublicKey.negation()
        oiG = schnorr.set_secp256p1().add(R,negEiTimesOraclePublicKey)
        
        #Alice creates the publicKey 
        #Aoig = A.add(oiG)
        Aoig = schnorr.set_secp256p1().add(A, oiG)

        #The Oracle computes the signature of the outcome_i
        eiTimesOraclePrivInt = ((ei*oracle_priv_int)%n)
        si = ((k - eiTimesOraclePrivInt)%n)

        #Alice receives the signed message i by the Oracle, which is the key, check that the outcome_i_PublicKey
        expected_oiG = schnorr.G.mul(si)

        #build the private key that can spend:
        priv_ia = (si + priv_int)%n
        expected_Aoig = schnorr.G.mul(priv_ia)

        self.assertTrue(expected_Aoig.get_x(), Aoig.get_x())
        self.assertTrue(expected_Aoig.get_y(), Aoig.get_y())

        """
        build the adresses from public key and alter on from the private key,
        load the wallet
        """
        hexpk = address.serialize_pubkey(Aoig)
        #test serialize and deserialize
        hexpk_from_str = address.deserialize_pubkey(hexpk.decode("utf-8"))
        self.assertTrue(Aoig.get_x() == hexpk_from_str.get_x()) 
        self.assertTrue(Aoig.get_y() == hexpk_from_str.get_y())

        #derive address:      
        descriptor = "wpkh("+hexpk.decode("utf-8")+")"
        descriptor_with_checksum = cu.get_descriptor_info(descriptor)
        addr = cu.derive_address(descriptor_with_checksum["descriptor"])
        addr_info = cu.get_address_info(addr[0])
        self.assertFalse(addr_info["ismine"])
        
        #send some coins to the address
        utxos = cu.get_utxos()
        self.assertTrue(isinstance(utxos, list))
        self.assertTrue(len(utxos) >= 1)
        sent_tx = cu.create_simple_transaction(utxos[0]['txid'], utxos[0]['n'], '7e57', addr[0], 0.1)
        self.requestNewTestBlock()
        raw_tx = cu.get_raw_tx(sent_tx)
        sent_tx_id = raw_tx["txid"]
        self.assertTrue(raw_tx['confirmations'] > 0)
        spendable_output_n = 0;
        for vout in raw_tx["vout"]:            
            if(vout["scriptPubKey"]["type"]=="witness_v0_keyhash" and vout.get("value")== '0.10000000'):
                break
            else:
                spendable_output_n+=1
         
        resc = cu.rescan_blockchain()   
        
        #try to spend before importing the secret key and fail
        can_sign = True
        try:
            payback_address = cu.get_new_address("payback")
            sent_tx_2 = cu.create_simple_transaction(sent_tx_id, spendable_output_n, '7e57', payback_address, 0.1)       
            self.requestNewTestBlock()
        except Exception as e:
            print(e)
            can_sign = False

        self.assertTrue(not can_sign)
        
        
        resc = cu.rescan_blockchain()   
        print(resc)

        
        #import the priv key and succeed at spending
        #from the private key create the base58check format
        priv_ia_bytes = address.int_to_32_bytes(priv_ia)
        wif = address.privkey_to_wif(priv_ia_bytes)

        
        wallet = cu.import_priv_key(wif, "localtest")
        addr_info = cu.get_address_info(addr[0])
        self.assertTrue(addr_info["ismine"])

        
        #spend from the address
        payback_address = cu.get_new_address("payback")
        print(payback_address)

        sent_tx_2 = cu.create_simple_transaction(sent_tx_id, spendable_output_n, '7e57', payback_address, 0.1)       
        self.requestNewTestBlock()
        raw_tx_2 = cu.get_raw_tx(sent_tx_2)
        print(raw_tx_2)
        self.assertTrue(raw_tx_2['confirmations'] > 0)


        """
        #don't import the wif, use "signwithkey"
        sent_tx_2 = cu.create_simple_transaction_sign_with_wif(sent_tx_id, spendable_output_n, '7e57', payback_address, 0.1, wif)        
        self.requestNewTestBlock()
        raw_tx_2 = cu.get_raw_tx(sent_tx_2)
        print(raw_tx_2)
        self.assertTrue(raw_tx_2['confirmations'] > 0)
        """
        




if __name__ == '__main__':
    unittest.main()
