import unittest
import requests
import random
import datetime

from app.main.model.dlcsession import DlcSession
from app.main.service.dlcsession_service import get_all_dlcsessions, save_new_dlcsession, get_a_dlcsession, lock_session
from app.main.service.dlcoutcome_service import get_all_dlcoutcomes, save_new_dlcoutcome, get_a_dlcoutcome, confirm_outcome, pay_outcome, default_outcome
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.crypto import cu
import app.main.util.schnorr as schnorr
import app.main.util.address as address
from app.test.base import BaseTestCase
from app.main.util.eonerror import EonError

class TestDlcSession(BaseTestCase):
    DEBUG = False

    def test_dlcsession_lifecycle(self):
        """ create dlc session and its outcome, put some coin in the escrow and finally spend them """
        kmc = KeyManagementClient()
        priv_key = kmc._get_kmic()._get_key()
        pub_key = priv_key.public_key()
        priv_numbers = priv_key.private_numbers()
        priv_int = priv_numbers.private_value
        O = schnorr.determine_pubkey(priv_int)
        pub_O = address.serialize_pubkey(O).decode("utf-8")

        payload = {
            "oracle_pubkey":pub_O,
            "value":0.1 #TODO: for now this must be static ad 0.1! adapt the rust library to get various
        }
        response = save_new_dlcsession(payload)

        dlc_id = response[0]["public_id"]
        self.assertTrue(response[1] == 201)

        dlc = get_a_dlcsession(dlc_id)
        self.assertTrue((dlc is not None))

        try:
            random_k = schnorr.generate_random()
            k = random_k.to_bytes(33, 'big')
            payload = {
                "k":k.hex(),
                "oracle_pubkey":pub_O
            }
            error_response = save_new_dlcsession(payload)
        except EonError as e:
            self.assertTrue(e.message == 'Wrong k, max 32 bytes length.')

        #try to lock a session with no outcomes:
        raises_exception=False
        try:
            locked_dlc = lock_session(dlc_id)
        except EonError as e:
            self.assertTrue(e.code==400)
            raises_exception=True 
        self.assertTrue(raises_exception)

        #create the dlc outcome and test that it takes the pubkey etc..
        a_sk_int = 66263699574289263131301875106398853252988772273302800014873407919715988453227
        compressed_pk = "03e33dc92e58956c10bba16e5652029d35a5a33ca79235c546a2ba9b18bd1f40c8"
        default_pubkey = "025b0513b7055b35afa51f5921fcfa84a8480332a7f52b404584ef12e43881e5db"
        dafault_wif = "cTCkaD1A6g9oMA6qPyDyTZaaHAZkzJgm7TUG9xfZ62miUohx1c9B"
        addr= "ert1qrnej32z25akkwl7y9l58emmhaym00wjy8lfzz9"
        message= "test"
        dlcoutcome_payload = {
            'winner_address':addr,
            'winner_pubkey':compressed_pk,
            'default_pubkey':default_pubkey,
            'dlc_session_id': dlc_id,
            'locktime': 3, #locktime is a number of blocks: the nSequence of the spending tx should be higher than this number but lower than the number of blocks that have been mined since the funding tx of the p2sh
            'message':message
        }

        response_outcome = save_new_dlcoutcome(dlcoutcome_payload)

        dlc = get_a_dlcsession(dlc_id)
        self.assertTrue(len(dlc.outcomes)==1)

        self.assertTrue(dlc.outcomes[0].escrow_address)
        self.assertTrue(dlc.outcomes[0].combined_pubkey)
        self.assertTrue(dlc.outcomes[0].combined_pubkey!=compressed_pk)

        #check that status is pending on both
        self.assertTrue(dlc.status=="pending")
        self.assertTrue(dlc.outcomes[0].status=="pending")

        #try to add another outcome:
        message2= "test2"
        dlcoutcome_payload2 = {
            'winner_address': addr,
            'winner_pubkey': compressed_pk,
            'default_pubkey': default_pubkey,
            'dlc_session_id': dlc_id,
            'locktime': 3,
            'message': message2
        }
        raises_exception=False
        try:
            response_outcome = save_new_dlcoutcome(dlcoutcome_payload2)
        except EonError as e:
            raises_exception=True
            self.assertTrue(e.code==400)
        self.assertTrue(raises_exception)

        #lock the session: i.e. send money to the escrow account

        locked_dlc = lock_session(dlc_id)
        #check that tx details are saved
        dlc = get_a_dlcsession(dlc_id)
        self.assertTrue(dlc.escrow_txid)
        self.assertTrue(dlc.escrow_voutn)
        #request a block
        self.requestNewTestBlock()

        #sign the message with oracle's priv_int and call the confirm_outcome function
        k_int = int.from_bytes(dlc.k, "big")
        R = schnorr.G.mul(k_int)
        n = schnorr.getCurve()[1]
        e = (schnorr.hashThis(message, R) %n)
        s = ((k_int - e*priv_int)%n)

        #convert s to hex string
        s_bytes = address.int_to_bytes(s)
        s_hex = s_bytes.hex()

        #test a wrong signature:
        got_exception = False
        try:
            wrong_s = "0103ee440f1f7357"
            confirm_outcome(dlc.outcomes[0].public_id, wrong_s)
        except EonError as e:
            got_exception = True
            self.assertTrue(e.message=="Invalid signature")
        self.assertTrue(got_exception)

        dlcoutcome = confirm_outcome(dlc.outcomes[0].public_id, s_hex)
        self.assertTrue(dlcoutcome.signed_message == s_hex)
        self.assertTrue(dlcoutcome.status == "confirmed")

        #pay the outcome winner
        #try to pay signing with the node key and fail:
        got_exception = False
        try:
            dlcoutcome = pay_outcome(dlcoutcome.public_id) #no wif means it will try with own signing node wif
        except EonError as e:
            got_exception = True
            self.assertTrue(e.message=="Wrong private key, escrow address is still locked")
        self.assertTrue(got_exception)

        #try to spend with the default key before the timelock:
        try:
            dlcoutcome = default_outcome(dlcoutcome.public_id, dafault_wif) 
        except EonError as e:
            got_exception = True
            self.assertTrue(e.message=="Timelock still active, wait more blocks")
        self.assertTrue(got_exception)

        #use the working key:
        priv_a_bytes = address.int_to_32_bytes(a_sk_int)
        wif_a = address.privkey_to_wif(priv_a_bytes)
        dlcoutcome = pay_outcome(dlcoutcome.public_id, wif_a)

        #check that there is the sent_with_tx field
        self.assertTrue(dlcoutcome.sent_with_tx!=None)
        self.requestNewTestBlock()

        final_raw_tx = cu.get_raw_tx(dlcoutcome.sent_with_tx)
        self.assertTrue(final_raw_tx['confirmations'] > 0) #can't work for non wallet tx
                


if __name__ == '__main__':
    unittest.main()
        