import datetime
import unittest

from app.main.services import db
from app.main.util.crypto import cu
from app.main.model.notarizationsession import NotarizationSession
from app.main.model.notarizationitem import NotarizationItem
from app.main.model.txo import Txo
from app.main.model.document import Document
from app.test.base import BaseTestCase
from app.main.util.eonerror import EonError
from app.main.util.eonutils import eonutils

from app.main.service.notarizationsession_service import save_new_notarization_session, delete_notarization_session, get_a_notarization_session, lock_notarization_session, get_notarisation_items_ordered_by_id, notarize_session
from app.main.service.notarizationitem_service import save_new_notarization_item
from app.main.service.txo_service import get_fresh_utxos
from app.main.service.document_service import get_a_document, get_related_documents


class TestNotarizationItemSessionModel(BaseTestCase):

    DEBUG = False
    
    def test_new_session_with_items(self):
        """ create a session, add items, remove session, check items are removed """
        session_payload = {
        } #can be created with no data
        res = save_new_notarization_session(session_payload)
        session_id = res[0]["public_id"]
        self.assertTrue(session_id)

        #missing message:
        item_payload = {
            'notarization_session_id':session_id,
            'notes':'just a test'
        }
        error = False
        try:
            res = save_new_notarization_item(item_payload)
        except Exception as e:
            error = True 
        self.assertTrue(error)

        #missing session
        item_payload = {
            'message_hash':'9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',
            'notes':'just a test'
        }
        error = False
        try:
            res = save_new_notarization_item(item_payload)
        except Exception as e:
            error = True 
        self.assertTrue(error)

        #wrong hash
        item_payload = {
            'notarization_session_id':session_id,
            'message_hash':'9f86d081884c7d659a2feaa0c55ad',
            'notes':'just a test'
        }
        error = False
        try:
            res = save_new_notarization_item(item_payload)
        except Exception as e:
            error = True 
        self.assertTrue(error)

        #happy flow
        item_payload = {
            'notarization_session_id':session_id,
            'message_hash':'9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',
            'notes':'just a test'
        }
        res = save_new_notarization_item(item_payload)
        item_id = res[0]["public_id"]
        self.assertTrue(item_id)

        #destroy session, check that the items are destroyed as well:
        res = delete_notarization_session(session_id)
        items = NotarizationItem.query.all()
        self.assertTrue(len(items)==0)

    def test_merkle_tree_notarisation(self):
        """ Create a session and lock it """
        #insert session
        session_payload = {
        } #can be created with no data
        res = save_new_notarization_session(session_payload)
        session_id = res[0]["public_id"]
        self.assertTrue(session_id)

        item_ids = []
        #insert item 1
        item_payload = {
            'notarization_session_id':session_id,
            'message_hash':'9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',
            'notes':'just a test'
        }
        res = save_new_notarization_item(item_payload)
        item_id_1 = res[0]["public_id"]
        self.assertTrue(item_id_1)
        item_ids.append(item_id_1)

        #insert item 2
        item_payload = {
            'notarization_session_id':session_id,
            'message_hash':'9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a07',
            'notes':'just a test'
        }
        res = save_new_notarization_item(item_payload)
        item_id_2 = res[0]["public_id"]
        self.assertTrue(item_id_2)
        item_ids.append(item_id_2)

        #insert item 3
        item_payload = {
            'notarization_session_id':session_id,
            'message_hash':'9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a06',
            'notes':'just a test'
        }
        res = save_new_notarization_item(item_payload)
        item_id_3 = res[0]["public_id"]
        self.assertTrue(item_id_3)
        item_ids.append(item_id_3)

        #make sure we get them in lexicographic order
        item_ids.sort()
        items = get_notarisation_items_ordered_by_id(session_id)
        i = 0
        for item_id in item_ids:
            self.assertTrue(items[i].public_id == item_id)
            i+=1

        session_res = lock_notarization_session(session_id)
        self.assertTrue(session_res[0].status == "locked and waiting notarization")

        notarisation_res = notarize_session(session_id)
        #generate a block to add 1 confirmation to the tx:
        self.requestNewTestBlock()
        self.assertTrue(notarisation_res[0].status == "closed")    
        self.assertTrue(notarisation_res[0].notarization_txid)    

        #get the seals form the doc and unlock them
        doc = Document.query.filter_by(public_id=session_res[0].document_id).first()
        seal_a = Txo.query.filter_by(public_id=doc.current_seal).first()
        seal_b = Txo.query.filter_by(public_id=doc.next_seal).first()
        seal_a_spent = False
        #first seal is spent, therefore this must fail:
        try:
            cu.unlock_utxo(seal_a.txid, seal_a.voutn)
        except Exception as e:
            seal_a_spent=True 
        self.assertTrue(seal_a_spent)
        #second seal is still open
        cu.unlock_utxo(seal_b.txid, seal_b.voutn)

        ################
        #create a new session, refer it to the old one
        session_payload_2 = {
            "previous_session_id":session_id
        } #can be created with no data
        res_2 = save_new_notarization_session(session_payload_2)
        session_id_2 = res_2[0]["public_id"]
        self.assertTrue(session_id_2)

        item_ids_2 = []
        #insert items
        item_payload_2 = {
            'notarization_session_id':session_id_2,
            'message_hash':'9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',
            'notes':'just a test'
        }
        res_2 = save_new_notarization_item(item_payload_2)
        item_id_2_1 = res_2[0]["public_id"]
        self.assertTrue(item_id_2_1)
        item_ids.append(item_id_2_1)

        item_payload_2 = {
            'notarization_session_id':session_id_2,
            'message_hash':'9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a07',
            'notes':'just a test'
        }
        res_2 = save_new_notarization_item(item_payload_2)
        item_id_2_2 = res_2[0]["public_id"]
        self.assertTrue(item_id_2_2)
        item_ids.append(item_id_2_2)

        #lock the second session
        session_res_2 = lock_notarization_session(session_id_2)
        self.assertTrue(session_res_2[0].status == "locked and waiting notarization")

        #check that the new session has current_seal == next_seal of the previous session
        doc_2 = Document.query.filter_by(public_id=session_res_2[0].document_id).first()
        seal_a_2 = Txo.query.filter_by(public_id=doc_2.current_seal).first()
        seal_b_2 = Txo.query.filter_by(public_id=doc_2.next_seal).first()
        self.assertTrue(seal_b.public_id == seal_a_2.public_id)

        #spend the second session
        notarisation_res_2 = notarize_session(session_id_2)
        #generate a block to add 1 confirmation to the tx:
        self.requestNewTestBlock()
        self.assertTrue(notarisation_res_2[0].status == "closed")    
        self.assertTrue(notarisation_res_2[0].notarization_txid)    

        #get the document for the new session and check that in its chain there's the previous document
        doc_chain = get_related_documents(doc_2.public_id)
        self.assertTrue(len(doc_chain)==2)
        self.assertTrue(doc_chain[0].public_id == doc.public_id)
        self.assertTrue(doc_chain[1].public_id == doc_2.public_id)

        #after all tests:
        #check utxos are still there? Depends on the init status of the node. 
        #first seal is spent, therefore this must fail:
        seal_spent=False
        try:
            cu.unlock_utxo(seal_a_2.txid, seal_a_2.voutn)
        except Exception as e:
            seal_a_spent=True 
        self.assertTrue(seal_a_spent)
        #second seal is still open
        cu.unlock_utxo(seal_b_2.txid, seal_b_2.voutn)
        #NOTE: a fresh node may not have 2 utxos, you should create them?
        utxos = get_fresh_utxos(2)
        self.assertTrue(len(utxos)==2)
if __name__ == '__main__':
    unittest.main()