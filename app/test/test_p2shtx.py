import unittest
import random
import os
import binascii

from flask import current_app
from binascii import b2a_hex, unhexlify
from hashlib import new, sha256

from bitcoin import SelectParams
from bitcoin.core import b2x, lx, COIN, COutPoint, CMutableTxOut, CMutableTxIn, CMutableTransaction, Hash160
from bitcoin.core.script import CScript, OP_DUP, OP_HASH160, OP_EQUALVERIFY, OP_CHECKSIG, SignatureHash, SIGHASH_ALL, OP_IFDUP, OP_NOTIF, OP_CHECKSIGVERIFY, OP_CHECKSEQUENCEVERIFY, OP_ENDIF
from bitcoin.core.scripteval import VerifyScript, SCRIPT_VERIFY_P2SH
from bitcoin.wallet import CBitcoinAddress, CBitcoinSecret

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import ec
import app.main.util.schnorr as schnorr
from app.main.util.tx.script import InputScript, Script, OutputScript
from app.main.util.dlcontract import Oracle, OracleCommitment, Participant
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.crypto import cu
import app.main.util.address as address
from app.test.base import BaseTestCase

from app.main.util.eonrust import create_p2sh_address, create_spending_tx

SelectParams('testnet')

class TestSchnorr(BaseTestCase):

    def test_complex_script2(self):
        """ create two outcomes claimed by 2 different keys, create the p2sh address, then try to spend. Static test only comparing precomputed results"""

        res = build_composite_keys() #use signing key as key A, generate a random K for the signature and a random oralce key, key B is the local node

        #preapre the public and private keys for party A and B
        winner_pubkey_hexlified = address.serialize_pubkey(res["Aoig"]) #pub_key_A = message_i + A, this is dynamic and changes at every test.. for now we need a static one
        winner_pubkey = binascii.unhexlify(winner_pubkey_hexlified).hex()
        winning_priv_int = res["priv_ia"]
        winning_wif = address.privkey_to_wif(address.int_to_32_bytes(winning_priv_int))

        priv_b_wif = "cTCkaD1A6g9oMA6qPyDyTZaaHAZkzJgm7TUG9xfZ62miUohx1c9B"
        priv_b_bytes = address.wif_to_privkey(priv_b_wif)
        priv_b_int = int.from_bytes(priv_b_bytes, byteorder="big")
        B = schnorr.determine_pubkey(priv_b_int)
        default_pub_key_bytes = address.serialize_pubkey(B)
        default_pub_key = binascii.unhexlify(default_pub_key_bytes).hex()

        #print("A: ", winning_wif, winner_pubkey)
        #print("B: ", priv_b_wif, default_pub_key)

        #create the custom address P2SH:
        txin_p2sh_address = create_p2sh_address(winner_pubkey, default_pub_key, "3") 
        txin_p2sh_address_static = "XCZa81Ee1SdCnPfg3Xxq95rH7nxHRJpEFZ" #new version with OP_DROP etc..     
        self.assertTrue(txin_p2sh_address==txin_p2sh_address_static)


        #use static values for default
        txid = "db087b014e5bf5e53ff814bf9ad291e6f010035fcfbb8083033e1e1092e1a632" 
        voutn = 2
        
        #winning:
        static_A_spending_tx = "01000000000132a6e192101e3e038380bbcf5f0310f0e691d29abf14f83fe5f55b4e017b08db02000000a34830450221009b4eccf025bfeccd0fe6e9aaee1d8541f06fb79156650450b98bd24b30bb428102201788102bb1f4a1bf78a84d97faf6df9db9f282a8d91155f0cebfab716c137a2e012103e6e9385ce0203306c819540b21947ba3f47b904cb93fc8d626acc35857f3495151366376a914ecbd6ab7d9841dc4a40bafb45bc65956ed73f91c6753b27576a9140b3e08858752a61605279abd72f6700efa4d8cad6888ac030000000201230f4f5d4b7c6fa845806ee4f67713459e1b69e8e60fcee2e4940c7a0d5de1b201000000000098929800160014ecbd6ab7d9841dc4a40bafb45bc65956ed73f91c01230f4f5d4b7c6fa845806ee4f67713459e1b69e8e60fcee2e4940c7a0d5de1b20100000000000003e8000001000000"
        hex_tx2 = create_spending_tx(winning_wif, txid, str(voutn), "10000000", "1000", winner_pubkey, default_pub_key, "3") 
        self.assertTrue(hex_tx2==static_A_spending_tx) 
                
        #deafult
        static_B_spending_tx = "02000000000132a6e192101e3e038380bbcf5f0310f0e691d29abf14f83fe5f55b4e017b08db02000000a3483045022100d1093554d979dc6d462709ac80c80125f4fee36acdcb964a4b573b6cdaa92d540220010353f9f503367ddc760032547933ee8ca921fd908ace8d7e1da5c00b84844d0121025b0513b7055b35afa51f5921fcfa84a8480332a7f52b404584ef12e43881e5db00366376a914ecbd6ab7d9841dc4a40bafb45bc65956ed73f91c6753b27576a9140b3e08858752a61605279abd72f6700efa4d8cad6888ac030000000201230f4f5d4b7c6fa845806ee4f67713459e1b69e8e60fcee2e4940c7a0d5de1b201000000000098929800160014ecbd6ab7d9841dc4a40bafb45bc65956ed73f91c01230f4f5d4b7c6fa845806ee4f67713459e1b69e8e60fcee2e4940c7a0d5de1b20100000000000003e8000001000000"
        hex_tx = create_spending_tx(priv_b_wif, txid, str(voutn), "10000000", "1000", winner_pubkey, default_pub_key, "3")        
        self.assertTrue(hex_tx==static_B_spending_tx)

        #fail for fees
        panic = False
        try:
            hex_tx = create_spending_tx(priv_b_wif, txid, str(voutn), "1000", "2000", winner_pubkey, default_pub_key, "3") 
        except Exception as e:
            panic = True 
        self.assertTrue(panic)

if __name__ == '__main__':
    unittest.main()


def hash_160(data):
    """ Calculates the RIPEMD-160 hash of a given string.
    :param data: Data to be hashed. Normally used with ECDSA public keys.
    :type data: hex str
    :return: The RIPEMD-160 hash.
    :rtype: bytes
    """

    # Calculate the RIPEMD-160 hash of the given data.
    md = new('ripemd160')
    h = sha256(unhexlify(data)).digest()
    md.update(h)
    h160 = md.digest()

    return h160

def build_composite_keys():
    """ generate a random oracle key, use local key as winner key, build the combined key from the  """
    #create an oracle PublicKey, create an outcome_i PublicKey, compute the pubA+pubOut, then with the signed message by the oracle, create the privA+privOut and test
    #oracle_priv_int = schnorr.generate_random()
    oracle_priv_int = 66263699574289263131301875106398853252988772273302800014873407919715988453227
    O = schnorr.determine_pubkey(oracle_priv_int)
    kmc = KeyManagementClient()
    priv_key = kmc._get_kmic()._get_key()
    priv_numbers = priv_key.private_numbers()
    priv_int = priv_numbers.private_value
    n = schnorr.getCurve()[1]
    A = schnorr.determine_pubkey(priv_int)

    #commit to the point R
    k = schnorr.generate_random();
    k = 57515335877946651103000592781062611306605479119981192844740481185759581996391
    R = schnorr.G.mul(k)

    #Alice computes the outcome_i_PublicKey 
    outcome_i = "okish"
    ei = (schnorr.hashThis(outcome_i, R) %n)
    eiTimesOraclePublicKey = O.mul(ei)
    negEiTimesOraclePublicKey = eiTimesOraclePublicKey.negation()
    oiG = schnorr.set_secp256p1().add(R,negEiTimesOraclePublicKey)
    
    #Alice creates the publicKey 
    #Aoig = A.add(oiG)
    Aoig = schnorr.set_secp256p1().add(A, oiG)

    #The Oracle computes the signature of the outcome_i
    eiTimesOraclePrivInt = ((ei*oracle_priv_int)%n)
    si = ((k - eiTimesOraclePrivInt)%n)

    #Alice receives the signed message i by the Oracle, which is the key, check that the outcome_i_PublicKey
    expected_oiG = schnorr.G.mul(si)

    #build the private key that can spend:
    priv_ia = (si + priv_int)%n
    expected_Aoig = schnorr.G.mul(priv_ia)

    res = {
        'oracle_priv_int':oracle_priv_int,
        'O':O,
        'a_priv_int':priv_int,
        'k':k,
        'Aoig':Aoig,
        'si': si,
        'priv_ia':priv_ia
    }
    return res
