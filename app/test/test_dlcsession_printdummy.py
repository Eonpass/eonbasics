import requests
import random
import datetime

from app.main.model.dlcsession import DlcSession
from app.main.service.dlcsession_service import get_all_dlcsessions, save_new_dlcsession, get_a_dlcsession, lock_session
from app.main.service.dlcoutcome_service import get_all_dlcoutcomes, save_new_dlcoutcome, get_a_dlcoutcome, confirm_outcome, pay_outcome, default_outcome
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.crypto import cu
import app.main.util.schnorr as schnorr
import app.main.util.address as address
from app.test.base import BaseTestCase
from app.main.util.eonerror import EonError

class TestDlcSessionDummyDataPrints(BaseTestCase):
    DEBUG = False

    def test_dlcsession_lifecycle(self):
        """ create dlc session and its outcome, put some coin in the escrow and finally spend them """
        kmc = KeyManagementClient()
        priv_key = kmc._get_kmic()._get_key()
        pub_key = priv_key.public_key()
        priv_numbers = priv_key.private_numbers()
        priv_int = priv_numbers.private_value
        print("priv int", hex(priv_int))
        local_wif = address.privkey_to_wif(address.int_to_32_bytes(priv_int))
        print("wif", local_wif)

        O = schnorr.determine_pubkey(priv_int)
        pub_O = address.serialize_pubkey(O).decode("utf-8")

        payload = {
            "oracle_pubkey":pub_O,
            "value":0.1 #TODO: for now this must be static ad 0.1! adapt the rust library to get various
        }
        print(payload)
        response = save_new_dlcsession(payload)

        dlc_id = response[0]["public_id"]
        self.assertTrue(response[1] == 201)

        dlc = get_a_dlcsession(dlc_id)
        self.assertTrue((dlc is not None))
        print(dlc)
        print(dlc.k)

    def test_prepare_winner_data(self):
        wif = 'cQdubDSq5iUX3QFWe3GWzgCTY6YMLKVBejiHwiSmvfqxiPJ52hre'
        addrs = 'el1qqw4vu3cmnnm0qncryc9p4w4eqs9n8ayekqgl6ldxe5fvz04zhrp530f8kjjt8vf6nerwfa6hs0vurhlqruzzz88jnyewvwn36'

        priv_int_bytes = address.wif_to_privkey(wif)
        priv_int = int.from_bytes(priv_int_bytes, "big")
        pub_key_EC = schnorr.determine_pubkey(priv_int)
        bip_pub_key = address.serialize_pubkey(pub_key_EC)
        print("remote bip:", bip_pub_key)