import unittest
import os.path
from flask import current_app

from cryptography.hazmat.primitives import serialization

from app.main.util.keymanagementutils import KeyManagementClient
from app.test.base import BaseTestCase


class TestKeyManagmentClientFactory(BaseTestCase):

    def test_kmc_factory(self):
        """ Get the current test key, in test env we use the developer KMI which creates a file in the project folder """
        kmc = KeyManagementClient()
        #test the loaded type of KMC according to config
        self.assertTrue(kmc._get_kmic()._type == current_app.config['KMI_TYPE'])
        #check that the privatekey file has been created or was already there:
        filename = os.path.join(os.path.dirname(__file__), '../main/util/privkey.pem') #path relative to test file
        exception_raised = False
        try:
            with open(filename, 'rb') as pem_in:
                pemlines = pem_in.read()
        except OSError as e:
            print(e)
            exception_raised = True
        finally:
            self.assertFalse(exception_raised)     

    def test_kmc_signature(self):
        """ Test the signature and verification """
        message = '0145eaea'
        kmc = KeyManagementClient()
        result = kmc.sign_message(bytes.fromhex(message))
        priv_key = kmc._get_kmic()._get_key()
        pub_key = priv_key.public_key()
        serialized_pub_key = pub_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        #check that the public key is the one originating from the file
        self.assertTrue(result['serialized_public'] == serialized_pub_key)
        #check that the signature is correct
        verify_outcome = kmc.verify_signed_message(result['signed'], bytes.fromhex(message), serialized_pub_key)
        self.assertTrue(verify_outcome)
        #check that the signature is non corrupt
        message2 = '32323232'
        verify_outcome = kmc.verify_signed_message(result['signed'], bytes.fromhex(message2), serialized_pub_key)
        self.assertFalse(verify_outcome)


if __name__ == '__main__':
    unittest.main()
